<?php 
class mailboxManager {
    
    public static function query_Event($method, $args){   
        include("mod_db.php");
        date_default_timezone_set('Asia/Taipei');
        $ini_result = self::iniread(__pageroot . "core/query/sys.ini");  

        switch ($method) {            
            case 'getSellMailboxList': // 列表
                $sql_inquery = $ini_result['data']['getSellMailboxList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getRentMailboxList': // 列表
                $sql_inquery = $ini_result['data']['getRentMailboxList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getRentMailboxList': // 列表
                $sql_inquery = $ini_result['data']['getRentMailboxList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'loadMailbox': 
                $sql_inquery = $ini_result['data']['loadMailbox']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "updateMailboxStatus":
                $sql_inquery = $ini_result['data']['updateMailboxStatus']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['status'],PDO::PARAM_INT);
                $state->bindValue(2,$args['comment'],PDO::PARAM_STR);
                $state->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(4,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return array("updatetime" => date("Y-m-d H:i:s"));
                }
            break;
            default:
                # code...
            break;
        }
    }
     

    //base
    private static function iniread($filename) 
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");  //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }

}
?>