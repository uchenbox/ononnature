jQuery(function($){
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
      })
    // headerCarousel();
});

var headerCarousel = function(){
    $('.header-loop').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoHeight:true,
        items:1,
        loop:true,
        // margin:30,
        autoplay:true,
        dots: false,
    });
}

var showmore = function(obj){
    $count = $('.gallery-item.d-none').length;
    for (let index = 0; index < 6; index++) {
        const element = $('.gallery-item.d-none:eq(0)');
        element.removeClass('d-none');
        
        $count = $('.gallery-item.d-none').length;
        if($count == 0)
        {
            obj.addClass('d-none');
            $("#mb-150").attr('style', 'margin-bottom:150px;');
        }
    }
    
}