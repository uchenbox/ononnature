<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>

<div class="abgne_tab">

    <ul class="tabs">
        <li><a href="javascript:void(0)" for-id="tab1">訊息</a></li>
        <li><a href="javascript:void(0)" for-id="tab2">內容</a></li>
    </ul>
    
    <div class="tab_container">
    
		<form id="form_edit_save" class="form-horizontal">
        
			<?php foreach( $_html_ as $key => $val ){ 
                    
                    $table_sn = 'tabsn'.$key;
            ?>
				<div class="Table_border <?=$table_sn?>">
            	
                    <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">
                    
                    <div id="tab1" class="tab_content">
        				<?php 
						//-----------------------------------------//
						$Arr_Name = 'Product_Code'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Name'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
						//-----------------------------------------//
						$Arr_Name = 'Proclass_ID';
						
						echo $_TF->html_select($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1, $Class_Arr[$Main_Key3], '');
						//-----------------------------------------//
						$Arr_Name = 'Proclass_ID';
						
						echo $_TF->html_text_arrange($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', '');

						//-----------------------------------------//
						$Arr_Name = 'Product_Price'; 
						
						echo $_TF->html_number($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, '', '');
						//-----------------------------------------//
						$Arr_Name = 'Product_Intro';
				 
						echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');
						//-----------------------------------------//
						$Arr_Name = 'Product_Content';
				 
						echo $_TF->html_textedit($table_info[$Arr_Name]['Comment'], $Arr_Name, $val);
						//-----------------------------------------//
						$Arr_Name = 'Product_Img';
						
						echo $_TF->html_uploadimg($table_info[$Arr_Name]['Comment'], '( 建議尺寸 407 * 407 )', $Arr_Name, $val, '', 0);
						//-----------------------------------------//
						$Arr_Name = 'Product_Img1';
						
						echo $_TF->html_uploadimg($table_info[$Arr_Name]['Comment'], '( 建議尺寸 1366 * 915 )', $Arr_Name, $val, '', 0);
						//-----------------------------------------//
						$Arr_Name = 'Product_Img2';
						
						echo $_TF->html_uploadimg($table_info[$Arr_Name]['Comment'], '( 建議尺寸 1366 * 915 )', $Arr_Name, $val, '', 0);
						//-----------------------------------------//
						$Arr_Name = 'Product_Img3';
						
						echo $_TF->html_uploadimg($table_info[$Arr_Name]['Comment'], '( 建議尺寸 1366 * 915 )', $Arr_Name, $val, '', 0);
						//-----------------------------------------//
						$Arr_Name = 'Product_Sort';
						
						echo $_TF->html_number($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, 0, 99999);
						//-----------------------------------------//
						$Arr_Name = 'Product_Hot';
						
						echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Open';
						
						echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Sdate';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', '', $Arr_Name, $val, '', 0);
						?>
        	
					</div>   
					<div id="tab2" class="tab_content">
        
        	
					</div>                  
                         
	             </div>
	            <?php } ?>
			
                <div class="clear_both form-actions">
                    <button id="saveb" class="btn btn-info" type="button" onclick="form_edit_save()">
                        <i class="ace-icon fa fa-check bigger-110"></i>儲存
                    </button>&nbsp;&nbsp;&nbsp; 
                            
                    <button id="rsetb" class="btn btn" type="reset">
                        <i class="ace-icon fa fa-check bigger-110"></i>重設
                    </button>
                </div>
            </form>
		
    </div>
</div>

<script type="text/javascript">$('.imgajax').colorbox({width:"70%", height:"100%", rel:'imgajax'});</script>