<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column ">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
        <!-- header -->
        <div id="intrp-style-wrap" class="d-flex flex-column p-0 m-0">
            <div class="intrp-style-content">
                <h4 class="text-center">– ACCESS –</h4>
                <h2 class="text-center">園區交通</h2>
                <hr>
            </div>   
            <div class="d-flex flex-row meal-items-content">
                <div class="traffic-items item-1">
                    <div class="img-wrap">
                        <img class="w-100" src="<?php echo (!empty($data['traffic_header'][0]['Traffic_Img_Header']))?$data['traffic_header'][0]['Traffic_Img_Header']: '#'; ?>" alt="園區交通">
                    </div>
                </div>
                <div class="traffic-items item-2 d-md-block">
                    <div class="bg-gray float-right"></div>
                </div>     
            </div>    
        </div>
        
        <!-- 交通方式 -->
        <div class="traffic-way-content d-flex flex-column  p-0 m-0">
            <div class="traffic-way-wrap px-0 d-flex flex-column">
                <?php
                
                foreach ($data['traffic_way'] as $way) {
                    echo '<div class=" px-0 items d-flex flex-row">
                            <div class="px-0 mx-0 item-way">
                                <h3>'.$way['Traffic_Title_Way'].'</h3>
                            </div>
                            <div class="px-0 mx-0  item-desp">
                                <p>
                                '.str_replace("</p>", "<br/>", str_replace("<p>", '', $way['Traffic_Content_Way'])).'
                                </p>
                            </div>
                        </div>';
                }
                foreach ($data['traffic_way'] as $way) {
                    if(isset($way['Traffic_Img_Way']) && !empty($way['Traffic_Img_Way']))
                    {
                        echo '<div class="row px-0 items las-el">
                                <div class="col-12">
                                    <img class="w-100" src="'.((!empty($way['Traffic_Img_Way']))?$way['Traffic_Img_Way']: '#').'" alt="園區交通">
                                </div>
                            </div>';
                    }
                }
                ?>
                <!-- <div class="row px-0 items">
                    <div class="col-md-4 px-0 pl-md-0 pr-md-3 mx-0 ml-md-0 item-content">
                        <h3>自行開車</h3>
                    </div>
                    <div class="col-md-8 px-0 pl-md-3 pr-md-0 mx-0 mr-md-0 item-carousel">
                        <p>
                        請ＧＯＯＧＬＥ定位： ＯnOnＮature 泱泱自然園區 <br/>
                        國道1號、3號 → 國道4號（往豐原） → 豐勢路（往石岡） ※ 園區後方設有停車場
                        </p>
                    </div>
                </div>
                <div class="row px-0 items">
                    <div class="col-md-4 px-0 pl-md-0 pr-md-3 mx-0 ml-md-0 item-content">
                        <h3>高鐵轉乘</h3>
                    </div>
                    <div class="col-md-8 px-0 pl-md-3 pr-md-0 mx-0 mr-md-0 item-carousel">
                        <p>
                        高鐵台中站轉搭公車153號（6號出口21號月台） <br/>→ 於“石岡站”下車 <br/>→ 過馬路至對向，沿紅色樓梯往下抵達園區
                        </p>
                    </div>
                </div>
                <div class="row px-0 items">
                    <div class="col-md-4 px-0 pl-md-0 pr-md-3 mx-0 ml-md-0 item-content">
                        <h3>台鐵轉乘</h3>
                    </div>
                    <div class="col-md-8 px-0 pl-md-3 pr-md-0 mx-0 mr-md-0 item-carousel">
                        <p>
                        台鐵豐原站 → 步行至中正路豐原客運總站 <br/>→ 搭乘公車90/91/206~209 <br/>→ 於“石岡站”下車 <br/>→ 過馬路至對向，沿紅色樓梯往下抵達園區
                        </p>
                    </div>
                </div> 
                <div class="row px-0 items">
                    <div class="col-12">
                        <img class="w-100" src="assets/02B_園區交通/01_map.png" alt="OnOnNature園區交通">
                    </div>
                </div>-->
            </div>
        </div>

        <!-- 周邊景點 -->
        <div class="nearby-content d-flex flex-column  p-0 mx-0">
            <div class="row p-0 mx-0 w-100" id="cases-content">
                <div class="cases-wrap">
                    <div class="row px-0 mx-0 item-title">
                        <div class="col-12 text-center">
                            <h3 class="mb-0">NEARBY ATTRACTIONS</h3>
                        </div>
                    </div>
                    <div class="row p-0 m-0 items mx-auto">
                        <?php 
                            $gallery_count = 1;
                            foreach ($data['traffic_gallery'] as $item) {
                                echo '<div class=" item gallery-item '.(($gallery_count > 6)?'d-none':'').'">
                                        <a href="'.((isset($item['Traffic_Img_Gallery']))?$item['Traffic_Img_Gallery']:'#').'" data-lightbox="sky" data-title="">
                                            <img class="img-1x1" src="'.((isset($item['Traffic_Img_Gallery']))?$item['Traffic_Img_Gallery']:'#').'" alt="">
                                        </a>
                                    </div>';
                                $gallery_count++;
                            }
                        ?>
                        
                    </div>
                    <!-- <div class="row p-0 m-0">
                        <div class="viewmoreBt text-center mx-auto" onclick="showmore($(this));">view more</div>
                        <div id="mb-150"></div>
                    </div> -->
                </div>
            </div>
            
        </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/traffic.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>