<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>

<div class="abgne_tab">

    <ul class="tabs">
        <li><a href="javascript:void(0)" for-id="tab1">基本資料</a></li>
    </ul>
    
    <div class="tab_container">
    
		<div id="tab1" class="tab_content">
        
        	<form id="form_edit_save" class="form-horizontal">
        
			<?php foreach( $_html_ as $key => $val ){ 
                    
                    $table_sn = 'tabsn'.$key;
            ?>
				<div class="Table_border <?=$table_sn?>">
            	
                    <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">
                    
                    <?php 
					//-----------------------------------------//
					$Arr_Name = 'Favor_Name'; 
					
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', '', $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
					//-----------------------------------------//
					$Arr_Name = 'Favor_Count'; 
					
					echo $_TF->html_number($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, 0, 99999);
					//-----------------------------------------//
					$Arr_Name = 'Favor_PandD'; 
					
					echo $_TF->html_favorable($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, 'Favor_Type', '', $favor_states, 'NO_FIRST_OPTION');
					//-----------------------------------------//
					$Arr_Name = 'Favor_Starttime'; 
					
					echo $_TF->html_datetime($table_info[$Arr_Name]['Comment'], '', '', $Arr_Name, $val, '', 1, $key, 'datestart', 'Favor_Endtime', 'YYYY-MM-DD HH:mm');
					//-----------------------------------------//
					$Arr_Name = 'Favor_Endtime'; 
					
					echo $_TF->html_datetime($table_info[$Arr_Name]['Comment'], '', '', $Arr_Name, $val, '', 1, $key, 'dateend', 'Favor_Starttime', 'YYYY-MM-DD HH:mm');
					//-----------------------------------------//
					$Arr_Name = 'Favor_Sort';
					
					echo $_TF->html_number($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, 0, 99999);
					//-----------------------------------------//
					$Arr_Name = 'Favor_Open';
					
					echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1);
					?>     
                </div>
            <?php } ?>
			
                <div class="clear_both form-actions">
                    <button id="saveb" class="btn btn-info" type="button" onclick="form_edit_save()">
                        <i class="ace-icon fa fa-check bigger-110"></i>儲存
                    </button>&nbsp;&nbsp;&nbsp; 
                            
                    <button id="rsetb" class="btn btn" type="reset">
                        <i class="ace-icon fa fa-check bigger-110"></i>重設
                    </button>
                </div>
            </form>
		</div>
    </div>
</div>

<script type="text/javascript">$('.imgajax').colorbox({width:"70%", height:"100%", rel:'imgajax'});</script>