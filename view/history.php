<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
            <!-- header -->
            <div id="intrp-style-wrap" class="d-flex flex-column p-0 m-0">
                <div class="intrp-style-content">
                    <h4 class="text-center">– COMPANY PROFILE –</h4>
                    <h2 class="text-center">紅屋沿革</h2>
                    <hr>
                </div>   
                <div class="d-flex flex-row meal-items-content">
                    <div class="traffic-items item-1">
                        <div class="img-wrap">
                            <img class="w-100" src="<?php echo (!empty($data['history_header'][0]['History_Img_Header']))?$data['history_header'][0]['History_Img_Header']: '#'; ?>" alt="紅屋沿革">
                        </div>
                    </div>
                    <div class="traffic-items item-2 d-md-block">
                        <div class="bg-gray float-right"></div>
                    </div>     
                </div>      
            </div>
            
            <!-- 紅屋故事 -->
            <div class="visit-desp-content d-flex flex-column  p-0 m-0">
                <div class="visit-desp-wrap px-0">
                    <div class="d-flex flex-row px-0 mx-auto items">
                        <div class="px-0 mx-0 item-content">
                            <h3 class="mx-auto"><?php echo $data['history_desp'][0]['History_Title_Desp']; ?></h3>
                        </div>
                        <div class="px-0 mx-0 item-carousel">
                            <p class="px-0">
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_desp'][0]['History_Content_Desp']);?>
                            
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="history-content d-flex flex-column  p-0 m-0">
                <div class="container-fluid px-0">
                    <div class="row px-0 mx-auto items">
                        <div class="col-md-4 px-0 pl-md-0 pr-md-3 mx-0 ml-md-0 item-content">
                            <h3 class="mx-auto"><?php //echo $data['history_desp'][0]['History_Title_Desp']; ?></h3>
                        </div>
                        <div class="col-md-8 px-0 pl-md-3 pr-md-0 mx-0 mr-md-0 item-carousel">
                            <p class="px-3">
                            <?php //echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_desp'][0]['History_Content_Desp']); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div> -->

            <!-- 1984 -->
            <div class="history-right-content d-flex flex-column  px-0 mx-0">
                <div class="container-fluid px-0">
                    <div class="items mb-0 ml-auto d-flex flex-row position-relative item-title">
                        <div class=" align-self-start">
                            <h3><?php echo $data['history_pdesp'][0]['History_Title_Pdesp']; ?></h3>
                        </div>
                        <div class="item-en-name align-self-start">
                            <small><?php echo $data['history_pdesp'][0]['History_Intro_Pdesp']; ?></small>
                        </div>
                        
                    </div>
                    <div class="items items-img mt-n5 ml-auto">
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="<?php echo (!empty($data['history_pdesp'][0]['History_Img_Pdesp']))?$data['history_pdesp'][0]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][0]['History_Intro_Pdesp']; ?>">
                        </div>
                    </div>
                    <div class="items items-paragraph ml-auto">
                        <p class="px-0">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_pdesp'][0]['History_Content_Pdesp']); ?>
                        </p>
                    </div>
                </div>
            </div>

            <!-- 2004 -->
            <div class="history-left-content d-flex flex-column  px-0 mx-0">
                <div class="container-fluid px-0">
                    <div class="items mb-0 mr-auto d-flex flex-row-reverse position-relative item-title">
                        <div class="align-self-start">
                            <h3><?php echo $data['history_pdesp'][1]['History_Title_Pdesp']; ?></h3>
                        </div>
                        <div class="align-self-start item-en-name">
                            <small><?php echo $data['history_pdesp'][1]['History_Intro_Pdesp']; ?></small>
                        </div>
                        
                    </div>
                    <div class="items items-img mt-n5 mr-auto">
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="<?php echo (!empty($data['history_pdesp'][1]['History_Img_Pdesp']))?$data['history_pdesp'][1]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][1]['History_Intro_Pdesp']; ?>">
                        </div>
                    </div>
                    <div class="items items-paragraph mr-auto d-flex justify-content-md-end">
                        <p class="px-0">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_pdesp'][1]['History_Content_Pdesp']); ?>
                        </p>
                    </div>
                </div>
            </div>

            <!-- 2010 -->
            <div class="history-right-content d-flex flex-column  px-0 mx-0">
                <div class="container-fluid px-0">
                    <div class="items mb-0 ml-auto d-flex flex-row position-relative item-title">
                        <div class=" align-self-start">
                            <h3><?php echo $data['history_pdesp'][2]['History_Title_Pdesp']; ?></h3>
                        </div>
                        <div class="item-en-name align-self-start">
                            <small><?php echo $data['history_pdesp'][2]['History_Intro_Pdesp']; ?></small>
                        </div>
                        
                    </div>
                    <div class="items items-img mt-n5 mb-0 ml-auto">
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="<?php echo (!empty($data['history_pdesp'][2]['History_Img_Pdesp']))?$data['history_pdesp'][2]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][2]['History_Intro_Pdesp']; ?>">
                        </div>
                    </div>
                    <div class="items items-img-2 mr-auto">
                        <div class="img-wrap-2 overflow-hidden d-flex flex-row">
                            <img class="w-50" src="<?php echo (!empty($data['history_pdesp'][3]['History_Img_Pdesp']))?$data['history_pdesp'][3]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][3]['History_Intro_Pdesp']; ?>">
                            <img class="w-50" src="<?php echo (!empty($data['history_pdesp'][4]['History_Img_Pdesp']))?$data['history_pdesp'][4]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][4]['History_Intro_Pdesp']; ?>">
                        </div>
                    </div>
                    <div class="items items-paragraph mx-auto">
                        <p class="px-0">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_pdesp'][2]['History_Content_Pdesp']); ?>
                        </p>
                    </div>
                </div>
            </div>

            <!-- 2015 -->
            <div class="history-right-content d-flex flex-column  px-0 mx-0">
                <div class="container-fluid px-0">
                    <div class="items item-title mb-0 ml-auto d-flex flex-row position-relative">
                        <div class="align-self-start">
                            <h3><?php echo $data['history_pdesp'][5]['History_Title_Pdesp']; ?></h3>
                        </div>
                        <div class="item-en-name align-self-start">
                            <small><?php echo $data['history_pdesp'][5]['History_Intro_Pdesp']; ?></small>
                        </div>
                        
                    </div>
                    <div class="items items-img mt-n5 ml-auto">
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="<?php echo (!empty($data['history_pdesp'][5]['History_Img_Pdesp']))?$data['history_pdesp'][5]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][5]['History_Intro_Pdesp']; ?>">
                        </div>
                    </div>
                    <div class="items items-paragraph ml-auto">
                        <p class="px-0">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_pdesp'][5]['History_Content_Pdesp']); ?>
                        </p>
                    </div>
                </div>
            </div>

            <!-- 2018 -->
            <div class="history-left-content d-flex flex-column  px-0 mx-0">
                <div class="container-fluid px-0">
                    <div class="items item-title mb-0 mr-auto d-flex flex-row-reverse position-relative">
                        <div class="align-self-start">
                            <h3><?php echo $data['history_pdesp'][6]['History_Title_Pdesp']; ?></h3>
                        </div>
                        <div class="item-en-name align-self-start">
                            <small><?php echo $data['history_pdesp'][6]['History_Intro_Pdesp']; ?></small>
                        </div>
                        
                    </div>
                    <div class="items items-img mt-n5 mr-auto">
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="<?php echo (!empty($data['history_pdesp'][6]['History_Img_Pdesp']))?$data['history_pdesp'][6]['History_Img_Pdesp']: '#'; ?>" alt="<?php echo $data['history_pdesp'][6]['History_Intro_Pdesp']; ?>">
                        </div>
                    </div>
                    <div class="items items-paragraph mr-auto d-flex justify-content-md-end">
                        <p class="px-0">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['history_pdesp'][6]['History_Content_Pdesp']); ?>
                        </p>
                    </div>
                </div>
            </div>

            <!-- BRAND VISION -->
            <div class="brand-vision-content d-flex flex-column  px-0 mx-0 mt-5">
                <div class=" px-0 bg-gray">
                    <div class="items text-center mx-auto mt-0">
                        <h3>BRAND VISION</h3>
                    </div>
                    <div class="px-0 mx-auto items d-flex flex-row brand-vision-wrap">
                        <div class="px-0 mx-0 item-content">
                            <div class="img-wrap">
                                <img class="w-100" src="<?php echo (!empty($data['history_brand'][0]['History_Img_Brand']))?$data['history_brand'][0]['History_Img_Brand']: '#'; ?>" alt="<?php echo $data['history_brand'][0]['History_Title_Brand']; ?>">
                            </div>
                            <h4 class="mb-0">創辦人<small class=""><?php echo $data['history_brand'][0]['History_Title_Brand']; ?></small></h4>
                        </div>
                        <div class="px-0 mx-0 item-paragraph">
                            <p class="px-0">
                            <?php echo str_replace("</p>", "<br/>", str_replace("<p>", '', $data['history_brand'][0]['History_Content_Brand'])); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- BLOG -->
            <div id="blog-wrap" class="row p-0 m-0 w-100" style="background-color: #fff; margin-top: 0px !important;">
                <div class="container-fluid mx-0 px-0">
                    <h3 class="col-12 text-center">
                        <span>BLOG</span>
                    </h3>
                    <div class="row px-0 blog-resp" id="blog-content">
                        <div id="blog-resp">
                        <?php for ($i=0; $i < count($data['blog_dairy']); $i++) { 
                            $day = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[2];
                            $momth = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[1];
                            $year = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[0];
                            ?>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-3 mb-2">
                                <div class="card">
                                    <div class="img-wrap">
                                        <div class="img-1x1" style="background-image: url(<?php echo (!empty($data['blog_dairy'][$i]['Blog_Img_Dairy']))?"'".$data['blog_dairy'][$i]['Blog_Img_Dairy']."'": 'assets/04_紅屋日誌/可用的圖/06.jpg'; ?>);"></div>
                                        <!-- <img src="" class="" alt="紅屋日誌"> -->
                                    </div>
                                    
                                    <div class="card-body p-0">
                                        <p class="card-text mb-2 mt-4"><small class="text-muted"><?php echo $day . ' ' . $month_en[($momth * 1) - 1] . ' ' .  $year; ?></small> </p>
                                        <h5 class="card-title mt-0 text-truncate"><a href="./Blogdetail?title=<?php echo $data['blog_dairy'][$i]['Blog_ID_Dairy']; ?>" class="text-reset"><?php echo $data['blog_dairy'][$i]['Blog_Title_Dairy']; ?></a></h5>
                                        <p class="card-paragraph text-break overflow-hidden" style=""><?php echo  strip_tags($data['blog_dairy'][$i]['Blog_Content_Dairy']);?></p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="row px-0 mx-0 text-center">
                        <a href="./Blog" class="viewmoreBt text-center mx-auto text-decoration-none">view more</a>
                    </div>
                </div>
            </div>

            
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/traffic.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>