<?php 
class houseManager {
    
    public static function query_Event($method, $args){   
        include("mod_db.php");
        date_default_timezone_set('Asia/Taipei');
        $ini_result = self::iniread(__pageroot . "core/query/sys.ini");  

        switch ($method) {            
            case 'wordList': // 詞彙列表
                $sql_inquery = $ini_result['data']['wordList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;     
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "addWords": // 新增詞彙
                $sql_inquery = $ini_result['data']['addWords']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['name'],PDO::PARAM_STR);
                $state->bindValue(2,$args['pser'],PDO::PARAM_INT);
                $state->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $conn->lastInsertId();
                    // return $state->fetchAll();
                }
            break;
            case "loadWords": // 載入單一類型之項目
                $sql_inquery = $ini_result['data']['loadWords']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "updateWords":
                $sql_inquery = $ini_result['data']['updateWords']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['name'],PDO::PARAM_STR);
                $state->bindValue(2,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(3,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return array("updatetime" => date("Y-m-d H:i:s"));
                }
            break;            
            case 'delWords': 
                $sql_inquery = $ini_result['data']['delWords']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'deltypeWords': 
                $sql_inquery = $ini_result['data']['deltypeWords']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->bindValue(2,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getSelltype':  //類型『售屋』
                $sql_inquery = $ini_result['data']['getSelltype']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getSellArea':  //區域『售屋』
                $sql_inquery = $ini_result['data']['getSellArea']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getSellDirect':  //坐向『售屋』
                $sql_inquery = $ini_result['data']['getSellDirect']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getRenttype':  //類型『租屋』
                $sql_inquery = $ini_result['data']['getRenttype']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getCity':  //縣市
                $sql_inquery = $ini_result['data']['getCity']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getArea':  //鄉鎮區
                $sql_inquery = $ini_result['data']['getArea']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['C_name'],PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "houseList": //售屋房屋列表
                $sql_inquery = $ini_result['data']['houseList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;     
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "rentHouseList": //租屋房屋列表
                $sql_inquery = $ini_result['data']['rentHouseList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;     
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "loadHouse": // 載入房屋資料
                $sql_inquery = $ini_result['data']['loadHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "loadRHouse": // 載入租屋資料
                $sql_inquery = $ini_result['data']['loadRHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "addHouse": // 新增基本房屋資料
                $sql_inquery = $ini_result['data']['addHouse']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['type'],PDO::PARAM_STR);
                $state->bindValue(2,$args['showed'],PDO::PARAM_STR);
                $state->bindValue(3,$args['title'],PDO::PARAM_STR);
                $state->bindValue(4,$args['category'],PDO::PARAM_INT);
                $state->bindValue(5,$args['structure'],PDO::PARAM_STR);
                $state->bindValue(6,$args['community'],PDO::PARAM_STR);
                $state->bindValue(7,$args['price'],PDO::PARAM_STR);
                $state->bindValue(8,$args['uni'],PDO::PARAM_STR);
                $state->bindValue(9,$args['levelground'],PDO::PARAM_STR);
                $state->bindValue(10,$args['floor'],PDO::PARAM_INT);
                $state->bindValue(11,$args['totalfloor'],PDO::PARAM_INT);
                $state->bindValue(12,$args['phone'],PDO::PARAM_STR);
                $state->bindValue(13,$args['area'],PDO::PARAM_INT);
                $state->bindValue(14,$args['age'],PDO::PARAM_STR);
                $state->bindValue(15,$args['direction'],PDO::PARAM_INT);
                $state->bindValue(16,$args['trafficeseat'],PDO::PARAM_STR);
                $state->bindValue(17,$args['address'],PDO::PARAM_STR);
                $state->bindValue(18,$args['managefee'],PDO::PARAM_INT);
                $state->bindValue(19,$args['feeuni'],PDO::PARAM_STR);
                $state->bindValue(20,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $conn->lastInsertId();
                    // return $state->fetchAll();
                }
            break;
            case 'addSellHouse':  //新增售屋資料
                $sql_inquery = $ini_result['data']['addSellHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->bindValue(2,$args['mainlg'],PDO::PARAM_STR);
                $state->bindValue(3,$args['afflg'],PDO::PARAM_STR);
                $state->bindValue(4,$args['sharelg'],PDO::PARAM_STR);
                $state->bindValue(5,$args['description'],PDO::PARAM_STR);
                $state->bindValue(6,$args['youtube'],PDO::PARAM_STR);
                $state->bindValue(7,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case 'addRentHouse':  //新增租屋資料
                $sql_inquery = $ini_result['data']['addRentHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->bindValue(2,$args['deposit'],PDO::PARAM_INT);
                $state->bindValue(3,$args['shortrent'],PDO::PARAM_STR);
                $state->bindValue(4,$args['cooked'],PDO::PARAM_STR);
                $state->bindValue(5,$args['petted'],PDO::PARAM_STR);
                $state->bindValue(6,$args['movein'],PDO::PARAM_STR);
                $state->bindValue(7,$args['description'],PDO::PARAM_STR);
                $state->bindValue(8,$args['facility'],PDO::PARAM_STR);
                $state->bindValue(9,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case "updateHouse": // 更新基本房屋資料
                $sql_inquery = $ini_result['data']['updateHouse']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['showed'],PDO::PARAM_STR);
                $state->bindValue(2,$args['status'],PDO::PARAM_STR);
                $state->bindValue(3,$args['title'],PDO::PARAM_STR);
                $state->bindValue(4,$args['category'],PDO::PARAM_INT);
                $state->bindValue(5,$args['structure'],PDO::PARAM_STR);
                $state->bindValue(6,$args['community'],PDO::PARAM_STR);
                $state->bindValue(7,$args['price'],PDO::PARAM_STR);
                $state->bindValue(8,$args['uni'],PDO::PARAM_STR);
                $state->bindValue(9,$args['levelground'],PDO::PARAM_STR);
                $state->bindValue(10,$args['floor'],PDO::PARAM_INT);
                $state->bindValue(11,$args['totalfloor'],PDO::PARAM_INT);
                $state->bindValue(12,$args['phone'],PDO::PARAM_STR);
                $state->bindValue(13,$args['area'],PDO::PARAM_INT);
                $state->bindValue(14,$args['age'],PDO::PARAM_STR);
                $state->bindValue(15,$args['direction'],PDO::PARAM_INT);
                $state->bindValue(16,$args['trafficeseat'],PDO::PARAM_STR);
                $state->bindValue(17,$args['address'],PDO::PARAM_STR);
                $state->bindValue(18,$args['managefee'],PDO::PARAM_INT);
                $state->bindValue(19,$args['feeuni'],PDO::PARAM_STR);
                $state->bindValue(20,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(21,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return array("updatetime" => date("Y-m-d H:i:s"),"status" => $args['status']);
                }
            break;
            case 'updateSellHouse':  //更新售屋資料
                $sql_inquery = $ini_result['data']['updateSellHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['mainlg'],PDO::PARAM_STR);
                $state->bindValue(2,$args['afflg'],PDO::PARAM_STR);
                $state->bindValue(3,$args['sharelg'],PDO::PARAM_STR);
                $state->bindValue(4,$args['description'],PDO::PARAM_STR);
                $state->bindValue(5,$args['youtube'],PDO::PARAM_STR);
                $state->bindValue(6,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(7,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case 'updateRentHouse':  //更新租屋資料
                $sql_inquery = $ini_result['data']['updateRentHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['deposit'],PDO::PARAM_INT);
                $state->bindValue(2,$args['shortrent'],PDO::PARAM_STR);
                $state->bindValue(3,$args['cooked'],PDO::PARAM_STR);
                $state->bindValue(4,$args['petted'],PDO::PARAM_STR);
                $state->bindValue(5,$args['movein'],PDO::PARAM_STR);
                $state->bindValue(6,$args['description'],PDO::PARAM_STR);
                $state->bindValue(7,$args['facility'],PDO::PARAM_STR);
                $state->bindValue(8,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(9,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case 'delHouse': 
                $sql_inquery = $ini_result['data']['delHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'delSellHouse': 
                $sql_inquery = $ini_result['data']['delSellHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case 'delRentHouse': 
                $sql_inquery = $ini_result['data']['delRentHouse']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case 'delHImages': 
                $sql_inquery = $ini_result['data']['delHImages']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['hid'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'houseImagesList': 
                $sql_inquery = $ini_result['data']['houseImagesList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['hid'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'loadHouseimage': 
                $sql_inquery = $ini_result['data']['loadHouseimage']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'delHouseimage': 
                $sql_inquery = $ini_result['data']['delHouseimage']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "addHouseImage":
                $sql_inquery = $ini_result['data']['addHouseImage']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['hid'],PDO::PARAM_INT);
                $state->bindValue(2,$args['sorted'],PDO::PARAM_INT);
                $state->bindValue(3,$args['cover'],PDO::PARAM_STR);
                $state->bindValue(4,$args['showed'],PDO::PARAM_STR);
                $state->bindValue(5,$args['source'],PDO::PARAM_STR);
                $state->bindValue(6,$args['filename'],PDO::PARAM_STR);
                $state->bindValue(7,$args['filename_small'],PDO::PARAM_STR);
                $state->bindValue(8,$args['type'],PDO::PARAM_STR);
                $state->bindValue(9,$args['size'],PDO::PARAM_INT);
                $state->bindValue(10,$args['dir'],PDO::PARAM_STR);
                $state->bindValue(11,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return $state->fetchAll();
                }
            break;
            case 'updateImageSorted': 
                $sql_inquery = $ini_result['data']['updateImageSorted']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['sorted'],PDO::PARAM_INT);
                $state->bindValue(2,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(3,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'removeImageCover': 
                $sql_inquery = $ini_result['data']['removeImageCover']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['hid'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return true;
                }
            break;
            case 'updateImageCover': 
                $sql_inquery = $ini_result['data']['updateImageCover']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(2,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            default:
                # code...
            break;
        }
    }
     

    //base
    private static function iniread($filename) 
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");  //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }

}
?>