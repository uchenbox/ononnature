jQuery(function($){
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
      })
      // 選到頁面中帶有 data-src 屬性的所有元素
    // const images = document.querySelectorAll('[data-src]')

    // // 設定在什麼情況下觸發 callback 函式
    // const options = {
    // rootMargin: '0px 0px 50px 0px',
    // threshold: 0.5
    // }

    // // 載入圖片的函式
    // const loadImage = (img) => {
    //     img.previousElementSibling.classList.add('loading')
    //     //  取得元素上 data-src 屬性中的圖片位址
    //     const src = img.getAttribute('data-src')
    //     if (!src) return
    //     // 設定圖片的 src 並放入圖片位址讓瀏覽器載入
    //     img.src = src
    //     // update monitor
    // }

    // // callback 函式
    // const callback = (entries, observer) => {
    // entries.forEach(entry => {
    //     // 當此圖片進入 viewport 時才載入圖片
    //     if (!entry.isIntersecting) return
    //     // 載入圖片
    //     loadImage(entry.target)
    //     // 停止觀察此圖片
    //     observer.unobserve(entry.target)
    // })
    // }

    // // 創建一個 observer
    // let observer = new IntersectionObserver(callback, options)

    // // 觀察所有圖片
    // images.forEach(image => observer.observe(image))
    
    // headerCarousel();
    // loopCarousel();
    
});

var loopCarousel = function(){
    $('.loop').owlCarousel({
        dots: true,
        center: true,
        items:1,
        loop:true,
        lazyLoad:true,
        margin:56,
        responsive:{
            1201:{
                items:2
            }
            
        }
    });
}

var headerCarousel = function(){
    var head_loop = $('.header-loop').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoHeight:true,
        items:1,
        loop:true,
        // margin:30,
        autoplay:true,
        dots: false
    });
    $('.customNextBtn').click(function() {
        head_loop.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.customPrevBtn').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        head_loop.trigger('prev.owl.carousel');
    })
}

var showmore = function(obj){
    $count = $('.gallery-item.d-none').length;
    for (let index = 0; index < 6; index++) {
        const element = $('.gallery-item.d-none:eq(0)');
        element.removeClass('d-none');
        
        $count = $('.gallery-item.d-none').length;
        if($count == 0)
        {
            obj.addClass('d-none');
            $("#mb-150").attr('style', 'margin-bottom:150px;');
        }
    }
    
}