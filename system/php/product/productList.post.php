<?php
require_once("../../include/inc.config.php");
require_once("../../include/inc.check_login.php");

$_Type  = $POST['_type'];//主執行case
$_Type1 = $POST['_type1'];//副執行選項

if( !empty($_Type) ){
	
	$db 			= new MySQL();
	
	$_TF 			= new TableFun();
			
	$json_array 	= array();
	
	$Menu_Data 		= MATCH_MENU_DATA(FUN);//取得目前目錄資料
	
	$Main_Key  		= $Menu_Data['Menu_TableKey'];//資料表主健
	
	$Main_Table 	= $Menu_Data['Menu_TableName'];//目錄使用的資料表
	
	$Main_TablePre  = $Menu_Data['Menu_TablePre'];//資料表前輟
	
	$Main_Key2		= $Menu_Data['Menu_TableKey1'];//擴充資料表主健
	
	$Main_Table2 	= $Menu_Data['Menu_TableName1'];//擴充資料表
	
	$Main_TablePre2	= $Menu_Data['Menu_TablePre1'];//擴充資料表前輟

	//====
	$Main_Key3		= $Menu_Data['Menu_TableKey2'];//分類資料表主健
	
	$Main_Table3 	= $Menu_Data['Menu_TableName2'];//分類資料表
	
	$Main_TablePre3	= $Menu_Data['Menu_TablePre2'];//分類資料表前輟

	$Main_maxLv3	= $Menu_Data['Menu_ClassMax'];//分類最大層級
	
	$DATE 			= date('Y-m-d H:i:s');
	
	include('../../include/inc.path.php');
	
	switch( $_Type ){
		
		case "Table_Re"://表格刷新
		
			$Contents 		= new Contents($POST, $GET);
	
			$_html_content	= $Contents->set_Content_List();
		break;
	
		case "Table_Del"://表格資料刪除
			
			$_html_msg  = "";
			$_ID		= $POST['tab_chks'];
						
			$Del_Arr 	= array();
			if( is_array($_ID) ){

				$Del_Arr   = $_ID;
			}else{
				
				$Del_Arr[] = $_ID;
			}
			
			//$PathTemp = $Path;
			foreach( $Del_Arr as $key => $ID ){
				
				if( Menu_Use($Menu_Data, 'delete') ){
						
					$db->Where = " WHERE " .$Main_Key. " = '" .$ID. "'";
					
					$Field = $Main_TablePre.'_Mcp';
					
					$db->query_sql($Main_Table, $Field);
					$row = $db->query_fetch();
					
					if( !empty($row) ){
						
						$_Path_ 	= $Path.$ID.DIRECTORY_SEPARATOR;
						
						$File_Name 	= $row[$Field];
						
						if( !empty($File_Name) ){
							
							$File = ICONV_CODE( 'UTF8_TO_BIG5', $_Path_.$File_Name );
							
							if( is_file($File) ){//圖片存在
							
								Delete_File($_Path_, $File_Name);
								
								if( is_file($File) ){//刪除原圖
								
									$_html_msg = '檔案刪除失敗';
									break;
								}
							}/*else{
								
								$_html_msg = '無圖片資料, 請檢查';
								break;
							}*/
						}
						
						$_Path_ 	= $Path.$ID.DIRECTORY_SEPARATOR;
						
						$Field 		= $Main_TablePre2.'_Img';
						
						$db->query_sql($Main_Table2, $Field);
						while( $prow = $db->query_fetch() ){
							
							$File_Name = $prow[$Field];
							
							$File = ICONV_CODE( 'UTF8_TO_BIG5', $_Path_.$File_Name );
							
							if( is_file($File) ){//圖片存在
							
								Delete_File($_Path_, $File_Name);
								
								if( is_file($File) ){//刪除原圖
								
									$_html_msg = '檔案刪除失敗';
									break;
								}
							}
						}
												
						if( is_dir($_Path_) ){
							
							if( @!rmdir($_Path_) ){
								
								$_html_msg = '相簿資料刪除失敗, 可能資料夾權限不足';
								break;
							}
						}
						
						$db->query_delete($Main_Table);
						$db->query_delete($Main_Table2);
					}else{
						
						$_html_msg = '無此資料';
						break;
					}
				}else{
					
					$_html_msg = '無權限刪除';
					break;
				}											
			}
			
			$db->query_optimize($Main_Table);//最佳化資料表
			
			$Contents 		= new Contents($POST, $GET);
	
			$_html_content	= $Contents->set_Content_List();
		break;
		
		case "Table_Edit"://表格編輯畫面
			
			$_ID = $POST['tab_chks'];
			$CM = new Custom();

			//抓取第一層分類
			$db = new MySQL();
			$db->Where ="Where ProductC_Lv = 1 AND ProductC_Open = 1 ";
			$db->query_sql( $Main_Table3 , 'ProductC_ID,ProductC_Name' );
			$LV1_Arr = array();
			while( $row = $db->query_fetch( '', 'assoc') ){
				
				$LV1_Arr[$row['ProductC_ID']] = $row['ProductC_Name'];
			}
			//=============

			$db = new MySQL();
			$Edit_Arr = array();
			if( is_array($_ID) ){
				
				$Edit_Arr   = $_ID;
			}else{
		
				$Edit_Arr[] = $_ID;
			}
			
			$table_info = $db->get_table_info($Main_Table);//取出資料表欄位的詳細的訊息
			
			$Class_Arr[$Main_Key3] = Class_Get('TYPE1');//抓取分類列表

			$_SignList = $CM->GET_SIGN_LIST();
			//=================

			$_html_ = array();
			if( !empty($Edit_Arr) ){
				
				foreach( $Edit_Arr as $key => $ID ){
					
					$PathUrl = $Pathweb.$ID.'/';

					$Sheet = $Main_Table." as a LEFT JOIN ".$Main_Table3." as b ON a.`ProductC_ID` = b.`ProductC_ID`";

					$db->Where = " WHERE a." .$Main_Key. " = '" .$ID. "'";
					$db->query_sql($Sheet, 'a.*, b.`ProductC_UpMID` as `ProductC_ID_LV2`');
					$row = $db->query_fetch();//取出資料
					
					$row[$Main_TablePre2.'_Img_Arr'] = array();
					$db->Order_By = ' ORDER BY a.' .$Main_TablePre2.'_Sort'. ' DESC, a.' .$Main_Key2. ' DESC';
					$db->query_sql($Main_Table2, '*');
					while( $prow = $db->query_fetch() ){
						
						$prow[$Main_TablePre2.'_Img_sUrl'] = $PathUrl.'sm_'.$prow[$Main_TablePre2.'_Img'];
						$prow[$Main_TablePre2.'_Img_bUrl'] = $PathUrl.$prow[$Main_TablePre2.'_Img'];
						
						$row[$Main_TablePre2.'_Img_Arr'][] = $prow;
					}
					
					//撈出 1 、2層 分類ID
					if( !empty($row['ProductC_ID']) ) {

						$row['ProductC_ID_LV1'] = $CM->GET_FIRST_LV_IN_PRODUCT_CATEGORY( $row['ProductC_ID'] );

						$row['ProductC_ID_LV2_List'] = $CM->GET_PRODUCT_SAME_LV_LIST( $row['ProductC_ID_LV2'] );
						$row['ProductC_ID_LV3_List'] = $CM->GET_PRODUCT_SAME_LV_LIST( $row['ProductC_ID'] );
					}

					$_html_[] = $row;
				}
			}
			
			if( empty($_html_msg) ){
				
				ob_start();
				if( $_Type1 == 'View' ){
					
					if( Menu_Use($Menu_Data, 'view') ){
						
						include_once(SYS_PATH.$Menu_Data['Menu_Path'].DIRECTORY_SEPARATOR.$Menu_Data['Menu_Exec_Name'].".view.".$Menu_Data['Exec_Sub_Name']);
						
						if( $_Type1 == 'View'){
							
							$_html_boxtype = 3;
						}
					}else{
						
						$_html_msg = '無權限檢視';
					}
				}else{
					
					include_once(SYS_PATH.$Menu_Data['Menu_Path'].DIRECTORY_SEPARATOR.$Menu_Data['Menu_Exec_Name'].".edit.".$Menu_Data['Exec_Sub_Name']);
				}
				
				$_html_content = ob_get_contents();
				ob_end_clean();
			}	
		break;
		
		case "Table_Edit_Save"://記錄編輯資料
						
			$ID_Arr = $POST[$Main_Key];

			foreach( $ID_Arr as $key => $val ){

				$Fields = REMOVE_NDATA($POST, array($Main_Key));
				
				$db_data = array();


				foreach( $Fields as $key2 => $val2 ){
					
					$Field = Turndecode($key2);
					
					if( $Field == 'Product_Signlist' || $Field == 'Product_desc1' || $Field == 'Product_desc2'){

						$val2[$key] = serialize($val2);
					}

					$db_data[$Field] = $val2[$key];
				}
				
				if( !empty($val) && !empty($db_data) ){
					
					if( Menu_Use($Menu_Data, 'edit') ){

						$db->Where = " WHERE " .$Main_Key. " = '" .$val. "'";
						
						$db->query_data($Main_Table, $db_data, 'UPDATE');
						
						if( !empty($db->Error) ){
							
							$_html_msg = '更新失敗';
							break;
						}else{
							
							$_html_msg = '更新完成';
						}
					}else{
						
						$_html_msg = '無權限編輯';
						break;
					}
				}else if( !empty($db_data) ){
										
					if( Menu_Use($Menu_Data, 'add') ){
						
						$NEW_ID = Get_Sn_Code();
						
						if( !empty($NEW_ID) ){//有開啟自訂義編號
							
							$db_data[$Main_Key] = $NEW_ID;
						}
						
						$db_data[$Main_TablePre.'_Sdate'] = $DATE;
												
						$db->query_data($Main_Table, $db_data, 'INSERT');
						
						if( !empty($db->Error) ){
							
							$_html_msg	= '新增失敗';
							break;
						}else{
							
							$val 		= !empty($NEW_ID) ? $NEW_ID : $db->query_insert_id();
							$_html_msg 	= '新增完成';
						}
					}else{
						
						$_html_msg = '無權限新增';
						break;
					}
				}
				
				if( !empty($_FILES) && !empty($val) ){//判斷有無上傳圖片
						
					foreach( $_FILES as $Fkey => $Fval ){
						
						$Field_Name = $Fkey;//欄位名稱
								
						if( $Field_Name == 'productl_Img' ){
							
							$_Path_			= $Path.$val.DIRECTORY_SEPARATOR;
													
							$PicSize_Arr 	= Get_PicSize(2, array());
							
							$_TF->TableUpload($val, 'uploadmultiple', $Field_Name, $_Path_, '', true, $PicSize_Arr);

						//表格上傳
						}else if( $Field_Name == 'Product_table1' || $Field_Name == 'Product_table2' || $Field_Name == 'Product_table3' ){
							require 'vendor/autoload.php';

							$db = new MySQL();
							$Path = $PathFile.$val.DIRECTORY_SEPARATOR;
							$Files_Key = $Fkey;//欄位名稱
							
							$ex 		= explode('.',$Fval['name']);
							$Files_Name = $ex[0];
							$Sub_Name 	= end($ex);
							$Allow_File = 'xls, xlsx, csv';
							if( !preg_match('/' .$Sub_Name. '/i', $Allow_File) ){
								
								$_html_msg  = '請上傳Excel格式檔案';
								break;
							}
								
							CHK_PATH($Path);//判斷位置是否存在
											
							$Upload = new Upload();
							
							$Upload_Name = $Upload->Upload_File($_FILES[$Files_Key], $Path);

							if( empty($Upload_Name) ){
								
								$_html_msg  = $Upload->Error;
								break;
							}

							$File_Url = $Path.$Upload_Name;

							if( is_file($File_Url) ){
								
								$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($File_Url);
								$reader->setReadDataOnly(true);
								$spreadsheet = $reader->load($File_Url);
								$sheet = $spreadsheet->getActiveSheet();
								
								//Y軸數量
								$ExcelRow 	= $sheet->getHighestRow(); // 取得總列  //幾筆資料// Y軸

								$columnH 	= $sheet->getHighestColumn(); //總行數  X軸  // 回傳英文

								$columnLookup = [
						            'A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6, 'G' => 7, 'H' => 8, 'I' => 9, 'J' => 10, 'K' => 11, 'L' => 12, 'M' => 13,
						            'N' => 14, 'O' => 15, 'P' => 16, 'Q' => 17, 'R' => 18, 'S' => 19, 'T' => 20, 'U' => 21, 'V' => 22, 'W' => 23, 'X' => 24, 'Y' => 25, 'Z' => 26,
						            'a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5, 'f' => 6, 'g' => 7, 'h' => 8, 'i' => 9, 'j' => 10, 'k' => 11, 'l' => 12, 'm' => 13,
						            'n' => 14, 'o' => 15, 'p' => 16, 'q' => 17, 'r' => 18, 's' => 19, 't' => 20, 'u' => 21, 'v' => 22, 'w' => 23, 'x' => 24, 'y' => 25, 'z' => 26,
						        ];

						        //X軸數量
								$columnCnt = $columnLookup[$columnH];

								$data = array();



								for( $i = 1; $i <= $ExcelRow; $i++ ){
									
									for( $j = 1 ; $j <=$columnCnt; $j++  ) {

										if( $i == 1 ){
											$data[$i][$j] = (string)$sheet->getCellByColumnAndRow($j, $i)->getValue();	
										}else{

											$data[$i][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
										}
									}
								}
								
								$db_data = array();
								$db_data[$Field_Name] = serialize($data);
								$db->Where = " WHERE Product_ID = '".$val."'";
								$db->query_data($Main_Table, $db_data, 'UPDATE');
								
								if( !empty($db->Error) ){
									
									$_html_msg = '資料有問題';
									break;
								}

								if( empty($_html_msg) ){
									
									$_html_msg = '匯入完成';
								}

								
							}else{
								
								$_html_msg = '找不到檔案';
							}

							
						}else{
							
							$_Path_			= $Path.$val.DIRECTORY_SEPARATOR;
							
							$PicSize_Arr 	= Get_PicSize(1, array());
							
							$_TF->TableUpload($val, 'uploadimg', $Field_Name, $_Path_, $Field_Name.$val, true, $PicSize_Arr);
						}
						
						if( !empty($_TF->Msg) ){
							
							$_html_msg = !empty($_html_msg) ? $_html_msg.', '.$_TF->Msg : $_TF->Msg;
							break;
						}
					}
					
					if( empty($_TF->Msg) ){
						
						$_html_msg = !empty($_html_msg) ? $_html_msg.', 上傳完成' : '上傳完成';
					}
				}
			}

			$_html_eval		= 'Return_Table()';//重新載入返回
			
			$Contents 		= new Contents($POST, $GET);
	
			$_html_content	= $Contents->set_Content_List();
		break;
		
		case "Table_Data_Change"://切換資料
			
			$_ID 	= $POST['id'];
			$_Field	= Turndecode($POST['field']);
			$_Data	= $POST['data'];
			
			$_Key	= $Main_Key;
			$_Table	= $Main_Table;
			if( $_Type1 == 'delimgs' || $_Type1 == 'numbers' || $_Type1 == 'checkboxs' ){
				
				$_Key	= $Main_Key2;
				$_Table	= $Main_Table2;
			}
			
			if( $_Type1 == 'delimg' || $_Type1 == 'delimgs' || $_Type1 == 'delfile' ){
						
				if( $_Type1 == 'delfile' ){
					
					$_Path_ = $PathFile;
				}else if( $_Type1 == 'delimg' || $_Type1 == 'delimgs' ){
					
					if( $_Type1 == 'delimgs' ){
						
						$db->Where = " WHERE " .$Main_Key2. " = '" .$db->val_check($_ID). "'";
						$db->query_sql($Main_Table2, $Main_Key.', '.$_Field);
					}else{
						
						$db->Where = " WHERE " .$Main_Key. " = '" .$db->val_check($_ID). "'";
						$db->query_sql($Main_Table, $Main_Key.', '.$_Field);
					}
					
					$row = $db->query_fetch();
					
					$_Path_ = $Path.$row[$Main_Key].DIRECTORY_SEPARATOR;
				}	
				
				if( empty($row) ){
				
					$_html_msg = '資料錯誤請檢查';
					break;
				}				
			}
			
			$Data_Arr  = array(
				'Table' => $_Table,
				'Key' 	=> $_Key,
				'ID' 	=> $_ID,
				'Field' => $_Field,
				'Data' 	=> $_Data
			);
							
			$_TF->TableChange($_Type1, $Data_Arr, $_Path_);
			
			if( !empty($_TF->Msg) ){
				
				$_html_msg = $_TF->Msg;
			}
		break;
		
		case 'UploadExcel':
			
			require 'vendor/autoload.php';
			$db = new MySQL();
			$Path = $PathFile;
			
			$Files_Key = key($_FILES);//欄位名稱
			
			$ex 		= explode('.',$_FILES[$Files_Key]['name']);
			$Files_Name = $ex[0];
			$Sub_Name 	= end($ex);
			
			$Allow_File = 'xls, xlsx, csv';
			if( !preg_match('/' .$Sub_Name. '/i', $Allow_File) ){
				
				$_html_msg  = '請上傳Excel格式檔案';
				break;
			}
				
			CHK_PATH($Path);//判斷位置是否存在
							
			$Upload = new Upload();
			
			$Upload_Name = $Upload->Upload_File($_FILES[$Files_Key], $Path, 'import-excel');
			
			if( empty($Upload_Name) ){
				
				$_html_msg  = $Upload->Error;
				break;
			}
			/*** */
			$File_Url = $Path.$Upload_Name;
			if( is_file($File_Url) ){
				
				$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($File_Url);
				$reader->setReadDataOnly(true);
				$spreadsheet = $reader->load($File_Url);
				$sheet = $spreadsheet->getActiveSheet();
				
				$ExcelRow 	= $sheet->getHighestRow(); // 取得總列  //幾筆資料//
				
				for( $i = 2; $i <= $ExcelRow; $i++ ){
					
					$db_data = array();
					
					$News_Title			= (string)$sheet->getCellByColumnAndRow(1, $i)->getValue();
					$News_PostDate		= $sheet->getCellByColumnAndRow(2, $i)->getValue();
					$News_EndDate		= $sheet->getCellByColumnAndRow(3, $i)->getValue();
					$News_Content		= $sheet->getCellByColumnAndRow(4, $i)->getValue();
					$_html_msg = $News_Title;
					// $db_data['News_ID'] 			= '';
					$db_data['News_Title'] 			= $News_Title;
					$db_data['News_PostDate'] 		= $News_PostDate;
					$db_data['News_EndDate'] 		= $News_EndDate;
					$db_data['News_Content'] 		= $News_Content;
					$db_data['News_Sort'] 			= 0;
					$db_data['News_Open'] 			= 1;
					
					// if( empty($Barcode_Arr[$Product_Barcode.'-'.$Product_CustomCode]) && !empty($Product_Barcode) ){
						
						$NEW_ID = Get_Sn_Code();
									
						/*if( !empty($NEW_ID) ){//有開啟自訂義編號
							
							$db_data[$Main_Key] = $NEW_ID;$_html_msg = $NEW_ID;
						break;	
						}
						*/
						$db_data[$Main_TablePre.'_Sdate'] = $DATE;
						
						$db->query_data($Main_Table, $db_data, 'INSERT');
						
						if( !empty($db->Error) ){
							
							$_html_msg = '資料有問題';
							break;
						}
						//$_html_msg .= $UID.':'.$Name.':'.$Tel.'<br>';
					// }
				}
				
				if( empty($_html_msg) ){
					
					$_html_msg = '匯入完成';
				}
				
				$_html_eval = 'Reload()';
			}else{
				
				$_html_msg = '找不到檔案';
			}
		break;
									
		case "Table_Return"://表格返回
						
			$json_array = Get_Top_Return($Menu_Data);
		break;
	}
	
	ob_end_clean();
	
	$json_array['html_msg']     = $_html_msg ? $_html_msg : '';//訊息
	$json_array['html_href']    = $_html_href ? $_html_href : '';//連結
	$json_array['html_eval']    = $_html_eval ? $_html_eval : '';//確定後要執行的JS
	$json_array['html_content'] = $_html_content ? $_html_content : '';//輸出內容
	$json_array['html_boxtype'] = $_html_boxtype ? $_html_boxtype : '1';//彈出視窗格式1=>只有確定,2=>確定與取消
	$json_array['html_clear']   = $_html_clear ? $_html_clear : '';//清空欄位Array,填欄位ID
	$json_array['html_type'] 	= $_Type ? $_Type : '';//執行類型
	$json_array['html_type1'] 	= $_Type1 ? $_Type1 : '';//副執行類型
	
	echo json_encode( $json_array );
}
?>