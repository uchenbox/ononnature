
//-------------------------------------------------------------//
function Init(){
	
	var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable;

    /* initialize the external events
    -----------------------------------------------------------------*/

    var containerEl = document.getElementById('external-events');
	
    new Draggable(containerEl, {
      itemSelector: '.fc-event',
      eventData: function(eventEl) {
        return {
          title: 	eventEl.innerText.trim(),
		  id : 		eventEl.getAttribute("data-id"),
        }
      }
    }
	
	);
	
}

function Init_Calendar(){
	
	var this_workplace = $(".this_workplace").val();
	
	var Calendar = FullCalendar.Calendar;
	
	
    /* initialize the calendar
    -----------------------------------------------------------------*/
	var _thisdate = "";
	var Today=new Date();
    var calendarEl = document.getElementById('calendar');
	
	$("#calendar").empty();
	
    var calendar = new Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
      now: new Date(),
      editable: true, // enable draggable events
      droppable: true, // this allows things to be dropped onto the calendar
      aspectRatio: 1.8,
      header: {
        right: 'today prev,next',
        center: 'title',
        left: ''
      },
      locale: 'zh-TW',
      defaultView: 'dayGridMonth',
	  events: {
		url: 'ajax/get_equ_handover.php?wp='+this_workplace,
		data: function(result) { 
		  return result;
		}
	  },
	  //https://fullcalendar.io/docs/v3/events-json-feed
      drop: function(arg) {
		
		Init_Calendar();
		
        console.log('drop date: ' + arg.dateStr)

        if (arg.resource) {
          console.log('drop resource: ' + arg.resource.id)
        }
		 _thisdate = arg.dateStr;
      },
	  eventClick: function(calEvent, jsEvent, view) {
			
			var _update =  { 
				id : calEvent.event.id, 
				title : calEvent.event.title
			} ;
		
			Update_Data( _update );
		},
      eventReceive: function(arg) { // called when a proper external event is dropped
	  
		var _insert =  { 
			id : arg.event.id , 
			date : _thisdate, 
			title : arg.event.title,
			workplace : this_workplace
		} ;
		
		Save_Data( _insert );
		
      },
      eventDrop: function(arg) { // called when an event (already on the calendar) is moved
        console.log('eventDrop', arg.event);
      }
    });
    calendar.render();

}


function Save_Data( _data ){
	  

	$.ajax({
		url: 'ajax/save_equ_handover.php',
		data: _data,
		type: "POST",
		async: false,
		dataType: 'json',
		success: function(result){
			
			Init_Calendar();
		},
		error:function(xhr, ajaxOptions, thrownError){ 

			alert( thrownError );
		}
	});
}

function Update_Data( _data ){
	  
	$.ajax({
		url: 'ajax/update_equ_handover.php',
		data: _data,
		type: "POST",
		async: false,
		dataType: 'json',
		success: function(result){
			
			Init_Calendar();
		},
		error:function(xhr, ajaxOptions, thrownError){ 

			alert( thrownError );
		}
	});
}

function CHG_WORKPLACE( _this ){
	
	var _val = _this.val();
	
	Init_Calendar( _val );
}
