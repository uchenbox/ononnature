$(function(){
    // showHeaderVideo();
})
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
// 列表
var goToList = function(){
    
}

// 載入header影片
var showHeaderVideo = function()
{
    $.ajax({
        type: 'POST',
        url: 'api/getHomeHeader',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data.data[0];
            
            $("#header-video").attr('src', postdata['Home_FileHeader']);
        }

    });
}

 var loadTop3Sell = function(){
    $.ajax({
        type: 'POST',
        url: 'route/webcontroller.php?action=houseListTop3',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data.data;
            switch(data.message){
                case "failed":
                    // toastr.error('<h6>資料載入失敗</h6>','載入錯誤');
                    $("#buyrecommand .bhouse-carousel").html(`<h1 class="display-4 text-center">無相關房屋資料</h1> `);
                                       
                break;
                case "success":
                    let str = "";

                    for(let i = 0 ; i < postdata.length ; i++)
                    {                        
                        str += `<a class="text-reset text-decoration-none" href="House?act=bdetail&itemno=${postdata[i].ser}&title=${postdata[i].title}"><div class="card item" style="width: 18rem;">
                                <img src="${postdata[i].picfile.replace("../", "")}" class="card-img-top owl-lazy" data-src="${postdata[i].picfile.replace("../", "")}">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6 font-2x">${postdata[i].title}</div>
                                        <div class="col-6 text-right text-danger font-2x">${postdata[i].price}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 text-success">${postdata[i].levelground}</div>
                                        <div class="col-6 text-right text-muted">${postdata[i].direct}<br>${postdata[i].age}</div>
                                    </div>
                                </div>
                            </div></a>`;
                    }

                    $("#buyrecommand .bhouse-carousel").html(str);
                    $('.bhouse-carousel').owlCarousel({
                        center: false,
                        loop:false,
                        lazyLoad:true,
                        margin:10,
                        nav:true,
                        responsive:{
                            0:{
                                items:1,
                                lazyLoadEager:2
                            },
                            600:{
                                items:2,
                                lazyLoadEager:1
                            },
                            1000:{
                                items:3
                            }
                        }
                    })
                break;
            }
        }

    });
}

var loadTop3Rent = function(){
    $.ajax({
        type: 'POST',
        url: 'route/webcontroller.php?action=rentListTop3',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data.data;
            switch(data.message){
                case "failed":
                    // toastr.error('<h6>資料載入失敗</h6>','載入錯誤');
                    $("#rentrecommand .shownodata").html('<h1 class="display-4 text-center bg-light">無相關租屋資料</h1>');
                                       
                break;
                case "success":
                    let str = "";

                    for(let i = 0 ; i < postdata.length ; i++)
                    {                        
                        str += `<a class="text-reset text-decoration-none" href="House?act=rdetail&itemno=${postdata[i].ser}&title=${postdata[i].title}"><div class="card item" style="width: 18rem;">
                                <img src="${postdata[i].picfile.replace("../", "")}" class="card-img-top owl-lazy" data-src="${postdata[i].picfile.replace("../", "")}">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6 font-2x">${postdata[i].title}</div>
                                        <div class="col-6 text-right text-danger font-2x">${postdata[i].price}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 text-success">${postdata[i].levelground}</div>
                                        <div class="col-6 text-right text-muted">${(postdata[0].structure.split(':')[0] != "")?postdata[0].structure.split(':')[0]+"房":""}${(postdata[0].structure.split(':')[1] != "")?postdata[0].structure.split(':')[1]+'廳':''}${(postdata[0].structure.split(':')[2] != "")?postdata[0].structure.split(':')[2]+"衛":""}${postdata[0].structure.split(':')[3]}</div>
                                    </div>
                                </div>
                            </div></a>`;
                    }

                    $("#rentrecommand .rhouse-carousel").html(str);
                    $('.rhouse-carousel').owlCarousel({
                        center: false,
                        loop:false,
                        lazyLoad:true,
                        margin:10,
                        nav:true,
                        responsive:{
                            0:{
                                items:1,
                                lazyLoadEager:2
                            },
                            600:{
                                items:2,
                                lazyLoadEager:1
                            },
                            1000:{
                                items:3
                            }
                        }
                    })
                break;
            }
        }

    });
}
