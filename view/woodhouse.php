<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-2.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0 overflow-hidden"> 
        <!-- 首頁大圖 -->
        <div id="image-wrap" class="row p-0 m-0 overflow-hidden">
            <div class="owl-carousel owl-theme header-loop  overflow-hidden">
                <?php for ($i=0; $i < count($data['walk_header']); $i++) { ?>
                    <div class="item">
                        <img class="h-100" src="<?php echo (!empty($data['walk_header'][$i]['Walkhouse_Img']))?$data['walk_header'][$i]['Walkhouse_Img']: ''; ?>" alt="<?php echo $alt; ?>">
                        <div id="title-wrap"  class="d-flex d-column align-self-center ">
                            <!-- <div class="mr-3"> -->
                                <?php //echo str_replace('。', '<br/>。', str_replace('，', '<br/>，', str_replace('p>', 'div class="mr-3">', $data['walk_header'][$i]['Walkhouse_Intro']))); ?>
                            <!-- </div> -->
                            <!-- <div class="mr-3">青銀自在 <br/>。</div> -->
                            <!-- <div>輕盈住宅<br/>，</div> -->
                        </div>
                        <div id="nav-wrap"  class="d-flex d-column align-self-center ">
                            <p><span class="customPrevBtn"> < </span> <span><?php echo ($i + 1) ?>/<?php echo count($data['walk_header']);?></span> <span class="customNextBtn"> > </span></p>
                        </div>
                    </div>
                <?php } ?>
                <!-- <div class="item">
                    <img class="h-100" src="assets/01_行動木屋/00_header/01.jpg" alt="ononNature行動木屋">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div>
                <div class="item">
                    <img class="h-100" src="assets/01_行動木屋/00_header/02.jpg" alt="ononNature行動木屋">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div>
                <div class="item">
                    <img class="h-100" src="assets/01_行動木屋/00_header/03.jpg" alt="ononNature行動木屋">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div>
                <div class="item">
                    <img class="h-100" src="assets/01_行動木屋/00_header/04.jpg" alt="ononNature行動木屋">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div> -->
            </div>
            <!-- <img src="assets/01_行動木屋/00_header/01.jpg" alt="ononNature行動木屋" class="w-100"> -->
            <div id="title-wrap"  class="d-flex d-column align-self-center ">
                <?php echo str_replace('。', '<br/>。', str_replace('，', '<br/>，', str_replace('p>', 'div class="mx-2">', $data['walk_desp'][0]['Walkhouse_Intro_Desp']))); ?>
                <!-- <div class="mr-3">青銀自在 <br/>。</div> -->
                <!-- <div>輕盈住宅<br/>，</div> -->
            </div>
            
        </div>       
        <!-- 行動木屋 -->
        <div id="introhouse-wrap"  class="row p-0 m-0 w-100 overflow-hidden">
            <div class="container-fluid mx-0 px-0 overflow-hidden">
                <div class="w-100" id="introhouse-content">
                    <div class="row p-0 m-0">
                        <div id="introhouse" class="overflow-hidden">
                            <div class="row p-0 m-0">
                                <div class="col-sm-6 px-0 item">
                                    <div class="paragraph">
                                        <?php
                                        echo '<h3>' .preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_video'][0]['Walkhouse_Title_video'])  . '</h3>';
                                        echo '<p>' . preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_video'][0]['Walkhouse_Content_video']) . '</p>';
                                        ?>
                                        <!-- <h3>行動木屋</h3>
                                        <h3>2nd Home, 2nd Life</h3> -->
                                        <!-- <p>解放坪數迷思，預算更自在<br> 數千萬買百坪空屋？大可不必！<br> 為退休族輕鬆打造第二人生.<br> 讓年輕人看見理想家園，開創築巢新可能！</p> -->
                                    </div>
                                </div>
                                <div class="col-sm-6 px-0 item">
                                    <video  autoplay muted loop>
                                        <source src="<?php echo (isset($data['walk_video'][0]['Walkhouse_File_video']))?$data['walk_video'][0]['Walkhouse_File_video']:'assets/01_行動木屋/01_概述/product.mp4'; ?>" type="video/mp4" >
                                        Your browser does not support the video tag.
                                    </video>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row p-0 m-0  justify-content-end">
                        <div class="col-md-9 p-0 bg-gray">
                            <div id="title-wrap"  class="d-flex d-column align-self-center ">
                                <div class="mr-3">擁抱山林純木空間 <br/>。</div>
                                <div>告別都市冰冷水泥<br/>，</div>
                            </div>
                        </div>                    
                    </div>       
                </div>
                <!-- 木屋介紹 -->
                <div class="w-100 introhouse-content-2 first-child" >
                    <div class="row p-0 m-0">
                        <div class="col-lg-7 col-md-8 p-0 img-content">
                            <img class="w-100" src="<?php echo (isset($data['walk_wp001'][0]['Walkhouse_Img_Cover']))?$data['walk_wp001'][0]['Walkhouse_Img_Cover']:'assets/01_行動木屋/02_風景材/01.jpg'; ?>" alt="<?php echo $data['walk_wp001'][0]['Walkhouse_Title_Desp']; ?>">
                            <div class="desp-wrap float-none">
                                <p>
                                    <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_wp001'][0]['Walkhouse_Content_Desp']); ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-4 p-0 d-flex flex-column title-content">
                            <div class="title-wrap">
                                <div class="text-center float-left big-word-wrap">
                                    <span class="big-word"><?php echo $data['walk_wp001'][0]['Walkhouse_Title_Desp']; ?></span>
                                </div>
                                <span class="small-word mx-1"><?php echo $data['walk_wp001'][0]['Walkhouse_Intro_Desp']; ?></span>
                            </div>                    
                            <div class="desp-wrap float-none">
                                <p>
                                    <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_wp001'][0]['Walkhouse_Content_Desp']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="w-100 introhouse-content-2 " >
                    <div class="row p-0 m-0">
                        <div class="col-lg-7 col-md-8 p-0 img-content">
                            <img class="w-100" src="<?php echo (isset($data['walk_wp001'][1]['Walkhouse_Img_Cover']))?$data['walk_wp001'][1]['Walkhouse_Img_Cover']:'assets/01_行動木屋/02_風景材/02.jpg'; ?>" alt="<?php echo $data['walk_wp001'][1]['Walkhouse_Title_Desp']; ?>">
                            <div class="desp-wrap float-none">
                                <p>
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_wp001'][1]['Walkhouse_Content_Desp']); ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-4 p-0 d-flex flex-column title-content">
                            <div class="title-wrap">
                                <div class="text-center float-left big-word-wrap">
                                    <span class="big-word"><?php echo $data['walk_wp001'][1]['Walkhouse_Title_Desp']; ?></span>
                                </div>
                                <span class="small-word mx-1"><?php echo $data['walk_wp001'][1]['Walkhouse_Intro_Desp']; ?></span>
                            </div>                    
                            <div class="desp-wrap float-none">
                                <p>
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_wp001'][1]['Walkhouse_Content_Desp']); ?>
                                </p>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
                <div class="w-100 introhouse-content-2 last-child" >
                    <div class="row p-0 m-0">
                        <div class="col-lg-7 col-md-8 p-0 img-content">
                            <img class="w-100" src="<?php echo (isset($data['walk_wp001'][2]['Walkhouse_Img_Cover']))?$data['walk_wp001'][2]['Walkhouse_Img_Cover']:'assets/01_行動木屋/02_風景材/03.jpg'; ?>" alt="<?php echo $data['walk_wp001'][2]['Walkhouse_Title_Desp']; ?>">
                            <div class="desp-wrap float-none">
                                <p>
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_wp001'][2]['Walkhouse_Content_Desp']); ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-4 p-0 d-flex flex-column title-content">
                            <div class="title-wrap">
                                <div class="text-center float-left big-word-wrap">
                                    <span class="big-word"><?php echo $data['walk_wp001'][2]['Walkhouse_Title_Desp']; ?></span>
                                </div>
                                <span class="small-word mx-1"><?php echo $data['walk_wp001'][2]['Walkhouse_Intro_Desp']; ?></span>
                            </div>                    
                            <div class="desp-wrap float-none">
                                <p>
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_wp001'][2]['Walkhouse_Content_Desp']); ?>
                                </p>
                            </div>                        
                        </div>
                    </div>                
                </div>
                <!-- 縮時吊裝 -->
                <div class="w-100 px-0 " id="installhouse-content">
                    <div class="container-fluid mx-0 px-0 ">
                        <div class="row p-0 m-0 response-hidden">
                            <div class="col-md-9 bg-gray position-absolute"></div>
                        </div>
                        <div class="row p-0 m-0  justify-content-start">
                            <div class="col-md-12 p-0 "  id="installhouse-title">
                                <div class="row px-0 mx-0">
                                    <div class="col-md-5 p-0 desp-col">
                                        <div class="title-wrap">
                                            <div class="text-center float-left big-word-wrap">
                                                <span class="big-word">3</span>
                                            </div>
                                            <span class="small-word mx-1">小時</span>
                                        </div>                    
                                        <div class="desp-wrap ">
                                            <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", str_replace('p>', 'h3>', $data['walk_video'][1]['Walkhouse_Title_video'])) ?>
                                            <p class="response-hidden">
                                            <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_video'][1]['Walkhouse_Content_video']); ?>
                                            </p>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-7 p-0 video-col">
                                        <div class="video-wrap">
                                            <div id="installhouse">
                                                <div class="row px-0 mx-0">
                                                    <div class="col px-0 mx-0">
                                                        <video class="w-100"  autoplay muted loop>
                                                            <source src="<?php echo (isset($data['walk_video'][1]['Walkhouse_File_video']))?$data['walk_video'][1]['Walkhouse_File_video']:'assets/01_行動木屋/03_三小時吊裝/constructing.mp4'; ?>" type="video/mp4" >
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                </div>  
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <!-- <div class="col-md-12 ">
                                <div class="video-wrap">
                                    <div id="installhouse">
                                        <div class="row">
                                            <div class="col">
                                                <video class="w-100"  autoplay muted loop>
                                                    <source src="assets/01_行動木屋/03_三小時吊裝/constructing.mp4" type="video/mp4" >
                                                    Your browser does not support the video tag.
                                                </video>
                                            </div>
                                        </div>  
                                    </div> 
                                </div>
                                
                            </div>                  -->
                        </div>
                        <div class="row p-0 m-0 response-show">
                            <div class="col-md-9 bg-gray position-absolute">
                                <div class="desp-wrap">
                                    <p>
                                    <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['walk_video'][1]['Walkhouse_Content_video']); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                        
                </div>
                <!-- 設計生活 -->
                <div class="row px-0 mx-0" id="designlive-content"> 
                    <div class="container-fluid mx-0 px-0">
                        <div class="col-12">
                            <h3 class="text-center">DESIGN & LIVING</h3>
                        </div>
                        <?php
                            $sizes = 1;
                            foreach ($data['walk_dltheme'] as $item) {
                                $pd = 0;
                                echo '<div class="col-12 mb-5 px-0 mx-0 carousel-content">
                                            <div class="owl-carousel owl-theme loop">';
                                foreach ($data['walk_dlimg'] as $dlimg) {
                                    if(strpos($item['WH_ID'], $dlimg['WH_ID']) !== false )
                                    {
                                        echo '<div class="item">
                                                <div class="d-flex">
                                                    <div class="flex-shrink-1 carousel-content-title">
                                                        <span>'.$dlimg['WH_Title_dlimg'].'</span>
                                                    </div>
                                                    <div class="w-100">
                                                    <img class="" src="'.((isset($dlimg['WH_Img_dlimg']))?$dlimg['WH_Img_dlimg']:'assets/01_行動木屋/04_設計生活/05_陽光衛浴/01.jpg').'" alt="'.$dlimg['WH_Title_dlimg'].'">
                                                    </div>
                                                </div>
                                            </div>';
                                    }
                                }
                                echo '      </div>
                                    </div>';

                                echo '<div class="col-12">
                                            <h3 class="text-center title-center mt-0">
                                                '.$item['WH_Name'].'
                                            </h3>
                                            <p class="content-center mx-auto">
                                               '.$item['WH_Content'].'
                                            </p>
                                        </div>';
                            }
                        ?>
                        
                    </div>           
                    
                </div>
                <!-- 結構構造 -->
                <div class="row px-0 mx-0 w-100" id="construct-content">
                    <div class="container-fluid px-0 mx-0">
                        <div class="d-flex flex-row construct-wrap">
                            <div class="img-content response-hidden">
                                <!-- <img src="assets/01_行動木屋/05_結構構造/00_section.png" class="" alt=""> -->
                            </div>
                            <div class="d-flex flex-column pdesp-content ">
                                <div class="px-0 mx-0 item-title">
                                    <h3>CONSTRUCTION</h3>
                                </div> 
                                <?php
                                    foreach ($data['walk_strutheme'] as $item) {
                                        echo ' <div class="px-0 mx-0 item">      
                                                <div class="d-flex flex-row align-items-center">
                                                    <div class=" item-big-word">
                                                       '.$item['WH_Name'].'
                                                    </div>
                                                    <div class=" item-imgs d-flex">';
                                        foreach ($data['walk_struimg'] as $struimg) {
                                            if(strpos($item['WH_ID'], $struimg['WH_ID']) !== false )
                                            {
                                                echo '<div class="item-img">                                            
                                                        <a href="'.((isset($struimg['WH_Img_strimg']))?$struimg['WH_Img_strimg']:'assets/01_行動木屋/05_結構構造/01_天壁地/01.jpg').'" data-lightbox="sky" data-title="'.$struimg['WH_Title_strimg'].'">
                                                            
                                                            <img class="img-1x1" src="'.((isset($struimg['WH_Img_strimg']))?$struimg['WH_Img_strimg']:'assets/01_行動木屋/05_結構構造/01_天壁地/01.jpg').'" alt="'.$struimg['WH_Title_strimg'].'">
                                                        </a>
                                                    </div>';
                                            }
                                        }

                                        echo '</div>
                                                    </div>
                                                    <div class="item-paragraph px-0">
                                                        <p class="">
                                                            '.$item['WH_Content'].'
                                                        </p>
                                                    </div>
                                                </div>';
                                    }
                                ?>
                                
                                <p class="text-muted comment-paragraph">
                                    ※2021年初版，實際規格本公司保留更新權利。
                                </p>
                            </div>
                            <div class="img-content response-show">
                                <img src="assets/01_行動木屋/05_結構構造/00_section.png" class="" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-0 m-0 w-100" id="bg-gray-title">
                    <div class="container-fluid p-0 d-flex justify-content-center align-items-center">
                        <div class="text-center">
                            <h4>適合台灣氣候的</h4>
                            <h3>“熱帶建築”</h3>
                        </div>
                    </div>
                </div>
                <!-- 熱帶建築 -->
                <div class="row p-0 m-0 w-100" id="hot-build-content">
                    <div class="container-fluid p-0 ">
                        <div class="d-flex flex-row p-0 m-0 item">
                            <div class="item-img">
                                <img class="w-100" src="<?php echo ((isset($data['walk_wp004'][0]['Walkhouse_Img_Cover']))?$data['walk_wp004'][0]['Walkhouse_Img_Cover']:'assets/01_行動木屋/05_結構構造/02_熱帶建築/01.jpg'); ?>" alt="<?php echo $data['walk_wp004'][0]['Walkhouse_Title_Desp']; ?>">
                            </div>
                            <div class="item-paragraph ">
                                <div class="paragraph-content d-flex flex-column">
                                    <div>
                                        <h3><?php echo $data['walk_wp004'][0]['Walkhouse_Title_Desp']; ?></h3>
                                    </div>
                                    <div>
                                        <p>
                                        <?php echo $data['walk_wp004'][0]['Walkhouse_Content_Desp']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row p-0 m-0 item">
                            <div class="item-img order-sm-1">
                                <img class="w-100" src="<?php echo ((isset($data['walk_wp004'][1]['Walkhouse_Img_Cover']))?$data['walk_wp004'][1]['Walkhouse_Img_Cover']:'assets/01_行動木屋/05_結構構造/02_熱帶建築/01.jpg'); ?>" alt="<?php echo $data['walk_wp004'][0]['Walkhouse_Title_Desp']; ?>" alt="<?php echo $data['walk_wp004'][1]['Walkhouse_Title_Desp']; ?>">
                            </div>
                            <div class="item-paragraph reverse">
                                <div class="paragraph-content d-flex flex-column">
                                    <div>
                                        <h3><?php echo $data['walk_wp004'][1]['Walkhouse_Title_Desp']; ?></h3>
                                    </div>
                                    <div>
                                        <p>
                                        <?php echo $data['walk_wp004'][1]['Walkhouse_Content_Desp']; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row p-0 m-0 item">
                            <div class="item-img">
                                <img class="w-100" src="<?php echo ((isset($data['walk_wp004'][2]['Walkhouse_Img_Cover']))?$data['walk_wp004'][2]['Walkhouse_Img_Cover']:'assets/01_行動木屋/05_結構構造/02_熱帶建築/01.jpg'); ?>" alt="<?php echo $data['walk_wp004'][2]['Walkhouse_Title_Desp']; ?>">
                            </div>
                            <div class="item-paragraph">
                                <div class="paragraph-content d-flex flex-column">
                                    <div>
                                        <h3><?php echo $data['walk_wp004'][2]['Walkhouse_Title_Desp']; ?></h3>
                                    </div>
                                    <div>
                                        <p>
                                        <?php echo $data['walk_wp004'][2]['Walkhouse_Content_Desp']; ?>
                                        </p>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <div class="d-flex flex-row p-0 m-0 item">
                            <div class="item-img order-sm-1">
                                <img class="w-100" src="<?php echo ((isset($data['walk_wp004'][3]['Walkhouse_Img_Cover']))?$data['walk_wp004'][3]['Walkhouse_Img_Cover']:'assets/01_行動木屋/05_結構構造/02_熱帶建築/01.jpg'); ?>" alt="<?php echo $data['walk_wp004'][3]['Walkhouse_Title_Desp']; ?>">
                            </div>
                            <div class="item-paragraph reverse">
                                <div class="paragraph-content d-flex flex-column">
                                    <div>
                                        <h3><?php echo $data['walk_wp004'][3]['Walkhouse_Title_Desp']; ?></h3>
                                    </div>
                                    <div>
                                        <p>
                                        <?php echo $data['walk_wp004'][3]['Walkhouse_Content_Desp']; ?>
                                        </p>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- 地震颱風 -->
                <div class="d-block px-0 mx-0 earthquake-content ">
                    <div class="px-0 d-flex bg bg1" style="background: url('<?php echo ((isset($data['walk_wp011'][0]['Walkhouse_Img_Cover']))?$data['walk_wp011'][0]['Walkhouse_Img_Cover']:''); ?>') no-repeat;background-size: cover;">
                        <div class="desp-content">
                            <h3 class="mx-0"><?php echo $data['walk_wp011'][0]['Walkhouse_Title_Desp']; ?></h3>
                            <p><?php echo $data['walk_wp011'][0]['Walkhouse_Content_Desp']; ?></p>
                        </div>
                    </div>
                </div>
                <div class="d-block px-0 mx-0 earthquake-content ">
                    <div class="px-0 d-flex bg bg2" style="background: url('<?php echo ((isset($data['walk_wp011'][1]['Walkhouse_Img_Cover']))?$data['walk_wp011'][1]['Walkhouse_Img_Cover']:''); ?>') no-repeat;background-size: cover;">
                        <div class="desp-content">
                            <h3 class="mx-0"><?php echo $data['walk_wp011'][1]['Walkhouse_Title_Desp']; ?></h3>
                            <p><?php echo $data['walk_wp011'][1]['Walkhouse_Content_Desp']; ?></p>
                        </div>
                    </div>
                </div>
                <!-- 精選案例 -->
                <div class="row p-0 mx-0 w-100" id="cases-content">
                    <div class="cases-wrap">
                        <div class="row px-0 mx-0 item-title">
                            <div class="col-12 text-center">
                                <h3 class="mb-0">GALLERY</h3>
                            </div>
                        </div>
                        <div class="row p-0 m-0 items mx-auto">
                            <?php 
                                $gallery_count = 1;
                                foreach ($data['walk_gallery'] as $item) {
                                    echo '<div class=" item gallery-item '.(($gallery_count > 6)?'d-none':'').'">
                                            <a href="'.((isset($item['Walkhouse_Img_Gellery']))?$item['Walkhouse_Img_Gellery']:'assets/01_行動木屋/06_精選案例/01.jpg').'" data-lightbox="sky" data-title="'.$item['Walkhouse_Title_Gellery'].'">
                                                <img class="img-1x1" src="'.((isset($item['Walkhouse_Img_Gellery']))?$item['Walkhouse_Img_Gellery']:'assets/01_行動木屋/06_精選案例/01.jpg').'" alt="'.$item['Walkhouse_Title_Gellery'].'">
                                            </a>
                                        </div>';
                                    $gallery_count++;
                                }
                            ?>
                            
                        </div>
                        <div class="row p-0 m-0">
                            <div class="viewmoreBt text-center mx-auto" onclick="showmore($(this));">view more</div>
                            <div id="mb-150"></div>
                        </div>
                    </div>
                </div>
                <!-- 預約參訪 -->
                <div class="row px-0 mx-0 order-content ">
                    <div class="container-fluid p-0 d-flex flex-column justify-content-center align-items-center bg3">
                        <div>
                            <h3 class="mb-0 pb-0 text-center"><?php echo $data['walk_reserve'][0]['Walkhouse_Title']; ?></h3>
                            <p class="mx-0"><?php echo $data['walk_reserve'][0]['Walkhouse_Content']; ?></p>
                        </div>
                        <div class="mx-0">
                            <a href="./Housevisit" class="text-reset text-decoration-none">
                                <div class="gotoorderBt text-center mx-auto"><span>預約參訪</span></div>
                            </a>
                            
                        </div>
                    </div>
                    <div class="w-100 position-absolute bg-video" >
                        <video  autoplay muted loop class="">
                            <source src="<?php echo ((isset($data['walk_reserve'][0]['Walkhouse_File']) && !empty($data['walk_reserve'][0]['Walkhouse_File']))?$data['walk_reserve'][0]['Walkhouse_File']:'#') ?>" type="video/mp4" >
                            Your browser does not support the video tag.
                        </video>
                    </div>  
                </div>
                <!-- 分頁連結 -->
                <div class="row px-0 mx-0 other-page-content ">
                    <div class="px-0 mx-auto d-flex flex-row items">
                        <div class="px-0 item">
                            <a href="<?php echo ((isset($data['walk_link'][0]['Walkhouse_Link_Link']) && !empty($data['walk_link'][0]['Walkhouse_Link_Link']))?$data['walk_link'][0]['Walkhouse_Link_Link']:'#') ?>" class="text-reset">
                                <div class="d-flex flex-column item-img-wrap mx-auto">
                                    <div class="item-img text-center">
                                        <img src="<?php echo ((isset($data['walk_link'][0]['Walkhouse_Img_Link']))?$data['walk_link'][0]['Walkhouse_Img_Link']:'assets/01_行動木屋/07_分頁連結/01.jpg'); ?>" class="" alt="<?php echo $data['walk_link'][0]['Walkhouse_Title_Link'] ?>">
                                    </div>
                                    <div class="text-center p-0 m-0">
                                        <h3><?php echo $data['walk_link'][0]['Walkhouse_Title_Link'] ?></h3>
                                    </div>
                                </div>
                            </a>                            
                        </div>
                        <div class="px-0 item">
                            <a href="<?php echo ((isset($data['walk_link'][1]['Walkhouse_Link_Link']) && !empty($data['walk_link'][1]['Walkhouse_Link_Link']))?$data['walk_link'][1]['Walkhouse_Link_Link']:'#') ?>" class="text-reset">
                                <div class="d-flex flex-column item-img-wrap  mx-auto" >
                                    <div class="item-img text-center">
                                        <img src="<?php echo ((isset($data['walk_link'][1]['Walkhouse_Img_Link']))?$data['walk_link'][1]['Walkhouse_Img_Link']:'assets/01_行動木屋/07_分頁連結/01.jpg'); ?>" class="" alt="<?php echo $data['walk_link'][1]['Walkhouse_Title_Link'] ?>" >
                                    </div>
                                    <div class="text-center  p-0 m-0">
                                        <h3><?php echo $data['walk_link'][1]['Walkhouse_Title_Link'] ?></h3>
                                    </div>
                                </div>
                            </a>                            
                        </div>
                    </div>
                </div>
                <!-- FAQ -->
                <div class="px-0 mx-0 faq-content ">
                    <div class="">
                        <div class="row px-0 mx-0 item-title">
                            <div class="col-12">
                                <h3 class="text-center px-0 mx-0">FAQ</h3>
                            </div>
                        </div> 
                        <div class="d-flex px-0 mx-0 item-content">
                            <div class="accordion" id="accordionExample">
                                <?php 
                                $i = 0;
                                foreach ($data['walk_faq'] as $item) {
                                    echo '<div class="card">
                                        <div class="card-header p-0" id="heading'.$i.'">
                                            <h2 class="mb-0 p-0 collapsed" data-toggle="collapse" data-target="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'">
                                            '.$item['Faq_Intro_Answer'].'
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapse'.$i.'" class="card-collapse collapse" aria-labelledby="heading'.$i.'" data-parent="#accordionExample">
                                            <div class="card-body">
                                            '.$item['Faq_Content_Answer'].'
                                            </div>
                                        </div>
                                    </div>';
                                    $i++;
                                }
                                ?>                                    
                            </div>
                        </div>
                        <div class="row p-0 m-0">
                            <a href="./Faq" class="viewmoreBt text-center mx-auto text-decoration-none">view more</a>
                        </div>
                    </div>
                </div>
                <!-- BLOG -->
                <div id="blog-wrap" class="row p-0 m-0 w-100">
                    <div class="container-fluid mx-0 px-0">
                        <h3 class="col-12 text-center">
                            <span>BLOG</span>
                        </h3>
                        <div class="row px-0 blog-resp" id="blog-content">
                            <div id="blog-resp">
                            <?php for ($i=0; $i < count($data['blog_dairy']); $i++) { 
                                $day = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[2];
                                $momth = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[1];
                                $year = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[0];
                                ?>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-3 mb-2">
                                    <div class="card">
                                        <div class="img-wrap">
                                            <div class="img-1x1" style="background-image: url(<?php echo (!empty($data['blog_dairy'][$i]['Blog_Img_Dairy']))?"'".$data['blog_dairy'][$i]['Blog_Img_Dairy']."'": 'assets/04_紅屋日誌/可用的圖/06.jpg'; ?>);"></div>
                                            <!-- <img src="" class="" alt="紅屋日誌"> -->
                                        </div>
                                        
                                        <div class="card-body p-0">
                                            <p class="card-text mb-2 mt-4"><small class="text-muted"><?php echo $day . ' ' . $month_en[($momth * 1) - 1] . ' ' .  $year; ?></small> </p>
                                            <h5 class="card-title mt-0 text-truncate"><a href="./Blogdetail?title=<?php echo $data['blog_dairy'][$i]['Blog_ID_Dairy']; ?>" class="text-reset"><?php echo $data['blog_dairy'][$i]['Blog_Title_Dairy']; ?></a></h5>
                                            <p class="card-paragraph text-break overflow-hidden" style=""><?php echo  strip_tags($data['blog_dairy'][$i]['Blog_Content_Dairy']);?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                            <!-- <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                                <div class="card">
                                    <div class="img-wrap">
                                        <div class="img-1x1" style="background-image: url('assets/04_紅屋日誌/可用的圖/06.jpg');"></div>
                                    </div>
                                    
                                    <div class="card-body p-0">
                                        <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                        <h5 class="card-title mt-0 text-truncate">自由，從思考居住空間開始自由，從思考居住空間開始自由，從思考居住空間開始自由，從思考居住空間開始</h5>
                                        <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                                <div class="card">
                                    <div class="img-wrap">
                                        <div class="img-1x1" style="background-image: url('assets/01_行動木屋/06_精選案例/01.jpg');"></div>
                                    </div>
                                    
                                    <div class="card-body p-0">
                                        <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                        <h5 class="card-title mt-0 text-truncate">自由，從思考居住空間開始</h5>
                                        <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                                <div class="card">
                                    <div class="img-wrap">
                                        <div class="img-1x1" style="background-image: url('assets/01_行動木屋/06_精選案例/02.jpg');"></div>
                                    </div>
                                    
                                    <div class="card-body p-0">
                                        <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                        <h5 class="card-title mt-0 text-truncate" >自由，從思考居住空間開始</h5>
                                        <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                                <div class="card">
                                    <div class="img-wrap">
                                        <div class="img-1x1" style="background-image: url('assets/01_行動木屋/06_精選案例/03.jpg');"></div>
                                    </div>
                                    
                                    <div class="card-body p-0">
                                        <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                        <h5 class="card-title mt-0 text-truncate">自由，從思考居住空間開始</h5>
                                        <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="row px-0 mx-0 text-center">
                            <a href="./Blog" class="viewmoreBt text-center mx-auto text-decoration-none">view more</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
      </div>
      <div id="wrap-footer" >
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/jquery-1.11.3.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script>
        jQuery(function($){
            $('.loop').owlCarousel({
                dots: true,
                center: true,
                items:1,
                loop:true,
                lazyLoad:true,
                margin:56,
                responsive:{
                    1201:{
                        items:2
                    }
                    
                }
            });
            var head_loop = $('.header-loop').owlCarousel({
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                autoHeight:true,
                items:1,
                loop:true,
                // margin:30,
                autoplay:true,
                dots: false
            });
            $('.customNextBtn').click(function() {
                head_loop.trigger('next.owl.carousel');
            })
            // Go to the previous item
            $('.customPrevBtn').click(function() {
                // With optional speed parameter
                // Parameters has to be in square bracket '[]'
                head_loop.trigger('prev.owl.carousel');
            })
        });
    </script>

    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    
    <script src="dist/script/main.js"></script>
    
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script>
        $('#wrap-body').imagesLoaded()
            .always( function( instance ) {
                console.log('all images loaded');
            });
    </script>   
    
      <!-- <script src="dist/script/init.js"></script> -->
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/woodhouse.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>