<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
            <!-- 文章內容 -->
            <div class="blogs-detail-content d-flex flex-row  px-0 ">
                <div class="px-0 ml-auto  blog-article flex-fill">
                    <div class="d-flex flex-column p-0 m-0 blog-article-wrap">
                        <div class="traffic-items item-1">
                            <div class="img-wrap">
                                <img class="w-100 mb-0" src="<?php echo ((!empty($blog['Blog_Img_Dairy']))?$blog['Blog_Img_Dairy']: '#') ?>" alt="<?php echo $blog['Blog_Title_Dairy'] ?>">
                            </div>
                                <h3 class="mb-0"><?php echo $blog['Blog_Title_Dairy'] ?></h3>
                        </div>
                        <div class="traffic-items item-2">
                            <div class="bg-gray float-right">
                                <?php
                                $day = explode('-', substr( $blog['Blog_Sdate_Dairy'] , 0 , 10 ))[2];
                                $momth = explode('-', substr( $blog['Blog_Sdate_Dairy'] , 0 , 10 ))[1];
                                $year = explode('-', substr( $blog['Blog_Sdate_Dairy'] , 0 , 10 ))[0];
                                ?>
                                <h5 class="mt-n4"><?php echo $day . ' ' . $month_en[($momth * 1) - 1] . ' ' .  $year.' | '.$blog['Blog_Name'] ?> </h5>
                            </div>
                        </div>
                    </div>
                    <div class="item d-flex flex-column p-5 my-5 ">
                        <div class="ml-auto">
                            
                            <p class="blog-desp mx-0"><?php echo str_replace("</p>", "<br/>", str_replace("<p>", '', $blog['Blog_Content_Dairy'])) ?></p>
                            <div class="ref mx-0">
                                <h3>參考資料：</h3>
                                <p>
                                    <?php echo str_replace("</p>", "<br/>", str_replace("<p>", '', $blog['Blog_File_Dairy'])) ?>
                                    <!-- <ul>
                                        <li>隈研吾著、鍾瑞芳譯（2014）。《小建築》。臺北市：五南。</li>
                                        <li>Heavener, Brent（2019）:Tiny House: Live Small, Dream Big. Clarkson Potter Publishers, New York</li>
                                    </ul> -->
                                </p>
                            </div>
                            <div class="other-article mx-0">
                                <h3>相關文章：</h3>
                                <p>
                                    <ul>
                                        <?php 
                                            echo (isset($data['blogdairy_prev'][0]['pre_ID']))?'<li><a href="./Blogdetail?title='.$data['blogdairy_prev'][0]['pre_ID'].'" class="text-reset"> 上一篇：'.$data['blogdairy_prev'][0]['Blog_Title_Dairy'].'</a></li>':''; 

                                            echo (isset($data['blogdairy_next'][0]['next_ID']))?'<li><a href="./Blogdetail?title='.$data['blogdairy_next'][0]['next_ID'].'" class="text-reset"> 下一篇：'.$data['blogdairy_next'][0]['Blog_Title_Dairy'].'</a></li>':'';
                                        ?>
                                        
                                        <!-- <li><a href="#" class="text-reset">下一篇：今天想吃炸雞</a></li> -->
                                    </ul>
                                </p>
                            </div>
                            <div class="recommand-article">
                                <h3>推薦文章：</h3>
                                <div class="row px-0 blog-resp" id="blog-content">
                                <div id="blog-resp">
                                <?php for ($i=0; $i < count($data['blog_dairy']); $i++) { 
                                $day = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[2];
                                $momth = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[1];
                                $year = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[0];
                                ?>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-4 mb-2">
                                        <div class="card">
                                            <div class="img-wrap">
                                                <div class="img-1x1" style="background-image: url(<?php echo (!empty($data['blog_dairy'][$i]['Blog_Img_Dairy']))?"'".$data['blog_dairy'][$i]['Blog_Img_Dairy']."'": '#'; ?>);"></div>
                                            </div>
                                            
                                            <div class="card-body p-0">
                                                <p class="card-text mb-2 mt-4"><small class="text-muted"><?php echo $day . ' ' . $month_en[($momth * 1) - 1] . ' ' .  $year; ?></small> </p>
                                                <h5 class="card-title mt-0 text-truncate"><a href="./Blogdetail?title=<?php echo $data['blog_dairy'][$i]['Blog_ID_Dairy']; ?>" class="text-reset"><?php echo $data['blog_dairy'][$i]['Blog_Title_Dairy']; ?></a></h5>
                                                <p class="card-paragraph text-break overflow-hidden" style="max-height:140px;"><?php echo  strip_tags($data['blog_dairy'][$i]['Blog_Content_Dairy']);?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="px-0 mx-auto blog-link flex-fill px-0 response-hidden">
                    <div class="link-content">
                        <dl class="row border-bottom px-0 position-relative d-block">
                            <dt class="col-12">近期文章</dt>
                            <?php
                            foreach ($data['blog_dairyrecent'] as $recent) {
                                echo '<dd class="col-12 text-truncate " style="max-width: 250px;"><a class="text-decoration-none text-reset " href="./Blogdetail?title='.$recent['Blog_ID_Dairy'].'">'.$recent['Blog_Title_Dairy'].'</a></dd>';
                            }
                            ?>
                        </dl>
                        <dl class="row border-bottom px-0  position-relative d-block">
                            <dt class="col-12">文章分類</dt>
                            <?php
                            foreach ($data['blog_themelv1'] as $theme) {
                                echo ' <dd class="col-12">
                                        <a href="./Blog?theme='.$theme['Blog_ID'] .'" class="text-reset text-decoration-none">'.$theme['Blog_Name'] .'</a>';
                                        echo '<dl class="row sub my-0">';
                                        foreach ($data['blog_theme'] as $child) {
                                            if($theme['Blog_ID'] == $child['Blog_UpMID'])
                                            {
                                                echo '<dd class="col-12"><a href="./Blog?theme='.$child['Blog_ID'] .'" class="text-reset text-decoration-none">'.$child['child_name'].'</a></dd>';
                                                break;
                                            }
                                            else
                                            {
                                                echo '';
                                            }
                                        }
                                           
                                        echo '</dl>';
                                echo '</dd>';
                            }
                            echo '<dd class="col-12"><a href="./Blog?theme=null" class="text-reset text-decoration-none">未分類</a></dd>';
                            ?>
                        </dl>
                        <dl class="row position-relative px-0  d-block">
                            <dt class="col-12">標籤</dt>
                            <?php
                            $filter_lb = array();
                            $stick_str = '';
                            foreach ($data['blog_labels'] as $labels) {
                                $stick_str .= ($labels['Blog_Intro_Dairy'] . ',');
                            }
                            str_replace(' ', '', $stick_str);
                            $filter_lb = array_unique(explode(',', $stick_str));
                            echo '<div class="row px-3">';
                            foreach ($filter_lb as $lb) {
                                if(isset($lb) && !empty($lb))
                                {
                                    echo '<dd class="col-3"><a href="./Blog?label='.trim($lb) .'" class="text-reset text-decoration-none">'.$lb.'</a></dd>';
                                }
                                
                            }
                            echo '</div>';
                            ?>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="blogs-link-content d-flex flex-column  px-0 response-show">                
                <div class="px-0 mx-auto blog-link flex-fill px-0">
                    <div class="link-content-2">
                        <dl class="row border-bottom px-0 position-relative d-block">
                            <dt class="col-12">近期文章</dt>
                            <?php
                            foreach ($data['blog_dairyrecent'] as $recent) {
                                echo '<dd class="col-12 text-truncate " style="max-width: 250px;"><a class="text-decoration-none text-reset " href="./Blogdetail?title='.$recent['Blog_ID_Dairy'].'">'.$recent['Blog_Title_Dairy'].'</a></dd>';
                            }
                            ?>
                        </dl>
                        <dl class="row border-bottom px-0  position-relative d-block">
                            <dt class="col-12">文章分類</dt>
                            <?php
                            foreach ($data['blog_themelv1'] as $theme) {
                                echo ' <dd class="col-12">
                                        <a href="./Blog?theme='.$theme['Blog_ID'] .'" class="text-reset text-decoration-none">'.$theme['Blog_Name'] .'</a>';
                                        echo '<dl class="row sub my-0">';
                                        foreach ($data['blog_theme'] as $child) {
                                            if($theme['Blog_ID'] == $child['Blog_UpMID'])
                                            {
                                                echo '<dd class="col-12"><a href="./Blog?theme='.$child['Blog_ID'] .'" class="text-reset text-decoration-none">'.$child['child_name'].'</a></dd>';
                                                break;
                                            }
                                            else
                                            {
                                                echo '';
                                            }
                                        }
                                           
                                        echo '</dl>';
                                echo '</dd>';
                            }
                            echo '<dd class="col-12"><a href="./Blog?theme=null" class="text-reset text-decoration-none">未分類</a></dd>';
                            ?>
                        </dl>
                        <dl class="row position-relative px-0  d-block">
                            <dt class="col-12">標籤</dt>
                            <?php
                            $filter_lb = array();
                            $stick_str = '';
                            foreach ($data['blog_labels'] as $labels) {
                                $stick_str .= ($labels['Blog_Intro_Dairy'] . ',');
                            }
                            str_replace(' ', '', $stick_str);
                            $filter_lb = array_unique(explode(',', $stick_str));
                            echo '<div class="item-labels px-3">';
                            foreach ($filter_lb as $lb) {
                                if(isset($lb) && !empty($lb))
                                {
                                    echo '<dd class="col-3 pl-0"><a href="./Blog?label='.trim($lb) .'" class="text-reset text-decoration-none">'.$lb.'</a></dd>';
                                }
                                
                            }
                            echo '</div>';
                            ?>
                        </dl>
                    </div>
                </div>
            </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/blog.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>