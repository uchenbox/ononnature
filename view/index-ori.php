<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/style/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column ">
      <div id="wrap-header">
        <?php require('layout/Header.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" > 
          <!-- 首頁影片 -->
        <div id="video-wrap" class="row p-0 m-0">
            <video  autoplay muted loop>
                <source src="assets/00_首頁/00_header/video.mp4" type="video/mp4" >
                Your browser does not support the video tag.
            </video>
        </div>
        <!-- 快速連結 -->
        <div id="link-wrap" class="row p-0 m-0">
            <div class="col p-0">
                <div class="link-content bg1">
                    <div class="cover">
                        <div class="ms-3 text-white text-center">
                            <p class="link-title p-0 m-0">Mobile Home</p>
                            <p class="link-subtitle p-0 m-0">行動紅屋</p>
                            <p class="link-description pt-1 m-0">行動木屋、移動式組合屋</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col p-0">
                <div class="link-content bg2">
                    <div class="cover">
                        <div class="ms-3 text-white text-center">
                            <p class="link-title p-0 m-0">OnOnNature</p>
                            <p class="link-subtitle p-0 m-0">泱泱自然</p>
                            <p class="link-description pt-1 m-0">食、住、農體驗園區</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 紅屋介紹 -->
        <div id="intro-wrap">
            <div class="container">
                <h3 class="text-center">自然．紅屋</h3>
                <p class="text-center mx-auto mb-5">
                匯集「食住農」的整合，提出「人文回村」的體驗學習活動。適逢疫情橫行，行動木屋又多了一個大任務，「防疫逃城」。原本是為退休人找新幸福，如今更為眾人安身養命。 
                </p>
                <p class="text-center mx-auto">
                匯集「食住農」的整合，提出「人文回村」的體驗學習活動。適逢疫情橫行，行動木屋又多了一個大任務，「防疫逃城」。適逢疫情橫行，行動木屋又多了一個大任務，「防疫逃城」。
                </p>
                <div class="row">
                    <div class="viewmoreBt text-center mx-auto">view more</div>
                </div>
            </div>
        </div>
        <!-- BLOG -->
        <div id="blog-wrap">
            <h3 class="text-center">
                <span>BLOG</span>
            </h3>
            <div class="row px-0 " id="blog-content">
                <div class="col-3 mb-2">
                    <div class="card">
                        <div class="img-wrap">
                            <img src="assets/04_紅屋日誌/可用的圖/06.jpg" class="" alt="紅屋日誌">
                        </div>
                        
                        <div class="card-body p-0">
                            <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                            <h5 class="card-title mt-0">自由，從思考居住空間開始</h5>
                            <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                        </div>
                    </div>
                </div>
                <div class="col-3 mb-2">
                    <div class="card">
                        <div class="img-wrap">
                            <img src="assets/01_行動木屋/06_精選案例/01.jpg" class="" alt="紅屋日誌">
                        </div>
                        
                        <div class="card-body p-0">
                            <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                            <h5 class="card-title mt-0">自由，從思考居住空間開始</h5>
                            <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                        </div>
                    </div>
                </div>
                <div class="col-3 mb-2">
                    <div class="card">
                        <div class="img-wrap">
                            <img src="assets/01_行動木屋/06_精選案例/02.jpg" class="" alt="紅屋日誌">
                        </div>
                        
                        <div class="card-body p-0">
                            <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                            <h5 class="card-title mt-0">自由，從思考居住空間開始</h5>
                            <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                        </div>
                    </div>
                </div>
                <div class="col-3 mb-2">
                    <div class="card">
                        <div class="img-wrap">
                            <img src="assets/01_行動木屋/06_精選案例/03.jpg" class="" alt="紅屋日誌">
                        </div>
                        
                        <div class="card-body p-0">
                            <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                            <h5 class="card-title mt-0">自由，從思考居住空間開始</h5>
                            <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="viewmoreBt text-center mx-auto">view more</div>
            </div>
        </div>
      </div>
      <div id="wrap-footer" >
        <?php require('layout/Footer.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/index.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>