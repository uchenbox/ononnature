<?php

class Custom{
	
	
	
	var $_Data = array();
	
	
	function __construct( ){
		
		
		
	}
	
	//************************************************************************************************************
	//                                                首頁
	//************************************************************************************************************
	//---輪播
	
	function GET_BANNER(){
		
		global $text_arrange_class;

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_banner";
		}else{
			
			$Sheet 	= "web_banner";	
		}
		
		$db = new MySQL();
		
		$db->Where = " WHERE Banner_Open = 1 ";
		
		$_Field = "*";
		
		$db->query_sql( $Sheet , $_Field );
		
		while( $row = $db->query_fetch( '', 'assoc') ){
			
			if( $row['Banner_select_arrange'] > 0 ){
				$row['arrange_class'] = $text_arrange_class[$row['Banner_select_arrange']];	
			}else{
				$row['arrange_class'] = '';
			}

			$row['mobile_src'] 		= Banner_Url.$row['Banner_Mcp2'];
			$row['website_src'] 	= Banner_Url.$row['Banner_Mcp'];
			
			$_data[$row['Banner_ID']] = $row;
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Banner_Title'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Banner_Title'] );
		}
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}
	
	//************************************************************************************************************
	//                                                最新消息
	//************************************************************************************************************
	//---最新消息列表
	
	function GET_NEWS( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_news";
		}else{
			
			$Sheet 	= "web_news";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `News_Open` = 1 ";
		$db->Where .= " AND `News_public_date` <= NOW() ";
		if( isset($Input['id']) && $Input['id'] > 0 ) {

			$db->Where .= " AND `News_ID` = ".$Input['id'];
		}
		
		$_Field = "*";
		$db->Order_By = "ORDER BY `News_Sort`";
		
		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
				$row['img_src'] = News_Url.$row['News_Mcp'];
				$row['date_format'] = date('Y.m.d',strtotime($row['News_public_date']));
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['News_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['News_Title'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				$row['img_src'] = News_Url.$row['News_Mcp'];
				$row['date_format'] = date('Y.m.d',strtotime($row['News_public_date']));
				$_data[$row['News_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['News_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['News_Title'] );
			}
		}
		
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	//************************************************************************************************************
	//                                                關於我們
	//************************************************************************************************************
	
	function GET_ABOUT_CATEGROY( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_aboutclass";
		}else{
			
			$Sheet 	= "web_aboutclass";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `AboutC_Open` = 1 ";

		if( isset($Input['id']) && $Input['id'] > 0 ) {

			$db->Where .= " AND `AboutC_ID` = ".$Input['id'];
		}
		
		$_Field = "*";
		$db->Order_By = 'ORDER BY AboutC_Sort';
		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['AboutC_Name'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['AboutC_Name'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data[$row['AboutC_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['AboutC_Name'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['AboutC_Name'] );
			}
		}
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	function GET_ABOUT_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_about";
		}else{

			$Sheet 	= "web_about";	
		}
		
		$db = new MySQL();
		
		$db->Where = " WHERE `About_Open` = 1 ";
		
		if( isset($Input['AboutC_ID']) && $Input['AboutC_ID'] != '' ) {

			$db->Where .= " AND `AboutC_ID` = '".$Input['AboutC_ID']."'";
		}
		
		$_Field = "*";
		$db->Order_By = 'ORDER BY About_Sort';
		$db->query_sql( $Sheet , $_Field );

		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){

				if( !empty($row['About_Mcp']) ) {
					$row['img_url'] = About_Url.$row['About_Mcp'];	
				}
				
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['About_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['About_Content'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				if( !empty($row['About_Mcp']) ) {
					$row['img_url'] = About_Url.$row['About_Mcp'];	
				}

				$_data[$row['About_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['About_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['About_Content'] );
			}
		}
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	//************************************************************************************************************
	//                                                技術專區
	//************************************************************************************************************
	
	function GET_TECH_CATEGROY( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_techclass";
		}else{
			
			$Sheet 	= "web_techclass";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `TechC_Open` = 1 ";

		if( isset($Input['id']) && $Input['id'] > 0 ) {

			$db->Where .= " AND `TechC_ID` = ".$Input['id'];
		}
		
		$_Field = "*";
		$db->Order_By = 'ORDER BY TechC_Sort';
		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['TechC_Name'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['TechC_Name'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data[$row['TechC_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['TechC_Name'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['TechC_Name'] );
			}
		}
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	function GET_TECH_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_tech";
		}else{

			$Sheet 	= "web_tech";	
		}
		
		
		$db = new MySQL();
		
		$db->Where = " WHERE `Tech_Open` = 1 ";
		
		if( isset($Input['category']) && $Input['category'] != '' ) {

			$db->Where .= " AND `TechC_ID` = '".$Input['category']."'";
		}
		if( isset($Input['id']) && $Input['id'] > 0 ) {

			$db->Where .= " AND `Tech_ID` = '".$Input['id']."'";
		}

		$_Field = "*";
		$db->Order_By = 'ORDER BY Tech_Sort';
		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){

				if( !empty($row['Tech_Mcp']) ) {
					$row['img_url'] = Tech_Url.$row['Tech_Mcp'];	
				}
				
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Tech_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Tech_Content'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				if( !empty($row['Tech_Mcp']) ) {
					$row['img_url'] = Tech_Url.$row['Tech_Mcp'];	
				}

				$_data[$row['Tech_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Tech_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Tech_Content'] );
			}
		}
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	//************************************************************************************************************
	//                                                服務項目
	//************************************************************************************************************
	
	function GET_SERVICE( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_service";
		}else{
			
			$Sheet 	= "web_service";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `Service_Open` = 1 ";

		if( isset($Input['id']) && $Input['id'] > 0 ) {

			$db->Where .= " AND `Service_ID` = ".$Input['id'];
		}
		
		$_Field = "*";
		$db->Order_By = 'ORDER BY Service_Sort';

		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Service_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Service_Title'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				for ($i=1; $i <= 3; $i++) { 

					if( !empty($row['Service_Img'.$i]) ) {
						$row['Service_Img'.$i.'_src'] = Service_Url.$row['Service_Img'.$i];	
					}
				}
				
				$_data[$row['Service_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Service_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Service_Title'] );
			}
		}
		
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	//************************************************************************************************************
	//                                                下載專區
	//************************************************************************************************************
	//檔案下載
	function GET_DOWNLOAD_FILES( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_download";
		}else{
			
			$Sheet 	= "web_download";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `Download_Open` = 1 ";

		$_Field = "*";
		$db->Order_By = 'ORDER BY Download_Sort';

		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
				
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Download_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Download_Title'] );
			}
		}else{

			$sn =1;
			while( $row = $db->query_fetch( '', 'assoc') ){
				
				$row['sn'] = $sn;
				$row['Download_File_ori'] = $row['Download_File'];

				if( is_file( ICONV_CODE( 'UTF8_TO_BIG5', Download_Path.$row['Download_File'] )) ){

					$row['Download_File'] = urlencode( Turnencode('?t=web_download&k=Download_ID&f=Download_File&i='.$row['Download_ID'].'&p='.Download_Path, 'downfile') );
				}
				
				$_data[$row['Download_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Download_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Download_Title'] );

				$sn++;
			}
		}
		
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	//檔案下載
	function GET_VIDEOS_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_videos";
		}else{
			
			$Sheet 	= "web_videos";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `Videos_Open` = 1 ";

		$_Field = "*";
		$db->Order_By = 'ORDER BY Videos_Sort';

		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Videos_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Videos_Title'] );
			}
		}else{

			$sn =1;
			while( $row = $db->query_fetch( '', 'assoc') ){
				
				$row['sn'] = $sn;
				
				$_data[$row['Videos_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Videos_Title'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Videos_Title'] );

				$sn++;
			}
		}
		
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	//************************************************************************************************************
	//                                                產品專區
	//************************************************************************************************************

	//產品列表
	function GET_PRODUCT_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_product";
		}else{
			
			$Sheet 	= "web_product";
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `Product_Open` = 1 ";

		//從第一、二層搜尋
		if( isset($Input['category_array']) && !empty($Input['category_array']) ) {

			$sn = 0;
			foreach ($Input['category_array'] as $key => $val) {
				if( $sn == 0){

					$temp_sql = "'".$val."'";
				}else{
					$temp_sql .= ",'".$val."'";
				}
				$sn++;
			}
			$db->Where .= " AND ProductC_ID IN (".$temp_sql.") ";
		}

		//從第三層搜尋
		if( isset($Input['ProductC_ID']) && !empty($Input['ProductC_ID']) ) {

			$db->Where .= " AND ProductC_ID = '".$Input['ProductC_ID']."'";
		}

		//header搜尋
		if( isset($Input['search']) && !empty($Input['search']) ) {

			$db->Where .= " AND ( Product_Name like '%".$Input['search']."%'";
			$db->Where .= " OR Product_Name2 like '%".$Input['search']."%' ";
			$db->Where .= " OR Product_Content like '%".$Input['search']."%' ";
			$db->Where .= " OR Product_Code like '%".$Input['search']."%' ";
			$db->Where .= " OR Product_desc1 like '%".$Input['search']."%' ";
			$db->Where .= " OR Product_desc2 like '%".$Input['search']."%' ";
			$db->Where .= " OR Product_Intro like '%".$Input['search']."%' )";
		}

		$_Field = "*";

		$db->Order_By = 'ORDER BY Product_Sort';

		$db->query_sql( $Sheet , $_Field );
		
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Product_Name'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Product_Name'] );
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				if( !empty($row['Product_Mcp']) ) {

					$row['mcp_src'] = Product_Url.$row['Product_ID'].'/'.$row['Product_Mcp'];
				}

				$_data[$row['Product_ID']] = $row;
				
				$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['Product_Name'] , 'K' );
				$_SEO['WO_Description'] .= GET_HEAD_KD( $row['Product_Name'] );
			}
		}
		
		$_rs['Data'] 			= $_data;
		$_rs['SEO'] 			= $_SEO;
		
		return $_rs;
	}

	function GET_PRODUCT_DETAIL( $ID ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 		= "en_web_product";
			$Sheet2 	= "en_web_productl";
		}else{
			
			$Sheet 		= "web_product";	
			$Sheet2 	= "web_productl";
		}


		$db = new MySQL();
		
		$db->Where = " WHERE `Product_ID` = '".$ID."'";

		//$db->Where .= " AND ProductC_UpMID = '".$input['ProductC_ID']."'";

		$db->query_sql( $Sheet , '*' );

		$_data = array();
		if( $row = $db->query_fetch( '', 'assoc') ){
			
			$_data = $row;
			//產品圖
			$db = new MySQL();
			$db->Where = " WHERE `Product_ID` = '".$ID."'";
			$db->query_sql( $Sheet2 , '*' );
			while( $row = $db->query_fetch( '', 'assoc') ){

				$row['img_src'] = Product_Url.$ID.'/'.$row['productl_Img'];
				$_data['img_list'][$row['productl_ID']] = $row;
			}

			//標章
			$sign_list = unserialize($_data['Product_Signlist']);
			$_data['sign_list'] = array();
			$db = new MySQL();
			$db->Where = " WHERE `ProSign_Open` = 1 ";
			$db->Where .= " AND `ProSign_ID` IN (".implode(',', $sign_list).") ";
			$db->query_sql( 'web_product_sign' , 'ProSign_Img' );
			while( $row = $db->query_fetch( '', 'assoc') ){

				$_data['sign_list'][] = Sign_Url.$row['ProSign_Img'];
			}

			//產品說明
			$Product_desc1 = unserialize($_data['Product_desc1']);
			$Product_desc2 = unserialize($_data['Product_desc2']);
			foreach ($Product_desc1 as $key => $val) {

				$_data['desc_list'][] = array('title' => $val , 'content' => $Product_desc2[$key]);
			}

			//表格
			$_data['Product_table1'] = unserialize($_data['Product_table1']);
			$_data['Product_table2'] = unserialize($_data['Product_table2']);
			$_data['Product_table3'] = unserialize($_data['Product_table3']);
		}

		return $_data;
	}

	//使用第二層ID取得 第三層分類ID
	function GET_SAME_PRODUCT_CATEGORY_IN_LV2( $input = array() ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_productclass";
		}else{
			
			$Sheet 	= "web_productclass";	
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `ProductC_Open` = 1 ";
		$db->Where .= " AND ProductC_UpMID = '".$input['ProductC_ID']."'";

		$db->query_sql( $Sheet , 'ProductC_ID' );
		$_data = array();
		while( $row = $db->query_fetch( '', 'assoc') ){
			
			$_data[$row['ProductC_ID']] = $row['ProductC_ID'];
		}

		return $_data;
	}

	//使用第一層ID取得 所有第三層分類ID
	function GET_SAME_PRODUCT_CATEGORY_IN_LV1( $input = array() ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "`en_web_productclass` as a ";
			$Sheet .= " LEFT JOIN `en_web_productclass` as b ON a.`ProductC_UpMID` = b.`ProductC_ID`";
			$Sheet .= " LEFT JOIN `en_web_productclass` as c ON b.`ProductC_UpMID` = c.`ProductC_ID`";
		}else{
			
			$Sheet 	= "web_productclass as a ";
			$Sheet .= " LEFT JOIN `web_productclass` as b ON a.`ProductC_UpMID` = b.`ProductC_ID`";
			$Sheet .= " LEFT JOIN `web_productclass` as c ON b.`ProductC_UpMID` = c.`ProductC_ID`";
		}

		$db = new MySQL();
		$db->Where = " WHERE c.`ProductC_Open` = 1 AND b.`ProductC_Open` = 1 AND a.`ProductC_Open` = 1 ";
		$db->Where .= " AND c.`ProductC_ID` = '".$input['ProductC_ID']."'";
		$db->query_sql( $Sheet , 'a.ProductC_ID' );
		
		$_data = array();
		while( $row = $db->query_fetch( '', 'assoc') ){
			
			$_data[$row['ProductC_ID']] = $row['ProductC_ID'];
		}

		return $_data;
	}


	//取得同層其他類別
	function GET_PRODUCT_SAME_LV_LIST( $ProductC_ID ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_productclass as a ";
			$Sheet 	.= " LEFT JOIN en_web_productclass as b ON a.`ProductC_UpMID` = b.`ProductC_UpMID` ";
		}else{
			
			$Sheet 	= "web_productclass as a ";	
			$Sheet 	.= " LEFT JOIN web_productclass as b ON a.`ProductC_UpMID` = b.`ProductC_UpMID` ";
		}

		$db = new MySQL();
		$db->Where = " WHERE a.`ProductC_Open` = 1 AND b.`ProductC_Open` = 1 ";
		$db->Where .= " AND a.ProductC_ID = '".$ProductC_ID."'";
		$db->query_sql( $Sheet , 'b.ProductC_Name, b.ProductC_ID ' );

		$_data = array();
		while( $row = $db->query_fetch( '', 'assoc') ){

			$row['selected'] = 0;	

			if( $row['ProductC_ID'] == $ProductC_ID ) {

				$row['selected'] = 1;	
			}

			$_data[$row['ProductC_ID']] = $row;
			
		}

		return $_data;
	}
	//任意層取得第一層ID
	function GET_FIRST_LV_IN_PRODUCT_CATEGORY( $ProductC_ID ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_productclass";
		}else{
			
			$Sheet 	= "web_productclass";	
		}

		$LV = 3;

		$db = new MySQL();

		while( $LV != 1 ){

			$db->Where = " WHERE `ProductC_Open` = 1 ";
			$db->Where .= " AND ProductC_ID = '".$ProductC_ID."'";
			$db->query_sql( $Sheet , 'ProductC_ID, ProductC_UpMID, ProductC_Lv' );

			$_data = array();
			if( $row = $db->query_fetch( '', 'assoc') ){

				$LV = $row['ProductC_Lv'];
				$ProductC_ID = $row['ProductC_UpMID'];

				//沒有上一層了
				if( empty($row['ProductC_UpMID']) ) {

					return $row['ProductC_ID'];
				}
			}
		}
	}

	//任意層取得第一層ID
	function GET_EVERY_LV_PRODUCT_CATEGORY_NAME( $ProductC_ID ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_productclass as LV3 LEFT JOIN en_web_productclass as LV2 ON LV3.`ProductC_UpMID` = LV2.ProductC_ID";	
			$Sheet 	.= " LEFT JOIN en_web_productclass as LV1 ON LV2.`ProductC_UpMID` = LV1.ProductC_ID";
		}else{
			
			$Sheet 	= "web_productclass as LV3 LEFT JOIN web_productclass as LV2 ON LV3.`ProductC_UpMID` = LV2.ProductC_ID";	
			$Sheet 	.= " LEFT JOIN web_productclass as LV1 ON LV2.`ProductC_UpMID` = LV1.ProductC_ID";
		}


		$db = new MySQL();
		$db->Where = " WHERE LV3.`ProductC_ID` = '{$ProductC_ID}' ";
		$db->query_sql( $Sheet , 'LV3.ProductC_Name as LV3_name, LV2.ProductC_Name as LV2_name, LV1.ProductC_Name as LV1_name' );
		
		if( $row = $db->query_fetch( '', 'assoc') ){

			return $row;
		}
	}

	//---商品列表側邊選單
	function GET_PRODUCT_CATEGORY(){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_productclass";
			$Sheet2 = "en_web_product";
		}else{
			
			$Sheet 	= "web_productclass";
			$Sheet2 = "web_product";
		}
		
		$db = new MySQL();
		////************加LV排序 先把第一層撈出來//************
		$db->Where = " Where ProductC_Open = 1 ";
		$db->Order_By = "ORDER BY ProductC_Lv , ProductC_Sort";
		$db->query_sql( $Sheet, '*');

		$first = array();
		$sn=1;
		while( $row = $db->query_fetch() ){
			
			$mrow 			= array();
			$mrow['id'] 	= $row['ProductC_ID'];
			$mrow['name'] 	= $row['ProductC_Name'];

			if( $row['ProductC_Lv'] == 1 ){

				$mrow['NextLv'] = array();
				$mrow['sn'] = $sn;
				$sn++;
				$_AsideList[$row['ProductC_ID']] = $mrow;
				
			}else if( $row['ProductC_Lv'] == 2 ){

				$_AsideList[$row['ProductC_UpMID']]['NextLv'][$mrow['id']] = $mrow;

				$first[$mrow['id']] = $row['ProductC_UpMID'];

			}else if( $row['ProductC_Lv'] == 3 ){

				$LV1 = $first[$row['ProductC_UpMID']];

				$_AsideList[$LV1]['NextLv'][$row['ProductC_UpMID']]['NextLv'][$mrow['id']] = $mrow;
			}
			
			$_SEO['WO_Keywords'] 	.= GET_HEAD_KD( $row['ProductC_Name'] , 'K' );
			$_SEO['WO_Description'] .= GET_HEAD_KD( $row['ProductC_Name'] );
			
		}

		$_rs['Data'] 		= $_AsideList;
		$_rs['SEO'] 		= $_SEO;
		
		return $_rs;
	}

	//取得首頁產品列表
	function GET_INDEX_PRODUCT_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_product";
		}else{
			
			$Sheet 	= "web_product";
		}

		$db = new MySQL();
		
		$db->Where = " WHERE `Product_Open` = 1 AND Product_OpenIndex = 1 ";
		$db->Order_By = 'ORDER BY Product_Sort';

		$_Field = "*";

		$db->query_sql( $Sheet , $_Field );
		
		while( $row = $db->query_fetch( '', 'assoc') ){
			
			if( !empty($row['Product_Mcp']) ) {

				$row['mcp_src'] = Product_Url.$row['Product_ID'].'/'.$row['Product_Mcp'];
			}

			$_data[$row['Product_ID']] = $row;
		}
		
		$_rs['Data'] 			= $_data;
		
		return $_rs;
	}
	
	function CHK_PRO_CLASS_IS_EMPTY( $input = array() ){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_product";
		}else{
			
			$Sheet 	= "web_product";	
		}

		$db = new MySQL();
		$db->Where = " Where Product_Open = 1 ";
		if( isset($input['ProductC_ID']) && $input['ProductC_ID'] > 0 ){

			$db->Where .= " AND ProductC_ID = '".$input['ProductC_ID']."'";
		}
		$_count = $db->query_count($Sheet);
		
		if( $_count == 0 ) {
			
			return false;
		}else{

			return true;
		}
	}

	//************************************************************************************************************
	//                                         聯絡我們/詢價單       
	//************************************************************************************************************
	
	//主題列表
	function GET_CONTRACT_THEME_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_contact_theme";
		}else{
			
			$Sheet 	= "web_contact_theme";	
		}

		$db = new MySQL();
		$db->Where = " WHERE `Theme_Open` = 1 ";
		$db->Order_By = 'ORDER BY Theme_Sort';

		$_Field = "*";

		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				$_data[$row['Theme_ID']] = $row;
			}
		}
		
		return $_data;
	}

	//經緯度資訊
	function GET_CONTRACT_MAP_LIST( $Input = array() , $type ='MULT'){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 	= "en_web_contract_map";
		}else{
			
			$Sheet 	= "web_contract_map";	
		}

		$db = new MySQL();
		$db->Where = " WHERE `CMAP_Open` = 1 ";

		if( isset($Input['id']) && $Input['id'] > 0 ){

			$db->Where .= " AND CMAP_ID = '".$Input['id']."'";
		}

		$db->Order_By = 'ORDER BY CMAP_Sort';

		$_Field = "*";

		$db->query_sql( $Sheet , $_Field );
		
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
				
				$row['cs_content'] = '<h3>'.$row['CMAP_Title'].'</h3><p class="addr">'.$row['CMAP_addr'].'</p><br><a href="'.$row['CMAP_link'].'" target="_blank" class="gotomap">《前往地圖》</a><br>  <p class="tel">TEL：'.$row['CMAP_tel'].'</p><br><p class="email">E-mail：'.$row['CMAP_email'].'</p>';

				$_data = $row;
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				$_data[$row['CMAP_ID']] = $row;
			}
		}
		
		return $_data;
	}

	//詢價單產品列表
	function GET_CONTRCT_PRODUCT_LIST(){

		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 		= "en_web_product";
			$Sheet2 	= "en_web_productl";
		}else{
			
			$Sheet 		= "web_product";	
			$Sheet2 	= "web_productl";
		}

		$CartList = $_SESSION['temp_cart'][$_SESSION['ntm']['language']];

		if( empty($CartList) ) return array();

		$product_id_array = array();

		foreach ($CartList as $product_id => $val) {
			
			$product_id_array[] = $product_id;
		}

		$sn = 0;
		foreach ($product_id_array as $key => $value) {
			if( $sn == 0){

				$temp_sql = "'".$value."'";
			}else{

				$temp_sql .= ",'".$value."'";
			}
			$sn++;
		}
		$db = new MySQL();
		$db->Where = " WHERE `Product_Open` = 1 ";
		$db->Where .= " AND `Product_ID` IN (".$temp_sql.")";
		$db->query_sql( $Sheet , 'Product_ID, Product_Mcp, Product_Name' );
		
		$_data = array();

		while( $row = $db->query_fetch( '', 'assoc') ){

			$row['img_src'] = Product_Url.$row['Product_ID'].'/'.$row['Product_Mcp'];
			$row['count'] = $CartList[$row['Product_ID']]['count'];
			$_data[$row['Product_ID']] = $row;
		}

		return $_data;
	}

	//************************************************************************************************************
	//                                                
	//************************************************************************************************************
	
	//標章列表
	function GET_SIGN_LIST( $Input = array() , $type ='MULT'){
		
		$Sheet 	= "web_product_sign";	

		$db = new MySQL();
		
		$db->Where = " WHERE `ProSign_Open` = 1 ";

		$_Field = "*";
		$db->Order_By = 'ORDER BY ProSign_Sort';

		$db->query_sql( $Sheet , $_Field );
		if( $type == 'SINGLE'){

			if( $row = $db->query_fetch( '', 'assoc') ){
			
				$_data = $row;
			}
		}else{

			while( $row = $db->query_fetch( '', 'assoc') ){
				
				//$_data[$row['ProSign_ID']] = $row['ProSign_Title'];
				$row['img_src'] = IMG_URL.'ntm/ProSign/'.$row['ProSign_Img'];
				$_data[$row['ProSign_ID']] = $row;
			}
		}
		
		return $_data;
	}

	//---基本設定
	function SELECT_SETTING( $fields = '*' ){
		
		if( isset($_SESSION['ntm']['language']) && $_SESSION['ntm']['language'] == 'en'){

			$Sheet 		= "en_web_setting";
		}else{
			
			$Sheet 		= "web_setting";	
		}
		
		$db = new MySQL();
		
		$db->Where = " WHERE `Admin_ID` = 2 ";
		
		$db->query_sql( $Sheet , $fields );

		$_data = array();

		if( $row = $db->query_fetch( '', 'assoc') ){
			
			if( isset($row['About_Banner_Img']) ) {

				$row['About_Banner_Img'] = Setting_Url.$row['About_Banner_Img'];
			}
			$_data = $row;
		}
		
		return $_data;
	}

	
	function __destruct(){
		
		
		unset($_Data);
	}
}
?>