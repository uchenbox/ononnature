<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
            <!-- header -->
            <div id="intrp-style-wrap" class="d-flex flex-column p-0 m-0">
                <div class="intrp-style-content">
                    <h4 class="text-center">– FAQ –</h4>
                    <h2 class="text-center">常見問題</h2>
                    <hr>
                </div>  
                <div class="d-flex flex-row meal-items-content">
                    <div class="traffic-items item-1">
                        <div class="img-wrap">
                            <img class="w-100" src="<?php echo (!empty($data['faq_header'][0]['Faq_Img_Header']))?$data['faq_header'][0]['Faq_Img_Header']: '#'; ?>" alt="常見問題">
                        </div>
                    </div>
                    <div class="traffic-items item-2 d-md-block">
                        <div class="bg-gray float-right"></div>
                    </div>     
                </div>      
            </div>
            
            <!-- FAQ -->
            <div class="d-flex flex-column  p-0 mx-0 faq-wrap ">
                    <div class="px-0 mx-0 faq-content">
                        <?php
                        foreach ($data['faq_theme'] as $theme) {
                            echo '<div class="row title-wrap ">
                                    <h3 class="mb-0">'.$theme['Faq_Name'].'</h3>
                                </div>';
                            echo '<div class="d-flex px-0 mx-0 item-content">
                                        <div class="accordion" id="accordionExample_'.$theme['Faq_ID'].'">';
                                        $fi = 0;
                                        foreach ($data['faq'] as $faq) {
                                            if(strpos($theme['Faq_ID'], $faq['Faq_ID']) !== false )
                                            {
                                                echo '<div class="card">
                                                        <div class="card-header p-0" id="heading_'.$faq['Faq_ID'].'_'.$fi.'">
                                                            <h2 class="mb-0 p-0 collapsed" data-toggle="collapse" data-target="#collapse_'.$faq['Faq_ID'].'_'.$fi.'" aria-expanded="true" aria-controls="collapse_'.$faq['Faq_ID'].'_'.$fi.'">
                                                            '.$faq['Faq_Intro_Answer'].'
                                                            <span class="collapseBt float-right"></span>
                                                            </h2>
                                                        </div>

                                                        <div id="collapse_'.$faq['Faq_ID'].'_'.$fi.'" class="card-collapse collapse" aria-labelledby="heading_'.$faq['Faq_ID'].'_'.$fi.'" data-parent="#accordionExample_'.$theme['Faq_ID'].'">
                                                            <div class="card-body">
                                                            '.str_replace("</p>", "<br/>", str_replace("<p>", '', $faq['Faq_Content_Answer'])).'
                                                            </div>
                                                        </div>
                                                    </div>';
                                                $fi++;
                                            }
                                        }
                                           
                            echo        '</div>
                                </div>';
                        }
                        ?>
                        <!-- <div class="row title-wrap border-bottom border-secondary mb-5">
                            <h3>關於行動木屋</h3>
                        </div>
                        <div class="row p-0 m-0 p-0 mt-5">
                            <div class="col-12 p-0">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            有實體樣品可以參觀嗎？有實體樣品可以參觀嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapseOne" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapseOne">
                                            我想參觀木屋但不想用餐可以嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsetwo" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 " data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                                            餐點可以換嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsethree" class="card-collapse collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="true" aria-controls="collapseOne">
                                            附近有停車場嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsefour" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsefive" aria-expanded="true" aria-controls="collapseOne">
                                            有特斯拉充電站嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsefive" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row title-wrap border-bottom border-secondary mb-5">
                            <h3>關於OnOnNature</h3>
                        </div>
                        <div class="row p-0 m-0 p-0 mt-5">
                            <div class="col-12 p-0">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            有實體樣品可以參觀嗎？有實體樣品可以參觀嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapseOne" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapseOne">
                                            我想參觀木屋但不想用餐可以嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsetwo" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 " data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                                            餐點可以換嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsethree" class="card-collapse collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="true" aria-controls="collapseOne">
                                            附近有停車場嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsefour" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsefive" aria-expanded="true" aria-controls="collapseOne">
                                            有特斯拉充電站嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsefive" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row title-wrap border-bottom border-secondary mb-5">
                            <h3>關於紅屋家具</h3>
                        </div>
                        <div class="row p-0 m-0 p-0 mt-5">
                            <div class="col-12 p-0">
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            有實體樣品可以參觀嗎？有實體樣品可以參觀嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapseOne" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapseOne">
                                            我想參觀木屋但不想用餐可以嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsetwo" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 " data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                                            餐點可以換嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsethree" class="card-collapse collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="true" aria-controls="collapseOne">
                                            附近有停車場嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsefour" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header p-0" id="headingOne">
                                            <h2 class="mb-0 p-3 collapsed" data-toggle="collapse" data-target="#collapsefive" aria-expanded="true" aria-controls="collapseOne">
                                            有特斯拉充電站嗎？
                                            <span class="collapseBt float-right"></span>
                                            </h2>
                                        </div>

                                        <div id="collapsefive" class="card-collapse collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                            有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想有的。我們什麼都有什麼奇怪，這只是假字。今天天氣很好，內容不夠自己打。內容想不到，大家一起想。
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/blog.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>