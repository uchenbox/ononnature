jQuery(function($){
    $("#reserveform input.required").bind({
        keydown:function(){
            $require_el = $(this);            
            if($require_el.hasClass('has-error'))
            {
                if($require_el.val().length > 0 && $require_el.val().indexOf(' ') == -1)
                {
                    $require_el.removeClass('has-error');
                    $require_el.parent('div').find('small').remove();
                }
            }
            else
            {
                if($require_el.val().length == 0 || $require_el.val().indexOf(' ') > -1)
                {
                    $require_el.addClass('has-error').after('<small class="text-danger">必填</>');
                }
            }
        }
    });
    $("#submit").bind({
        click: function() {
            if(!validate())
            {
                swal({
                    title: "警告!",
                    text: "請輸入必填資料",
                    icon: "warning"
                });
                return false;
            }

            $.ajax({
                type: 'POST',
                url: 'api/addVisitReserve',
                dataType: 'json',
                data: new FormData($("#reserveform")[0]),
                processData : false,
                contentType: false,
                cache: false,
                success:function(data){
                    console.log(data);
                    if(data)
                    {
                        swal({
                            title: "預約成功!",
                            text: "請等候專人與您確認喔！",
                            icon: "success"
                        }).then((value) => {
                            $("#reserveform")[0].reset();
                        });
                    }                   
                    
                }
        
            });
        },
        mouseenter: function() {
          // Do something on mouseenter
        }
      });
});

var headerCarousel = function(){
    $('.header-loop').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoHeight:true,
        items:1,
        loop:true,
        // margin:30,
        autoplay:true,
        dots: false,
    });
}

var validate = function(){
    $("#reserveform input.required").each(function(){
        $require_el = $(this);            
        if($require_el.hasClass('has-error'))
        {
            if($require_el.val().length > 0 && $require_el.val().indexOf(' ') == -1)
            {
                $require_el.removeClass('has-error');
                $require_el.parent('div').find('small').remove();
            }
        }
        else
        {
            if($require_el.val().length == 0 || $require_el.val().indexOf(' ') > -1)
            {
                $require_el.addClass('has-error').after('<small class="text-danger">必填</>');
            }
        }
    });

    if($("#reserveform input.required.has-error").length > 0)
        return false;
    else
        return true;
}