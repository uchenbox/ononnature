<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>
<?php include_once('ln_list.php'); ?>
<div class="abgne_tab">

    <ul class="tabs">
        <li><a href="javascript:void(0)" for-id="tab1">基本資料</a></li>
        <li><a href="javascript:void(0)" for-id="tab2">共用</a></li>
        <li><a href="javascript:void(0)" for-id="tab3">產品專區</a></li>
        <li><a href="javascript:void(0)" for-id="tab4">下載專區</a></li>
        <li><a href="javascript:void(0)" for-id="tab5">聯絡我們</a></li>
    </ul>
    
    <div class="tab_container">
    
        	<form id="form_edit_save" class="form-horizontal">
        
			<?php foreach( $_html_ as $key => $val ){ 
                    
                    $table_sn = 'tabsn'.$key;
            ?>
			<div class="Table_border <?=$table_sn?>">
            	
                    
				<div id="tab1" class="tab_content">
                    <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">
                    <?php 
						
						//-----------------------------------------//
						//縮寫
						$Arr_Name = 'LN_AbName'; 
						
						echo $_TF->html_select($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1, $LN_LIST, '');
						//-----------------------------------------//
						//顯示語言名稱
						$Arr_Name = 'LN_Name'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//網站標題
						/*$Arr_Name = 'LN_web_WoTitle';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);*/
						//-----------------------------------------//
						
					?>
				</div>
                <!--共用-->    	
                <div id="tab2" class="tab_content">
                    <?php 
						//-----------------------------------------//
                  	    $Arr_Name = 'LN_Index';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//活動日期
						$Arr_Name = 'LN_About';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//開始日期
						$Arr_Name = 'LN_News';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//結束日期
						$Arr_Name = 'LN_tech';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//關鍵字
						$Arr_Name = 'LN_service'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//送出
						$Arr_Name = 'LN_download'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//

                    ?>
                </div> 

                <!--產品-->    	
                <div id="tab3" class="tab_content">
                    <?php 
						//-----------------------------------------//
                  	    $Arr_Name = 'LN_product_01';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//活動日期
						$Arr_Name = 'LN_product_02';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//開始日期
						$Arr_Name = 'LN_product_03';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//結束日期
						$Arr_Name = 'LN_product_04';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//關鍵字
						$Arr_Name = 'LN_product_05'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//送出
						$Arr_Name = 'LN_product_06'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_07'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_08'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_09'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_10'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_11'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_12'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_13'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_product_14'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
                    ?>
                </div> 

                <!--下載-->    	
                <div id="tab4" class="tab_content">
                    <?php 
						//-----------------------------------------//
                  	    $Arr_Name = 'LN_download_01';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//活動日期
						$Arr_Name = 'LN_download_02';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//開始日期
						$Arr_Name = 'LN_download_03';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//結束日期
						$Arr_Name = 'LN_download_04';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
                    ?>
                </div> 

                <!--聯絡我們-->    	
                <div id="tab5" class="tab_content">
                    <?php 
						//-----------------------------------------//
                  	    $Arr_Name = 'LN_contract_01';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//活動日期
						$Arr_Name = 'LN_contract_02';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//開始日期
						$Arr_Name = 'LN_contract_03';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//結束日期
						$Arr_Name = 'LN_contract_04';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//關鍵字
						$Arr_Name = 'LN_contract_05'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//送出
						$Arr_Name = 'LN_contract_06'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_07'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_08'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_09'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_10'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_11'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_12'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_13'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_14'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_15'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						//日期
						$Arr_Name = 'LN_contract_16'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
                    ?>
                </div> 
                
				<!--JS提示訊息-->    	
            <?php } ?>
			</div>
                <div class="clear_both form-actions">
                    <button id="saveb" class="btn btn-info" type="button" onclick="form_edit_save()">
                        <i class="ace-icon fa fa-check bigger-110"></i>儲存
                    </button>&nbsp;&nbsp;&nbsp; 
                            
                    <button id="rsetb" class="btn btn" type="reset">
                        <i class="ace-icon fa fa-check bigger-110"></i>重設
                    </button>
                </div>
            </form>
		</div>
    </div>
</div>

<script type="text/javascript">$('.imgajax').colorbox({width:"70%", height:"100%", rel:'imgajax'});</script>