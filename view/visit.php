<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column ">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
        <!-- header -->
        <div id="intrp-style-wrap" class="d-flex flex-column p-0 m-0">
            <div class="intrp-style-content">
                <h4 class="text-center">– HOUSE GUIDING TOUR –</h4>
                <h2 class="text-center">行動木屋參訪</h2>
                <hr>
            </div>   
            <div class="d-flex flex-row visit-items-content">
                <div class="visit-items item-1">
                    <div class="img-wrap">
                        <img class="w-100" src="<?php echo ((isset($data['visit_header'][0]['Visit_Img_Header']))?$data['visit_header'][0]['Visit_Img_Header']:'assets/01A_木屋參訪/00_header/01.jpg'); ?>" alt="OnOnNature行動木屋參訪">
                    </div>
                </div>
                <div class="visit-items item-2">
                    <div class="img-wrap float-right">
                        <img class="w-100" src="<?php echo ((isset($data['visit_header'][1]['Visit_Img_Header']))?$data['visit_header'][1]['Visit_Img_Header']:'assets/01A_木屋參訪/00_header/02.jpg'); ?>" alt="OnOnNature行動木屋參訪">
                    </div>
                </div>
            </div>
              
            <div id="title-wrap"  class="d-flex d-column align-self-center ">
                <!-- <div class="mr-3">好生活 <br/>。 </div>
                <div class="mr-3">美屋<br/>， </div>
                <div>輕食<br/>，</div> -->
                <?php echo str_replace('。', '<br/>。', str_replace('，', '<br/>，', str_replace('p>', 'div class="mr-3">', $data['visit_header'][0]['Visit_Intro_Header']))); ?>
            </div>       
        </div>
        
        <!-- 屋 Ｘ 食 -->
        <div class="visit-desp-content d-flex flex-column  p-0 m-0">
            <div class="visit-desp-wrap px-0">
                <div class="d-flex flex-row px-0 mx-auto items">
                    <div class="px-0 mx-0 item-content">
                        <h3 class="mx-auto"><?php echo $data['visit_desp'][0]['Visit_Title_Desp']; ?></h3>
                    </div>
                    <div class="px-0 mx-0 item-carousel">
                        <p class="px-0">
                            <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['visit_desp'][0]['Visit_Content_Desp']);?>
                        
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- 參訪內容 -->
        <div class="visit-content d-flex flex-column  p-0 mx-0">
            <div class="visit-content-wrap px-0">
                <div class="row px-0  mx-0">
                    <div class="col-12 px-0 mx-0 text-center item-title">
                        <h3 class="mb-0">CONTENT</h3>
                    </div>
                </div>
                <?php 
                    $j = 0;
                    foreach ($data['visit_pdesp'] as $item) {
                        echo '<div class="bg-gray '.(($j == 0)?'first-el':'').' '.(($j == 3)?'last-el':'').'">
                                <div class="d-flex flex-row px-0 items  mr-auto">
                                    
                                    <div class="item  text-center rounded-circle ">'.
                                        (($j == 3)?'':'<img class="img-1x1" src="'.$item['Visit_Img'].'" alt="'.$item['Visit_Title_Pdesp'].'">').'                                        
                                    </div>
                                    <hr class="lines h-line"/>
                                    
                                    <div class="item-desp d-flex flex-column">
                                        <div class="title-wrap">
                                            <div class="item-content-title">
                                                '.(($j != 3)?'<h3 class="mb-0">'.$item['Visit_Title_Pdesp'].'</h3>':'<h4>'.preg_replace("/\r\n|\n|\r/", "<br/>", $item['Visit_Content_Pdesp']).'</h4>').'
                                            </div>
                                            <div class="item-content-paragraph">
                                                <p>
                                                '.(($j == 3)?'':preg_replace("/\r\n|\n|\r/", "<br/>", $item['Visit_Content_Pdesp'])).'
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="lines v-line"/>
                            </div>';

                        $j++;
                    }
                ?>
                <!-- <div class="bg-gray first-el">
                    <div class="row px-0 items  mr-auto">
                        <div class="col-md-6 item mx-auto  text-center">
                            <img class="rounded-circle" src="assets/01A_木屋參訪/01_schedule/01.jpg" alt="OnOnNature木屋參訪">
                        </div>
                        <div class="col-md-6 item mx-auto">
                            <div class="title-wrap mb-3">
                                <div class="item-content-title">
                                    <h3>慢食午餐</h3>
                                </div>
                                <div class="item-content-paragraph">
                                    <p>
                                    首先享用蔬食午餐，<br> 自然農法食材＆徐徐微風，<br> 正是為您準備的見面禮。
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray">
                    <div class="row px-0 items  mr-auto">
                        <div class="col-md-6 item mx-auto  text-center">
                            <img class="rounded-circle" src="assets/01A_木屋參訪/01_schedule/02.jpg" alt="OnOnNature木屋參訪">
                        </div>
                        <div class="col-md-6 item mx-auto">
                            <div class="title-wrap mb-3">
                                <div class="item-content-title">
                                    <h3>木屋導覽</h3>
                                </div>
                                <div class="item-content-paragraph">
                                    <p>
                                    專員帶領小組參觀，<br> 從建材知識到生活方式，<br> 帶給您最完整的木屋遊覽。
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray">
                    <div class="row px-0 items  mr-auto">
                        <div class="col-md-6 item mx-auto  text-center">
                            <img class="rounded-circle" src="assets/01A_木屋參訪/01_schedule/03.jpg" alt="OnOnNature木屋參訪">
                        </div>
                        <div class="col-md-6 item mx-auto">
                            <div class="title-wrap mb-3">
                                <div class="item-content-title">
                                    <h3>茶敘</h3>
                                </div>
                                <div class="item-content-paragraph">
                                    <p>
                                    盡興參訪結束後，<br> 再回來喝杯茶吃點心，<br> 一起聊聊您的木屋體驗。
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray last-el">
                    <div class="row px-0 items  mr-auto">
                        <div class="col-md-6 item mx-auto  text-center">
                            
                        </div>
                        <div class="col-md-6 item mx-auto">
                            <div class="title-wrap mb-3">
                                <div class="item-content-title">
                                    <h4>大人 660元/人 <br> 小孩 330元/人</h4>
                                </div>
                                <div class="item-content-paragraph">
                                    <p>
                                    
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

        <!-- 預約表單 -->
        <div class="reserve-content d-flex flex-column  p-0 mt-0">
            <div class="reserve-content-wrap px-0">
                <div class="row px-0  mx-0">
                    <div class="col-12 text-center item-title">
                        <h3 class="mb-0">RESERVATION</h3>
                    </div>
                </div>
                <div class="row px-0 mx-auto items">
                    <div class="col-md-12 px-0  item-content">
                        <form class="mx-auto" id="reserveform">
                            <div class="row mb-3">
                                <label  class="col-3">姓名*</label>
                                <div class="col-9 ">
                                    <input type="text" placeholder="Full Name" name="name" class="required">
                                    
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label  class="col-3">電話*</label>
                                <div class="col-9">
                                    <input type="text" placeholder="Phone" name="tel" class="required">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label  class="col-3">E-MAIL</label>
                                <div class="col-9">
                                    <input type="email" placeholder="error@mail.com" name="email">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label  class="col-3">目的</label>
                                <div class="col-9">
                                    <select class="long-select" name="theme">
                                        <?php 
                                            foreach ($data['visit_theme'] as $item) {
                                                echo '<option value="'.$item['Visit_ID'].'">'.$item['Visit_Name'].'</option>';
                                            }
                                        ?>
                                        <!-- <option value="1">111</option>
                                        <option value="2">222</option>
                                        <option value="3">333</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-6">
                                    <div class="row">
                                    <label  class="col-6 pr-0">大人</label>
                                    <div class="col-6 pr-0">
                                        <select class="short-select" name="audlt">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row row-child px-0">
                                    <label  class="col-6 pr-0 text-center span-child">小孩</label>
                                    <div class="col-6 px-0">
                                        <select class="short-select" name="child">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3 px-0">
                                <label  class="col-3">備註</label>
                                <div class="col-9">
                                    <textarea class="" rows="10" placeholder="吃全素/ 兒童座椅＊2…" name="comment"></textarea>
                                </div>
                            </div>
                            <div class="row mb-3 px-0">
                                <p>
                                ※餐食為體驗環節，為當日蔬食，恕不提供點餐。<br> ※小孩330元為兒童餐，孩童如不需用餐不收費。<br> ※參訪地點位於台中石岡OnOnNature園區，詳見<a href="./Traffic" class="text-reset"><u>園區交通</u></a>。
                                </p>
                            </div>
                            <div class="row px-0">
                                <div class="col px-0">
                                    <button type="button" class="btn btn-block text-center" id="submit">送出</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
    <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
    <script src="dist/script/vendor/sweetalert.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/visit.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>