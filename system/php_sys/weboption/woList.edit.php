<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>

<div class="abgne_tab">

    <ul class="tabs">
        <li><a href="javascript:void(0)" for-id="tab1">一般設定</a></li>
        <li><a href="javascript:void(0)" for-id="tab2">SMTP設定</a></li>
        <li><a href="javascript:void(0)" for-id="tab3">收件人</a></li>
        <li><a href="javascript:void(0)" for-id="tab4">社交連結</a></li>
    </ul>
    
    <div class="tab_container">
		
        <form id="form_edit_save" class="form-horizontal">
        
			<?php foreach( $_html_ as $key => $val ){ 
                    
                    $table_sn = 'tabsn'.$key;
            ?>
			<div class="Table_border <?=$table_sn?>">
            	
                <div id="tab1" class="tab_content">
                    <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">
                    
                    <?php 
                    //-----------------------------------------//
					//網站網址
					$Arr_Name = 'WO_Url';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//網站名稱
					$Arr_Name = 'WO_Title';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//關鍵字
					$Arr_Name = 'WO_Keywords';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_keyword( $table_info[$Arr_Name]['Comment'], '輸入關鍵字後按下ENTER完成', $Arr_Name, $val );
					}
					//-----------------------------------------//
					//描述
					$Arr_Name = 'WO_Description';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], 'description : 輸入網站的介紹', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');     
					}
					//-----------------------------------------//
					//郵遞區號
					$Arr_Name = 'WO_Postalcode';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					//地址
					$Arr_Name = 'WO_Addr';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						$selcity  	= '#'.$Arr_Name;
						$selcounty	= '#WO_Addr1';
						echo $_TF->html_city($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', $table_sn, $selcity, $selcounty);
					}
					//-----------------------------------------//
					$Arr_Name = 'WO_Addr1';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_county($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '');
					}
					//-----------------------------------------//
					$Arr_Name = 'WO_Addr2';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//電話
					$Arr_Name = 'WO_Tel'; 
					if( $CfgWoption[$Arr_Name]['show'] ){ 
					
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, '', '');	
					}
					//-----------------------------------------//
					//email
					$Arr_Name = 'WO_Email'; 
					if( $CfgWoption[$Arr_Name]['show'] ){ 
					
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, '', '');	
					}
					//-----------------------------------------//
					//服務電話
					$Arr_Name = 'WO_ServiceTel'; 
					if( $CfgWoption[$Arr_Name]['show'] ){ 
					
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, '', '');	
					}
					//-----------------------------------------//		
					//傳真	
					$Arr_Name = 'WO_Fax'; 
					if( $CfgWoption[$Arr_Name]['show'] ){ 
					
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, '', '');
					}
					
					//-----------------------------------------//
					//營業時間
					// $Arr_Name = 'WO_ServiceTime'; 
					// if( $CfgWoption[$Arr_Name]['show'] ){ 
					
					// 	echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, '', '');	
					// }
					
					//-----------------------------------------//
					//GA
					$Arr_Name = 'WO_GAnalytics';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_textarea('SEO追蹤碼', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');   
					}
					//-----------------------------------------//
					//版權宣告
					$Arr_Name = 'WO_Privacy';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');   
					}
					
					?> 

                </div>
                
                <div id="tab2" class="tab_content">
                	
                    <?php 
					
					//-----------------------------------------//
					$Arr_Name = 'WO_StmpHost';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '範例 : ms39.hinet.net, msa.hinet.net, msr.hinet.net', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'WO_StmpPort';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '預設 : 25', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'WO_SendName';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '例如 : 王小名', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'WO_SendEmail';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '例如 : xxxxxx@msa.hinet.net', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					//-----------------------------------------//
					$Arr_Name = 'WO_StmpAuth';
						
					echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1, $Arr_Name);
					//-----------------------------------------//
					$Arr_Name = 'WO_StmpAcc';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, $val['WO_StmpAuth']?false:true, $Arr_Name);
					//-----------------------------------------//
					$Arr_Name = 'WO_StmpPass';
						
					echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, ($val['WO_StmpAuth']?false:true), $Arr_Name);
					//-----------------------------------------//
					$Arr_Name = 'WO_StmpSecure';
						
					echo $_TF->html_select($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1, $secure_states, 'NO_FIRST_OPTION');
					// //-----------------------------------------//
					?> 
                </div>

                <div id="tab3" class="tab_content">
                	
                    <?php 
					//-----------------------------------------//
					$Arr_Name = 'WO_Admin_reciver';
						
					echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '輸入管理員信箱 用 ; 分隔', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');     
					//-----------------------------------------//
					?> 
                </div>

				<div id="tab4" class="tab_content">
  
                    <?php 
                    //-----------------------------------------//
					//Facebook
					$Arr_Name = 'WO_Flink01';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//Line
					$Arr_Name = 'WO_Flink02';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//Instagram
					$Arr_Name = 'WO_Flink03';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//Weibo
					$Arr_Name = 'WO_Flink04';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//Wechat
					$Arr_Name = 'WO_Flink05';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//Twitter
					$Arr_Name = 'WO_Flink06';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//Telephone
					$Arr_Name = 'WO_Flink07';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//-----------------------------------------//
					//Youtube
					$Arr_Name = 'WO_Flink08';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}
					//Pinterest
					$Arr_Name = 'WO_Flink09';
		            if( $CfgWoption[$Arr_Name]['show'] ){
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
					}

					
					?> 

                </div>
				
				
            <?php } ?>
			</div>
            
            <div class="clear_both form-actions">
                <button id="saveb" class="btn btn-info" type="button" onclick="form_edit_save()">
                    <i class="ace-icon fa fa-check bigger-110"></i>儲存
                </button>&nbsp;&nbsp;&nbsp; 
                        
                <button id="rsetb" class="btn btn" type="reset">
                    <i class="ace-icon fa fa-check bigger-110"></i>重設
                </button>
            </div>
		</form>
    </div>
</div>