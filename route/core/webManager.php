<?php 
class webManager {
    
    public static function query_Event($method, $args){   
        include("mod_db.php");
        date_default_timezone_set('Asia/Taipei');
        $ini_result = self::iniread(__pageroot . "core/query/sys.ini");  

        switch ($method) {            
            case 'gelleryList': // 輪播器列表
                $sql_inquery = $ini_result['data']['gelleryList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "addgellery":
                $sql_inquery = $ini_result['data']['addGellery']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['sorted'],PDO::PARAM_INT);
                $state->bindValue(2,$args['showed'],PDO::PARAM_STR);
                $state->bindValue(3,$args['content'],PDO::PARAM_STR);
                $state->bindValue(4,$args['source'],PDO::PARAM_STR);
                $state->bindValue(5,$args['filename'],PDO::PARAM_STR);
                $state->bindValue(6,$args['filename_small'],PDO::PARAM_STR);
                $state->bindValue(7,$args['type'],PDO::PARAM_STR);
                $state->bindValue(8,$args['size'],PDO::PARAM_INT);
                $state->bindValue(9,$args['dir'],PDO::PARAM_STR);
                $state->bindValue(10,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return $state->fetchAll();
                }
            break;
            case "loadgellery":
                $sql_inquery = $ini_result['data']['loadgellery']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "updategellery":
                $sql_inquery = $ini_result['data']['updategellery']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['sorted'],PDO::PARAM_INT);
                $state->bindValue(2,$args['showed'],PDO::PARAM_STR);
                $state->bindValue(3,$args['content'],PDO::PARAM_STR);
                $state->bindValue(4,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(5,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return array("updatetime" => date("Y-m-d H:i:s"));
                }
            break;
            case "updategellerywithpic":
                $sql_inquery = $ini_result['data']['updategellerywithpic']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['sorted'],PDO::PARAM_INT);
                $state->bindValue(2,$args['showed'],PDO::PARAM_STR);
                $state->bindValue(3,$args['content'],PDO::PARAM_STR);
                $state->bindValue(4,$args['source'],PDO::PARAM_STR);
                $state->bindValue(5,$args['filename'],PDO::PARAM_STR);
                $state->bindValue(6,$args['filename_small'],PDO::PARAM_STR);
                $state->bindValue(7,$args['type'],PDO::PARAM_STR);
                $state->bindValue(8,$args['size'],PDO::PARAM_INT);
                $state->bindValue(9,$args['dir'],PDO::PARAM_STR);
                $state->bindValue(10,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(11,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return array("updatetime" => date("Y-m-d H:i:s"));
                }
            break;
            case 'delGellery': //刪除輪播圖片
                $sql_inquery = $ini_result['data']['delGellery']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            default:
                # code...
            break;
        }
    }
     

    //base
    private static function iniread($filename) 
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");  //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }

}
?>