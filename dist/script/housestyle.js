jQuery(function($){
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
      })
    $('#gotoTop').click(function(){
        $('html,body').animate({ scrollTop: 0 }, 'slow');   /* 返回到最頂上 */
        return false;
    });
    // loopCarousel();
});

var loopCarousel = function(){
    $('.loop').owlCarousel({
        dots: false,
        nav: true,
        navText: ["<img src='assets/0_共用/arrow_black.svg' style='transform: rotate(180deg)'>","<img src='assets/0_共用/arrow_black.svg'>"],
        // center: true,
        loop:false,
        lazyLoad:true,
        items:3,
        margin:60,
        responsiveClass:true,
        responsive:{
            // 599:{
            //     items:4,
            // }
        }
    });
}