<?php 
class dashboardManager {
    
    public static function query_Event($method, $args){   
        include("mod_db.php");
        date_default_timezone_set('Asia/Taipei');
        $ini_result = self::iniread(__pageroot . "core/query/sys.ini");  

        switch ($method) {            
            case 'showNums': 
                $sql_inquery = $ini_result['data']['showNums']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            default:
                # code...
            break;
        }
    }
     

    //base
    private static function iniread($filename) 
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");  //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }

}
?>