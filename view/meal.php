<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
            <!-- header -->
            <div id="intrp-style-wrap" class="d-flex flex-column p-0 m-0">
                <div class="intrp-style-content">
                    <h4 class="text-center">– MENU –</h4>
                    <h2 class="text-center">餐點介紹</h2>
                    <hr>
                </div>   
                <div class="d-flex flex-row meal-items-content">
                    <div class="traffic-items item-1">
                        <div class="img-wrap">
                            <img class="w-100" style="margin-top:-100px;" src="<?php echo (!empty($data['meal_header'][0]['Meal_Img_Header']))?$data['meal_header'][0]['Meal_Img_Header']: '#'; ?>" alt="餐點介紹">
                        </div>
                    </div>
                    <div class="traffic-items item-2  d-md-block">
                        <div class="bg-gray float-right"></div>
                    </div>
                </div>                         
            </div>
            
            <!-- 自然共生 -->
            <div class="visit-desp-content d-flex flex-column  p-0 m-0">
                <div class="meal-desp-wrap px-0">
                    <div class="d-flex flex-row px-0 mx-auto items">
                        <div class="px-0 mx-0 item-content">
                            <h3 class="mx-auto"><?php echo $data['meal_desp'][0]['Meal_Title_Desp']; ?></h3>
                        </div>
                        <div class="px-0 mx-0 item-carousel">
                            <p class="px-0">
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['meal_desp'][0]['Meal_Content_Desp']);?>
                            
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- 餐點內容 -->
            <?php
            $ti = 0;
            for ($i=0; $i < count($data['meal_theme']); $i++) {
                $theme = $data['meal_theme'][$i];
                echo '<div class="meal-right-content d-flex flex-column  p-0 m-0 overflow-hidden">
                        <div class="meal-right-wrap px-0">
                            <div class="items mb-0 ml-auto d-flex flex-row position-relative">
                                <div class="align-self-start">
                                    <h3>'.$theme['Meal_Name'].'</h3>
                                </div>
                                <div class="mt-5 align-self-start meal-en-name">';
                                foreach ($data['meal_pdesp'] as $pdesp) {
                                    if(strpos($theme['Meal_ID'], $pdesp['Meal_ID']) !== false )
                                    {
                                        echo '<small>'.$pdesp['Meal_Title_Pdesp'].'</small>';
                                    }
                                }
                                    
                echo            '</div>
                                
                            </div>
                            <div class="items mt-n5 ml-auto">
                                <div class="img-wrap overflow-hidden">';
                                foreach ($data['meal_pdesp'] as $pdesp) {
                                    if(strpos($theme['Meal_ID'], $pdesp['Meal_ID']) !== false && isset($pdesp['Meal_Mcp_Pdesp']))
                                    {
                                        echo '<img class="w-100" src="'.((isset($pdesp['Meal_Mcp_Pdesp']))?$pdesp['Meal_Mcp_Pdesp']:'#') .'" alt="'.$theme['Meal_Name'].'">';
                                    }
                                }
                                    
                echo            '</div>
                            </div>
                        </div>
                    </div>';
                
                echo '<div class="meal-left-content d-flex flex-column  px-0 mx-0 '.(($ti == (count($data['meal_theme'])-1))?'last-el':'').'">
                        <div class="meal-left-wrap px-0">
                            <div class="items bg-gray mr-auto d-flex justify-content-md-end">
                                <div class="item">
                                    <div class="row px-0 ml-auto item-row">
                                        ';
                                                foreach ($data['meal_pdesp'] as $pdesp) {
                                                    if(strpos($theme['Meal_ID'], $pdesp['Meal_ID']) !== false)
                                                    {
                                                        if($ti == 0 || $ti == 1)
                                                        {
                                                            echo '<div class="col-6 px-0 item-row-col-left d-flex align-items-start flex-column">
                                                                    <div class="mb-auto">
                                                                        <p class="p-0 m-0">';
                                                            echo $pdesp['Meal_Intro_Pdesp'];
                                                                    echo '</p>
                                                                    </div>
                                                                    <div>
                                                                        <h4>'.preg_replace("/\r\n|\n|\r/", "<br/>", $pdesp['Meal_Intro_Price']).'</h4>
                                                                    </div>
                                                                </div>';
                                                            echo '<div class="col-6 px-0 item-row-col-right">
                                                                    <p class="p-0 m-0">
                                                                    '.str_replace("</p>", "<br/>", str_replace("<p>", '', $pdesp['Meal_Content_Pdesp'])).'
                                                                    </p>
                                                                </div>';
                                                            echo '<div class="col-6 px-0 item-row-col-left item-row-col-right-price">
                                                                    <div>
                                                                        <h4>'.preg_replace("/\r\n|\n|\r/", "<br/>", $pdesp['Meal_Intro_Price']).'</h4>
                                                                    </div>
                                                            </div>';
                                                        }
                                                        elseif($ti == 2)
                                                        {
                                                            echo '<div class="col-6 px-0 item-row-col-left d-flex align-items-end  flex-column">
                                                                    <div>
                                                                        <p class="px-3">';
                                                                        echo $pdesp['Meal_Intro_Pdesp'];
                                                                    echo '</p>
                                                                    </div>
                                                                </div>';
                                                            echo '<div class="col-6 px-0 item-row-col-right  d-flex align-items-end">
                                                                    <div>
                                                                        <h4>'.preg_replace("/\r\n|\n|\r/", "<br/>", $pdesp['Meal_Intro_Price']).'
                                                                        </h4>
                                                                    </div>
                                                                </div>';
                                                        }
                                                        else
                                                        {
                                                            echo '<div class="col-6 px-0 item-row-col-left">
                                                                <div >
                                                                    <p class="px-3">
                                                                    <strong>'.$pdesp['Meal_Intro_Pdesp'].' .................................'.preg_replace("/\r\n|\n|\r/", "<br/>", $pdesp['Meal_Intro_Price']).'</strong> <br>
                                                                    '.str_replace("</p>", "<br/>", str_replace("<p>", '', $pdesp['Meal_Content_Pdesp'])).'
                                                                    </p>
                                                                </div>                                    
                                                            </div>';
                                                        }
                                                        
                                                    }
                                                }
                
        echo                        '</div>
                                </div>
                            </div>

                            <div class="items-gallery bg-gray">
                                <div class="item-gallery d-flex ">';
                                    $pi = 0;
                                    foreach ($data['meal_pimg'] as $pimg) {
                                        if(strpos($theme['Meal_ID'], $pimg['Meal_ID']) !== false )
                                        {
                                            if($pi <5)
                                            {
                                                echo '<div class="img-wrap ">
                                                    <a href="'.((isset($pimg['Meal_Img_Pimg']))?$pimg['Meal_Img_Pimg']:'#').'" data-lightbox="sky" data-title="">
                                                    <div class="img-1x1" style="background-image: url(\''.((isset($pimg['Meal_Img_Pimg']))?$pimg['Meal_Img_Pimg']:'#').'\');"></div>
                                                    </a>
                                                </div>';
                                            }
                                            $pi++;
                                        }
                                    }
                                    
        echo                    '</div>
                            </div>
                        </div>
                    </div>';
                
                $ti++;
            }
            ?>
            
            

            
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            jQuery(function($){
                lightbox.option({
                    'resizeDuration': 200,
                    'wrapAround': true
                })
            });
            var scripts = [
                'dist/script/init.js',
                'dist/script/traffic.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>