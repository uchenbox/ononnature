<?php 
class authManager {
    
    public static function query_Event($method, $args){   
        include("mod_db.php");
        date_default_timezone_set('Asia/Taipei');
        $ini_result = self::iniread(__pageroot . "core/query/sys.ini");  

        switch ($method) {
            case 'checktoken':
                $token = $_COOKIE['userid'];
                if(isset($token))
                {
                    $sql_inquery = $ini_result['data']['checkToken']['sql'];

                    $state = $conn->prepare($sql_inquery);
                    $state->bindValue(1,$token,PDO::PARAM_STR);
                    $state->execute();
                    if ($state->rowCount() < 1) {
                        return false;            
                    }
                    else 
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            break;
            case 'getbyToken':
                $token = $_COOKIE['userid'];
                if(isset($token))
                {
                    $sql_inquery = $ini_result['data']['checkToken']['sql'];

                    $state = $conn->prepare($sql_inquery);
                    $state->bindValue(1,$token,PDO::PARAM_STR);
                    $state->execute();
                    if ($state->rowCount() < 1) {
                        return false;            
                    }
                    else 
                    {
                        return $state->fetchAll();
                    }
                }
                else
                {
                    return false;
                }
            break;
            case 'checkauth':
                $sql_inquery = $ini_result['data']['checkAccount']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['account'],PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) { //無該帳號
                    $errmsg="404系統中無此帳號";
                    $sql_inquery0 = $ini_result['data']['loginlog']['sql'];
                    $state0 = $conn->prepare($sql_inquery0);
                    $state0->bindValue(1,$args['account'],PDO::PARAM_STR);
                    $state0->bindValue(2,$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
                    $state0->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                    $state0->bindValue(4,"F",PDO::PARAM_STR);
                    $state0->bindValue(5,$errmsg,PDO::PARAM_STR);
                    $state0->bindValue(6,0,PDO::PARAM_INT);
                    $state0->execute();

                    return array("code"=>404,"code_msg"=>"系統中無此帳號");            
                }
                else 
                {
                    $sql_inquery1 = $ini_result['data']['checkPwd']['sql'];

                    $state1 = $conn->prepare($sql_inquery1);
                    $state1->bindValue(1,$args['account'],PDO::PARAM_STR);
                    $state1->bindValue(2,md5($args['password']),PDO::PARAM_STR);
                    $state1->execute();
                    
                    if ($state1->rowCount() < 1) //密碼錯誤
                    { 
                        $errnum; //錯誤次數變數ｓ
                        $errmsg; //錯誤訊息
                        //是否有過登入紀錄
                        $sql_inqueryerr = $ini_result['data']['getPwdError']['sql'];
                        $state_err = $conn->prepare($sql_inqueryerr);
                        $state_err->bindValue(1,$args['account'],PDO::PARAM_STR);
                        $state_err->execute();
                        $errmsg="403密碼錯誤";
                        if ($state_err->rowCount() < 1) {  //沒有登入紀錄，新增
                            $sql_inquery2 = $ini_result['data']['loginlog']['sql'];

                            $state2 = $conn->prepare($sql_inquery2);
                            $state2->bindValue(1,$args['account'],PDO::PARAM_STR);
                            $state2->bindValue(2,$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
                            $state2->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                            $state2->bindValue(4,"F",PDO::PARAM_STR);
                            $state2->bindValue(5,$errmsg,PDO::PARAM_STR);
                            $state2->bindValue(6,1,PDO::PARAM_INT);
                            $state2->execute();            
                        }
                        else //有登入紀錄，更新
                        {
                            $row = $state_err->fetchAll();
                                
                            if (!is_numeric((int)$row[0]['forget'])) { //第一次錯誤登入
                                $errnum = 1;            
                            }
                            else 
                            {                            
                                $errnum = (int)$row[0]['forget'] + 1;
                            }
                            
                            $sql_inquery2 = $ini_result['data']['upadtePwdError']['sql'];

                            $state2 = $conn->prepare($sql_inquery2);
                            $state2->bindValue(1,$errnum,PDO::PARAM_INT);
                            $state2->bindValue(2,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                            $state2->bindValue(3,$args['account'],PDO::PARAM_STR);
                            $state2->execute();
                            
                            $errmsg="密碼錯誤" . $errnum . "次";                        
                            
                        }
                        return array("code"=>403,"code_msg"=>"密碼錯誤");   //密碼錯誤          
                    }
                    else 
                    {
                        $successdata = $state1->fetchAll();
                        $errmsg="200登入成功";
                        $sql_inquery2 = $ini_result['data']['loginlog']['sql'];

                        $state2 = $conn->prepare($sql_inquery2);
                        $state2->bindValue(1,$args['account'],PDO::PARAM_STR);
                        $state2->bindValue(2,$_SERVER['REMOTE_ADDR'],PDO::PARAM_STR);
                        $state2->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                        $state2->bindValue(4,"S",PDO::PARAM_STR);
                        $state2->bindValue(5,$errmsg,PDO::PARAM_STR);
                        $state2->bindValue(6,0,PDO::PARAM_INT);
                        $state2->execute(); 
                        
                        $sql_inquery3 = $ini_result['data']['upadteUserToken']['sql'];

                        $state3 = $conn->prepare($sql_inquery3);                        
                        $state3->bindValue(1,$successdata[0]['logintoken'],PDO::PARAM_STR);
                        $state3->bindValue(2,$args['account'],PDO::PARAM_STR);
                        $state3->bindValue(3,md5($args['password']),PDO::PARAM_STR);
                        $state3->execute(); 

                        $cookieexpiry = (time() + 21600);
                        setcookie("userid", $successdata[0]['logintoken'], $cookieexpiry);

                        return array("code"=>200,"code_msg"=>"登入成功","url"=>"./Dashboard?act=index","data"=>$successdata);
                    }
                    
                }
            break;
            case 'hasAccount':
                $sql_inquery = $ini_result['data']['checkAccount']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['account'],PDO::PARAM_STR);
                $state->execute();
                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'userList': // 帳號列表
                $sql_inquery = $ini_result['data']['userList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'addUser': //新增使用者
                $sql_inquery = $ini_result['data']['addUser']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['account'],PDO::PARAM_STR);
                $state->bindValue(2,md5($args['password']),PDO::PARAM_STR);
                $state->bindValue(3,$args['name'],PDO::PARAM_STR);
                $state->bindValue(4,$args['email'],PDO::PARAM_STR);
                $state->bindValue(5,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'loadUser': //載入單一帳號
                $sql_inquery = $ini_result['data']['loadUser']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'updateUser': //更新使用者
                $sql_inquery = $ini_result['data']['updateUser']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['name'],PDO::PARAM_STR);
                $state->bindValue(2,$args['email'],PDO::PARAM_STR);
                $state->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(4,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return array("updatetime" => date("Y-m-d H:i:s"));
                }
            break;
            case 'updatePersonUser':
                $token = $_COOKIE['userid'];
                $sql_inquery = $ini_result['data']['updatePersonUser']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['name'],PDO::PARAM_STR);
                $state->bindValue(2,$args['email'],PDO::PARAM_STR);
                $state->bindValue(3,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->bindValue(4,$token,PDO::PARAM_STR);
                $state->execute();
                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return array("updatetime" => date("Y-m-d H:i:s"));
                }
            break;
            case 'updatePsd': //更新密碼
                $sql_inquery = $ini_result['data']['updatePsd']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,md5($args['password']),PDO::PARAM_STR);
                $state->bindValue(2,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'updatePersonPsd': //更新Person密碼
                $token = $_COOKIE['userid'];
                $sql_inquery = $ini_result['data']['updatePersonPsd']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,md5($args['password']),PDO::PARAM_STR);
                $state->bindValue(2,$token,PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'delUser': //刪除使用者
                $sql_inquery = $ini_result['data']['delUser']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['ser'],PDO::PARAM_INT);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'loginLogList': // 登入紀錄
                $sql_inquery = $ini_result['data']['loginLogList']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            default:
                # code...
            break;
        }
    }
     

    //base
    private static function iniread($filename) 
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = _error_message("general", "0003", "", $filename . " not found.");  //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }

}
?>