<?php if( !function_exists('Chk_Login') ) header('Location: ../../index.php'); ?>

<div class="abgne_tab">

    <ul class="tabs">
        <li><a href="javascript:void(0)" for-id="tab1">基本資料</a></li>
        <li><a href="javascript:void(0)" for-id="tab2">標章選取</a></li>
        <li><a href="javascript:void(0)" for-id="tab3">產品圖</a></li>
        <li><a href="javascript:void(0)" for-id="tab4">產品說明</a></li>
        <li><a href="javascript:void(0)" for-id="tab5">表格上傳</a></li>
    </ul>
    
    <div class="tab_container">
    
		

		<form id="form_edit_save" class="form-horizontal">
        
			<?php foreach( $_html_ as $key => $val ){ 
                    
                    $table_sn = 'tabsn'.$key;
            ?>
				<div class="Table_border <?=$table_sn?>">
            	
                    <input type="hidden" id="<?=$Main_Key?>" name="<?=$Main_Key?>[]" value="<?=$val[$Main_Key]?>">
                    <div id="tab1" class="tab_content">
        				<?php 
						//-----------------------------------------//

						//標題、副標題、描述、分類、首頁顯示

						$Arr_Name = 'Product_Code'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Name'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '請輸入'.$table_info[$Arr_Name]['Comment'], 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Name2'; 
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						
						//echo $_TF->html_select($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1, $Class_Arr[$Main_Key3], '');

						echo '<div class="form-group">';
						echo '	<label class="col-sm-2 control-label">第一層分類</label>';
						echo '	<div class="col-sm-9">';
						echo '		<select id="first_lv" msg="請選擇第一層分類" class="col-xs-12 col-sm-5 padding_none">';
						echo "		<option value='0'>請選擇第一層分類</option>";
						foreach ($LV1_Arr as $id => $name) {

							if( isset($val['ProductC_ID_LV1']) && !empty($val['ProductC_ID_LV1'])) {
								if( $val['ProductC_ID_LV1'] == $id){
									echo "		<option value='{$id}' selected>{$name}</option>";	
								}else{
									echo "		<option value='{$id}' >{$name}</option>";
								}
							}else{

								echo "		<option value='{$id}'>{$name}</option>";	
							}
						}
						echo  '	</select>';
						echo '	</div>';
						echo '</div>';

						//=================================================

						echo '<div class="form-group">';
						echo '	<label class="col-sm-2 control-label">第二層分類</label>';
						echo '	<div class="col-sm-9">';
						echo '		<input type="hidden" id="lv2_selected" value="'.$val['ProductC_ID_LV2'].'">';
						echo '		<select id="first_lv2" msg="請選擇第二層分類" class="col-xs-12 col-sm-5 padding_none">';
						if( isset($val['ProductC_ID_LV2_List']) && !empty($val['ProductC_ID_LV2_List']) ) {
							foreach ($val['ProductC_ID_LV2_List'] as $id => $v) {
								if( $v['selected'] == 1 ){
									echo "		<option value='{$id}' selected>{$v['ProductC_Name']}</option>";			
								}else{
									echo "		<option value='{$id}'>{$v['ProductC_Name']}</option>";		
								}
							}
						}
						echo "		<option value='0'>請選擇第二層分類</option>";
						echo  '	</select>';
						echo '	</div>';
						echo '</div>';

						//-----------------------------------------//
						$TurnCode = Turnencode('ProductC_ID');
						//$TurnCode = 'ProductC_ID';

						echo '<div class="form-group">';
						echo '	<label class="col-sm-2 control-label">第三層分類</label>';
						echo '	<div class="col-sm-9">';
						echo '		<select id="'.$TurnCode.'" name="'.$TurnCode.'[]" msg="請選擇第三層分類" class="col-xs-12 col-sm-5 padding_none">';
						echo "		<option value='0'>請選擇第三層分類</option>";
						if( isset($val['ProductC_ID_LV3_List']) && !empty($val['ProductC_ID_LV3_List']) ) {
							foreach ($val['ProductC_ID_LV3_List'] as $id => $v) {
								if( $v['selected'] == 1 ){
									echo "		<option value='{$id}' selected>{$v['ProductC_Name']}</option>";			
								}else{
									echo "		<option value='{$id}'>{$v['ProductC_Name']}</option>";		
								}
							}
						}
						echo  '	</select>';
						echo '	</div>';
						echo '</div>';

						//-----------------------------------------//

						$Arr_Name = 'Product_Intro';
				 
						echo $_TF->html_textarea($table_info[$Arr_Name]['Comment'], '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '');
						//-----------------------------------------//
						$Arr_Name = 'Product_Sort';
						
						echo $_TF->html_number($table_info[$Arr_Name]['Comment'], '', '', $table_info[$Arr_Name]['Field_Length'], $Arr_Name, $val, '', 1, 0, 99999);
						//-----------------------------------------//
						$Arr_Name = 'Product_OpenIndex';
						
						echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Open';
						
						echo $_TF->html_checkbox($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1);
						//-----------------------------------------//
						$Arr_Name = 'Product_Sdate';
						
						echo $_TF->html_text($table_info[$Arr_Name]['Comment'], '', '', '', $Arr_Name, $val, '', 0);
						?>
        	
					</div>   
					<div id="tab2" class="tab_content">
        				<?php 
						//-----------------------------------------//

						//標章checkbox
						$Arr_Name = 'Product_Signlist'; 
						
						echo $_TF->html_checkbox_muti($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val, '', 1,'Product_Signlist',$_SignList);
						//-----------------------------------------//
						
						?>
        	
					</div>  

					<div id="tab3" class="tab_content">
        				<?php 
						//圖檔
						//-----------------------------------------//
						$Arr_Name = 'productl_Img';
					
						echo $_TF->html_uploadimg('多圖片上傳', '', $Arr_Name, $val, '', 10485760, true, array('name' => $Main_TablePre.'_Mcp'));
						//-----------------------------------------//
						?>
        	
					</div>   


					<div id="tab4" class="tab_content">
        				<?php 
						//圖檔
						//-----------------------------------------//
					
						$Arr_Name = 'Product_desc';

						$TurnCode_1 = Turnencode( "Product_desc1");
						$TurnCode_2 = Turnencode( "Product_desc2");
						
						$_arr1 = unserialize( $val["Product_desc1"] );
						$_arr2 = unserialize( $val["Product_desc2"] );
						
	                    echo "<div class='form-group'>";
	                    echo "<label class='col-sm-2 control-label'>產品說明列表</label>";
	                    echo "<div class='col-sm-9 InproBox'>";
						echo "<p class='drop_tip'>調整產品說明排序請直接拖拉藍色區塊排序</p>";
	                    echo "<ul class='Inpro_list drag-sort-enable'>";
						
						if( !empty($_arr1) && !empty($_arr2) ) {
						
							foreach( $_arr1 as $key_s => $val_s ){
								
								echo '<li class="dragbox">';
								echo '	<div class="dragbox__form">';
								echo '		<input class="wd120" type="text"  name="'.$TurnCode_1.'[]"  value="'.$val_s.'" placeholder="" autocomplete="off">';
								echo '		<input class="" type="text"  name="'.$TurnCode_2.'[]"  value="'.$_arr2[$key_s].'" placeholder="" autocomplete="off">';
								echo '	</div>';
								echo '	<div class="dragbox__delete">';
								echo '		<a class="delete_item"><i class="fa fa-times-circle"></i></a>';
								echo '	</div>';
								echo '</li>';
							}
						}
						
						echo "</ul>";
						

	                    echo "<button type='button' class='Inpro_additem'>新增說明</button>";
	                    echo "</div></div>";
						//-----------------------------------------//
						?>
        	
					</div>       

					<div id="tab5" class="tab_content">
        				<?php 
						//表格上傳
						$yes = '<span style="color:blue;">已上傳</span>';
						$no = '<span style="color:red;">未上傳</span>';
						$btn1 = "<a href='javascript:;' onclick='OP_TEST(\"Product_table1\")'>刪除</a>";
						$btn2 = "<a href='javascript:;' onclick='OP_TEST(\"Product_table2\")'>刪除</a>";
						$btn3 = "<a href='javascript:;' onclick='OP_TEST(\"Product_table3\")'>刪除</a>";
						//-----------------------------------------//
						echo "<p class='drop_tip'>如需更新表格請直接上傳，結果於前台產品頁呈現</p>";
						echo "<p class='drop_tip'>【規格表】上傳狀態 : ". (!empty($val['Product_table1']) ?$yes.$btn1:$no)."</p>";
						echo "<p class='drop_tip'>【切削數據】上傳狀態 : ". (!empty($val['Product_table2']) ?$yes.$btn2:$no)."</p>";
						echo "<p class='drop_tip'>【公差表】上傳狀態 : ". (!empty($val['Product_table3']) ?$yes.$btn3:$no)."</p>";

						$Arr_Name = 'Product_table1';
						$val['Product_table1'] = '';
						echo $_TF->html_uploadfile($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val );
						//-----------------------------------------//
						$Arr_Name = 'Product_table2';
						$val['Product_table2'] = '';
						echo $_TF->html_uploadfile($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val );
						//-----------------------------------------//
						$Arr_Name = 'Product_table3';
						$val['Product_table3'] = '';
						echo $_TF->html_uploadfile($table_info[$Arr_Name]['Comment'], '', $Arr_Name, $val );
						//-----------------------------------------//
						?>
        	
					</div>        
                         
	             </div>
	            <?php } ?>
			
                <div class="clear_both form-actions">
                    <button id="saveb" class="btn btn-info" type="button" onclick="form_edit_save()">
                        <i class="ace-icon fa fa-check bigger-110"></i>儲存
                    </button>&nbsp;&nbsp;&nbsp; 
                            
                    <button id="rsetb" class="btn btn" type="reset">
                        <i class="ace-icon fa fa-check bigger-110"></i>重設
                    </button>
                </div>
            </form>
		
    </div>
</div>


<script type="text/javascript">
	
	$(document).ready(function(e) {
	
	enableDragSort('drag-sort-enable');

	OP_TEST = function(  fields ){

		var FormData = 'id=<?=$val[$Main_Key]?>';

		FormData += '&fields='+fields;
		FormData += '&table=<?=$Main_Table?>';

		$.ajax({
	        url: '../ajax/ajax_product_table_del.php',
	        data: FormData,
	        type: "POST",
	        dataType: 'json',
	        success: function(data){
	          	location.reload();
	        },
	        error:function(xhr, ajaxOptions, thrownError){ 
	          alert(thrownError);
	        }
	      });

	}
	$('#first_lv').change(function(){

		var FormData = 'id='+$(this).val();

		FormData += '&table=<?=$Main_Table3?>';

		$.ajax({
	        url: '../ajax/ajax_product_category.php',
	        data: FormData,
	        type: "POST",
	        dataType: 'json',
	        success: function(data){
	          
	          	$("#first_lv2").empty();
	          	$("#first_lv2").append("<option value='0'>請選擇第二層分類</option>");
	          	$.each( data ,function (key,value){

	          		$("#first_lv2").append("<option value='"+key+"'>"+value+"</option>");
				});
	        },
	        error:function(xhr, ajaxOptions, thrownError){ 
	          alert(thrownError);
	        }
	      });
	});

	$('#first_lv2').change(function(){

		var FormData = 'id='+$(this).val();
		
		FormData += '&table=<?=$Main_Table3?>';

		$.ajax({
        url: '../ajax/ajax_product_category.php',
        data: FormData,
        type: "POST",
        dataType: 'json',
        success: function(data){
          	
          	$("#<?=$TurnCode?>").empty();

          	$("#<?=$TurnCode?>").append("<option value='0'>請選擇第三層分類</option>");
          	$.each( data ,function (key,value){
          		$("#<?=$TurnCode?>").append("<option value='"+key+"'>"+value+"</option>");
			});
        },
        error:function(xhr, ajaxOptions, thrownError){ 
          alert(thrownError);
        }
      });
	});

	$('.Inpro_additem').click(function(){
		
		var content = '';
		var TurnCode_1 = '<?=$TurnCode_1?>';
		var TurnCode_2 = '<?=$TurnCode_2?>';
		
		content += '<li class="dragbox">'
		
		content += '<div class="dragbox__form">'
		content += '<input class="wd120" type="text"  name="'+TurnCode_1+'[]"  value="" msg="" maxlength="1000" placeholder="" autocomplete="off">';
		content += '<input class="" type="text"  name="'+TurnCode_2+'[]"  value="" msg="" maxlength="1000" placeholder="" autocomplete="off">';
		content += '</div>'
		content += '<div class="dragbox__delete">'
		content += '<a class="delete_item"><i class="fa fa-times-circle"></i></a></div></li>';

		
		$(content).appendTo('.Inpro_list');

	});

	

});

	$("body").on("click",".delete_item",function(){
	     $(this).parents('.dragbox').remove();
	 });
function enableDragSort(listClass) {
  const sortableLists = document.getElementsByClassName(listClass);
  Array.prototype.map.call(sortableLists, (list) => {enableDragList(list)});
}

function enableDragList(list) {
  Array.prototype.map.call(list.children, (item) => {enableDragItem(item)});

}

function enableDragItem(item) {
  item.setAttribute('draggable', true)
  item.ondrag = handleDrag;
  item.ondragend = handleDrop;
}

function handleDrag(item) {
  const selectedItem = item.target,
        list = selectedItem.parentNode,
        x = event.clientX,
        y = event.clientY;
  
  selectedItem.classList.add('drag-sort-active');
  let swapItem = document.elementFromPoint(x, y) === null ? selectedItem : document.elementFromPoint(x, y);
  
  if (list === swapItem.parentNode) {
    swapItem = swapItem !== selectedItem.nextSibling ? swapItem : swapItem.nextSibling;
    list.insertBefore(selectedItem, swapItem);
  }
}

function handleDrop(item) {
  item.target.classList.remove('drag-sort-active');
}

$('body').on('click', '.Inpro_additem', function ()
{
	 enableDragSort('drag-sort-enable');
});
</script>
<script type="text/javascript">$('.imgajax').colorbox({width:"70%", height:"100%", rel:'imgajax'});</script>