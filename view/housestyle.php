<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column ">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
          <!-- 屋款介紹 -->
        <div id="intrp-style-wrap" class="d-flex flex-column align-items-center p-0 m-0">
            <div class="intrp-style-content">
                <h4 class="text-center">– SPECIFICATION –</h4>
                <h2 class="text-center">屋款介紹</h2>
                <div class="intro-items mx-auto">
                    <div class="owl-carousel owl-theme loop">
                        <?php 
                            foreach ($data['style_header'] as $item) {
                                echo '<div class="item">
                                        <a href="#'.$item['Style_ID'].'" class="text-decoration-none">
                                            <div class="img-wrap">
                                                <h3 class="position-absolute w-100 text-center mb-0">'.$item['Style_Name'].'</h3>
                                                <img class="img-1x1" src="'.((isset($item['Style_Img_Desp']))?$item['Style_Img_Desp']:'assets/01B_木屋款式/00_分類選單/01.jpg').'" alt="'.$item['Style_Name'].'">
                                            </div>
            
                                        </a>
                                        
                                    </div>';
                            }
                        ?>
                        <!-- <div class="item">
                            <a href="#div1" class="text-decoration-none">
                                <div class="img-wrap">
                                    <h3 class="position-absolute w-100 text-center">家庭長住</h3>
                                    <img class="" src="assets/01B_木屋款式/00_分類選單/01.jpg" alt="ononNature屋款介紹">
                                </div>

                            </a>
                            <p class="mx-auto mt-3">自然紅屋不止自然紅屋不只講求居好屋。更希望「以行動木屋帶您親近自然」，透過天然的素材，感受萬物的恩惠。希</p>
                        </div>
                        <div class="item">
                            <div class="img-wrap">
                                <h3 class="position-absolute w-100 text-center">休閒開發</h3>
                                <img class="" src="assets/01B_木屋款式/00_分類選單/02.jpg" alt="ononNature屋款介紹">
                            </div>
                            <p class="mx-auto mt-3">自然紅屋不止自然紅屋不只講求居好屋。更希望「以行動木屋帶您親近自然」，透過天然的素材，感受萬物的恩惠。希</p>
                        </div>
                        <div class="item">
                            <div class="img-wrap">
                                <h3 class="position-absolute w-100 text-center">特殊商品</h3>
                                <img class="" src="assets/01B_木屋款式/00_分類選單/03.jpg" alt="ononNature屋款介紹">
                            </div>
                            <p class="mx-auto mt-3">自然紅屋不止自然紅屋不只講求居好屋。更希望「以行動木屋帶您親近自然」，透過天然的素材，感受萬物的恩惠。希</p>
                        </div>
                        <div class="item">
                            <div class="img-wrap">
                                <h3 class="position-absolute w-100 text-center">周邊配件</h3>
                                <img class="" src="assets/01B_木屋款式/00_分類選單/04.jpg" alt="ononNature屋款介紹">
                            </div>
                            <p class="mx-auto mt-3">自然紅屋不止自然紅屋不只講求居好屋。更希望「以行動木屋帶您親近自然」，透過天然的素材，感受萬物的恩惠。希</p>
                        </div> -->
                    </div>
                    <p class="mx-auto px-0 intro-items-desp"><?php echo  $data['style_header'][0]['Style_Content_Desp'];?></p>
                </div>
            </div>            
        </div>
        <?php
            $sizes = 1;
            foreach ($data['style_header'] as $item) {
                $pd = 0;
                foreach ($data['style_pdesp'] as $pdesp) {
                    
                    if(strpos($item['Style_ID'], $pdesp['Style_UpMID']) !== false )
                    {
                        echo '<div class="living-content d-flex flex-column  px-0 mx-0 '.(($pdesp['Style_ID_Desp'] == 1)?'':'').'">';
                        echo ($pd == 0)?'<h3 class="text-center '.(($sizes == 1)?'':'fisrt-el').' mb-0" id="'.$item['Style_ID'].'">– '.$item['Style_Name'].' –</h3>':'';
                            echo '<div class="d-flex flex-row px-0 mx-0 items">';
                                echo '<div class=" item-content">';
                                    echo '<div class="d-flex flex-row justify-content-md-end justify-content-center">
                                            <div class="item-content-2 mb-3">
                                                <div class="item-content-title">
                                                    <h3 class="mb-0">'.$pdesp['Style_Name'].'</h3>
                                                    <h5>'.$pdesp['Style_Intro'].'</h5>
                                                </div>
                                                <div class="item-content-paragraph">
                                                    <p>
                                                    '.$pdesp['Style_Content'].'
                                                    </p>
                                                    <div class="item-content-btn">
                                                        <div class="viewmoreBt text-center" data-toggle="modal" data-target="#showModal_'.$pdesp['Style_ID'].'_'.$pd.'">平剖示意圖</div>
                                                    </div>
                                                </div>
                                                
                                            </div>';
                                    echo '</div>'; 
                                    echo  '<div class="modal fade alert-modal " id="showModal_'.$pdesp['Style_ID'].'_'.$pd.'" tabindex="-1" aria-labelledby="'.$pdesp['Style_ID'].'_'.$pd.'ModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content modal-content-visit">
                                                        <div class="modal-header border-0">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">                                      
                                                            <h3 class="text-center mt-n3">'.$pdesp['Style_Name'].'</h3>                                  
                                                            <div id="carousel'.$pdesp['Style_ID'].'_'.$pd.'Controls" class="carousel slide" data-interval="false">
                                                                <div class="carousel-inner">';
                                                                    $pc_inner = 0;
                                                                    foreach ($data['style_cover'] as $pcover) {
                                                                        if(strpos($pdesp['Style_ID'], $pcover['Style_ID']) !== false )
                                                                        {
                                                                            echo '<div class="carousel-item '.(($pc_inner == 0)?'active':'').'">
                                                                                    <img src="'.((isset($pcover['Style_Img_Cover']))?$pcover['Style_Img_Cover']:'#').'" class="d-block w-100" alt="'.$pdesp['Style_Name'].'">
                                                                                </div>';
                            
                                                                            $pc_inner++;
                                                                        }
                                                                    }
                                    echo                        '</div>
                                                                <a class="carousel-control-prev" href="#carousel'.$pdesp['Style_ID'].'_'.$pd.'Controls" role="button" data-slide="prev">
                                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                                    <span class="sr-only">Previous</span>
                                                                </a>
                                                                <a class="carousel-control-next" href="#carousel'.$pdesp['Style_ID'].'_'.$pd.'Controls" role="button" data-slide="next">
                                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                                    <span class="sr-only">Next</span>
                                                                </a>
                                                            </div>
                                                            <p class="carousel-paragraph text-center">
                                                            ※圖片僅供參考，實際規格依合約為準。
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>';
                                echo '</div>';
                                echo '<div class="item-carousel">';
                                    echo '<div id="carousel'.$pdesp['Style_ID'].'Indicators" class="carousel slide" data-interval="false">';
                                        echo '<ol class="carousel-indicators">';
                                        $pi = 0;
                                        foreach ($data['style_pimg'] as $pimg) {
                                            if(strpos($pdesp['Style_ID'], $pimg['Style_ID']) !== false )
                                            {
                                                echo '<li data-target="#carousel'.$pdesp['Style_ID'].'Indicators" data-slide-to="'.$pi.'" class="'.(($pi == 0)?'active':'').'"></li>';

                                                $pi++;
                                            }
                                        }
                                        echo '</ol>';
                                        echo '<div class="carousel-inner">';
                                        $pi_inner = 0;
                                        foreach ($data['style_pimg'] as $pimg) {
                                            if(strpos($pdesp['Style_ID'], $pimg['Style_ID']) !== false )
                                            {
                                                echo '<div class="carousel-item '.(($pi_inner == 0)?'active':'').'">
                                                        <img src="'.((isset($pimg['Style_Img_Img']))?$pimg['Style_Img_Img']:'#').'" class="d-block" alt="'.$pdesp['Style_Title'].'">
                                                    </div>';

                                                $pi_inner++;
                                            }
                                        }
                                        echo '</div>';
                                        echo '<a class="carousel-control-prev" href="#carousel'.$pdesp['Style_ID'].'Indicators" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carousel'.$pdesp['Style_ID'].'Indicators" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                        $pd++;
                    }
                }
                $sizes++;
            }
        ?>
        

         <!-- Top -->
         <div class="gototop-wrap d-flex flex-column align-items-center p-0 m-0">
            <div class="intrp-style-content">
                <h2 class="text-center m-0"><i class="fas fa-arrow-up"></i></h2>
                <h2 class="text-center" id="gotoTop">Top</h2>
            </div>            
        </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->   
    <script src="dist/script/vendor/jquery-1.11.3.min.js"></script>  
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script>
        jQuery(function($){
            $('.loop').owlCarousel({
                dots: false,
                nav: true,
                navText: ["<img src='assets/0_共用/arrow_black.svg' style='transform: rotate(180deg)'>","<img src='assets/0_共用/arrow_black.svg'>"],
                // center: true,
                loop:false,
                lazyLoad:true,
                items:3,
                margin:60,
                responsiveClass:true,
                responsive:{
                    // 599:{
                    //     items:4,
                    // }
                }
            });
        });
    </script>

    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script>
        $('#wrap-body').imagesLoaded()
            .always( function( instance ) {
                console.log('all images loaded');
            });
    </script>   
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/housestyle.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>