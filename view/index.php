<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column ">
      <div id="wrap-header">
        <?php require('layout/Header.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0 overflow-hidden"> 
          <!-- 首頁影片 -->
        <div id="video-wrap" class="d-flex p-0 m-0">
            <div class="flex-fill">
                <video  autoplay muted loop class="w-100">
                    <source src="<?php echo (isset($data['home_header']['Home_FileHeader']))?$data['home_header']['Home_FileHeader']:'assets/00_首頁/00_header/video.mp4'; ?>" type="video/mp4" id="header-video">
                    Your browser does not support the video tag.
                </video>
            </div>            
        </div>
        <!-- 快速連結 -->   
        <div id="link-wrap" class="row p-0 m-0 w-100">
            <div class="col-sm-6 p-0 item">
                <div class="link-content bg1" style="<?php echo (isset($data['home_link']))?'background: url(\''.$data['home_link'][0]['Home_File_Link'] .'\') no-repeat;':'';?>">
                    <div class="cover w-100">
                        <div class="ms-3 text-white text-center">
                            <p class="link-title p-0 m-0"><?php echo $data['home_link'][0]['Home_Title_Link'] ?></p>
                            <p class="link-subtitle p-0 m-0"><?php echo $data['home_link'][0]['Home_Intro_Link'] ?></p>
                            <p class="link-description pt-1 m-0"><?php echo $data['home_link'][0]['Home_Content_Link'] ?></p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-sm-6 p-0 item">
                <div class="link-content bg2" style="<?php echo (isset($data['home_link']))?'background: url(\''.$data['home_link'][1]['Home_File_Link'] .'\') no-repeat;':'';?>">
                    <div class="cover">
                        <div class="ms-3 text-white text-center">
                            <p class="link-title p-0 m-0"><?php echo $data['home_link'][1]['Home_Title_Link'] ?></p>
                            <p class="link-subtitle p-0 m-0"><?php echo $data['home_link'][1]['Home_Intro_Link'] ?></p>
                            <p class="link-description pt-1 m-0"><?php echo $data['home_link'][1]['Home_Content_Link'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 紅屋介紹 -->
        <div id="intro-wrap" class="row p-0 m-0 w-100">
            <div class="container px-0">
                <h3 class="text-center"><?php echo $data['home_desp']['Home_Title_Desp']; ?></h3>
                <div class="intro-content-desp mx-auto mb-lg-5">
                <?php echo $data['home_desp']['Home_Content_Desp']; ?>
                </div>
                
                <!-- <p class="text-center mx-auto mb-5">
                
                匯集「食住農」的整合，提出「人文回村」的體驗學習活動。適逢疫情橫行，行動木屋又多了一個大任務，「防疫逃城」。原本是為退休人找新幸福，如今更為眾人安身養命。 
                </p>
                <p class="text-center mx-auto">
                匯集「食住農」的整合，提出「人文回村」的體驗學習活動。適逢疫情橫行，行動木屋又多了一個大任務，「防疫逃城」。適逢疫情橫行，行動木屋又多了一個大任務，「防疫逃城」。
                </p> -->
                <div class="row">
                    <a href="<?php echo (!empty($data['home_desp']['Home_Link_Desp']))?$data['home_desp']['Home_Link_Desp']:'#'; ?>" class="viewmoreBt text-center mx-auto text-decoration-none">view more</a>
                </div>
            </div>
        </div>
        <!-- BLOG -->
        <div id="blog-wrap" class="row p-0 m-0 w-100">
            <div class="container-fluid mx-0 px-0">
                <h3 class="col-12 text-center">
                    <span>BLOG</span>
                </h3>
                <div class="row px-0 blog-resp" id="blog-content">
                    <div id="blog-resp">
                    <?php for ($i=0; $i < count($data['blog_dairy']); $i++) { 
                        $day = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[2];
                        $momth = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[1];
                        $year = explode('-', substr( $data['blog_dairy'][$i]['Blog_Sdate_Dairy'] , 0 , 10 ))[0];
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-3 mb-2">
                            <div class="card">
                                <div class="img-wrap">
                                    <div class="img-1x1" style="background-image: url(<?php echo (!empty($data['blog_dairy'][$i]['Blog_Img_Dairy']))?"'".$data['blog_dairy'][$i]['Blog_Img_Dairy']."'": 'assets/04_紅屋日誌/可用的圖/06.jpg'; ?>);"></div>
                                    <!-- <img src="" class="" alt="紅屋日誌"> -->
                                </div>
                                
                                <div class="card-body p-0">
                                    <p class="card-text mb-2 mt-4"><small class="text-muted"><?php echo $day . ' ' . $month_en[($momth * 1) - 1] . ' ' .  $year; ?></small> </p>
                                    <h5 class="card-title mt-0 text-truncate"><a href="./Blogdetail?title=<?php echo $data['blog_dairy'][$i]['Blog_ID_Dairy']; ?>" class="text-reset"><?php echo $data['blog_dairy'][$i]['Blog_Title_Dairy']; ?></a></h5>
                                    <p class="card-paragraph text-break overflow-hidden" style=""><?php echo  strip_tags($data['blog_dairy'][$i]['Blog_Content_Dairy']);?></p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <!-- <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                        <div class="card">
                            <div class="img-wrap">
                                <div class="img-1x1" style="background-image: url('assets/04_紅屋日誌/可用的圖/06.jpg');"></div>
                            </div>
                            
                            <div class="card-body p-0">
                                <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                <h5 class="card-title mt-0 text-truncate">自由，從思考居住空間開始自由，從思考居住空間開始自由，從思考居住空間開始自由，從思考居住空間開始</h5>
                                <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                        <div class="card">
                            <div class="img-wrap">
                                <div class="img-1x1" style="background-image: url('assets/01_行動木屋/06_精選案例/01.jpg');"></div>
                            </div>
                            
                            <div class="card-body p-0">
                                <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                <h5 class="card-title mt-0 text-truncate">自由，從思考居住空間開始</h5>
                                <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                        <div class="card">
                            <div class="img-wrap">
                                <div class="img-1x1" style="background-image: url('assets/01_行動木屋/06_精選案例/02.jpg');"></div>
                            </div>
                            
                            <div class="card-body p-0">
                                <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                <h5 class="card-title mt-0 text-truncate" >自由，從思考居住空間開始</h5>
                                <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-2">
                        <div class="card">
                            <div class="img-wrap">
                                <div class="img-1x1" style="background-image: url('assets/01_行動木屋/06_精選案例/03.jpg');"></div>
                            </div>
                            
                            <div class="card-body p-0">
                                <p class="card-text mb-2 mt-4"><small class="text-muted">01 Jan 2020</small> </p>
                                <h5 class="card-title mt-0 text-truncate">自由，從思考居住空間開始</h5>
                                <p class="card-text text-break">「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開的。」</p>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="row text-center">
                    <a href="./Blog" class="viewmoreBt text-center mx-auto text-decoration-none">view more</a>
                </div>
            </div>
        </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/index.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>