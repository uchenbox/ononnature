<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-2.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
        <!-- 首頁大圖 -->
        <div id="image-wrap" class="row p-0 m-0 overflow-hidden">
            <div class="owl-carousel owl-theme header-loop  overflow-hidden">
                <?php for ($i=0; $i < count($data['on_header']); $i++) { ?>
                    <div class="item">
                        <img class="h-100" src="<?php echo (!empty($data['on_header'][$i]['ON_Img_Header']))?$data['on_header'][$i]['ON_Img_Header']: ''; ?>" alt="<?php echo $alt; ?>">
                        <div id="title-wrap"  class="d-flex d-column align-self-center ">
                            <!-- <div class="mr-3"> -->
                                <?php //echo str_replace('。', '<br/>。', str_replace('，', '<br/>，', str_replace('p>', 'div class="mr-3">', $data['on_header'][$i]['ON_Intro_Header']))); ?>
                            <!-- </div> -->
                            <!-- <div class="mr-3">青銀自在 <br/>。</div> -->
                            <!-- <div>輕盈住宅<br/>，</div> -->
                        </div>
                        <div id="nav-wrap"  class="d-flex d-column align-self-center ">
                            <p><span class="customPrevBtn"> < </span> <span><?php echo ($i + 1) ?>/<?php echo count($data['on_header']);?></span> <span class="customNextBtn"> > </span></p>
                        </div>
                    </div>
                <?php } ?>
                <!-- <div class="item">
                    <img class="h-100" src="assets/02_OnOnNature/00_header slides/01.jpg" alt="ononNature">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div>
                <div class="item">
                    <img class="h-100" src="assets/02_OnOnNature/00_header slides/02.jpg" alt="ononNature">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div>
                <div class="item">
                    <img class="h-100" src="assets/02_OnOnNature/00_header slides/03.jpg" alt="ononNature">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div>
                <div class="item">
                    <img class="h-100" src="assets/02_OnOnNature/00_header slides/04.jpg" alt="ononNature">
                    <div id="title-wrap"  class="d-flex d-column align-self-center ">
                        <div class="mr-3">青銀自在 <br/>。</div>
                        <div>輕盈住宅<br/>，</div>
                    </div>
                </div> -->
            </div>
            <!-- <img src="assets/01_行動木屋/00_header/01.jpg" alt="ononNature行動木屋" class="w-100"> -->
            <div id="title-wrap"  class="d-flex d-column align-self-center ">
                <?php echo str_replace('。', '<br/>。', str_replace('，', '<br/>，', str_replace('p>', 'div class="mr-3">', $data['on_hdesp'][0]['ON_Intro_Hdesp']))); ?>
                <!-- <div class="mr-3">青銀自在 <br/>。</div> -->
                <!-- <div>輕盈住宅<br/>，</div> -->
            </div>
        </div> 
        
        <!-- OnOnNature -->
        <div class="on-desp-content d-flex flex-column  px-0 mx-0">
            <div class="container-fluid px-0">
                <h3 class="mx-auto"><?php echo $data['on_desp'][0]['ON_Title_Desp']; ?></h3>
                <span class="response-show"><?php echo $data['on_intro'][0]['ON_Title_Intro']; ?></span>
                <div class="d-flex flex-row mx-auto items">
                    <div class=" px-0 mx-0 item-intro response-hidden">
                        <h3 class="mx-auto"><?php echo str_replace('、', '<br/>、', $data['on_intro'][0]['ON_Title_Intro']); ?></h3>                        
                    </div>
                    <div class=" px-0 mx-0 item-desp">
                        
                        <p class="px-0">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_desp'][0]['ON_Content_Desp']); ?>
                        <br>
                        <div class="time-paragraph d-flex flex-row px-0">
                            <strong class="float-left">Opening <br> Hours</strong>
                            <p class="float-left"><?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_desp'][0]['ON_Intro_Desp']); ?></p>
                        </div>
                        
                        </p>
                    </div>
                </div>
                <div class="row px-0 mx-auto pitems">
                    <div class="col-12 px-0 text-center">
                        <img src="<?php echo (!empty($data['on_intro'][0]['ON_Img_Intro']))?$data['on_intro'][0]['ON_Img_Intro']: 'assets/02_OnOnNature/01_概述/onon.jpg'; ?>" alt="OnOnNature">
                    </div>
                    <div class="col-12 px-0 text-center last-el">
                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_intro'][0]['ON_Content_Intro']); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- 食 health -->
        <div class="on-left-content d-flex flex-column  px-0 mx-0">
            <div class="on-left-wrap px-0">
                <div class="d-flex flex-row p-0 m-0">
                    <div class="px-0 img-content">
                        <img class="w-100" src="<?php echo (!empty($data['on_pdesp'][0]['ON_Img_Pdesp']))?$data['on_pdesp'][0]['ON_Img_Pdesp']: ''; ?>" alt="<?php echo $alt; ?>" >
                    </div>
                    <div class="px-0 d-flex flex-column title-content">
                        <div>
                            <div class="title-wrap d-flex flex-row">
                                <div class="text-center float-left big-word-wrap">
                                    <span class="big-word"><?php echo $data['on_pdesp'][0]['ON_Title_Pdesp']; ?></span>
                                </div>
                                <div style="margin-top: auto;">
                                    <span class="small-word mr-1"><?php echo $data['on_pdesp'][0]['ON_Intro_Pdesp']; ?></span>
                                </div>
                                
                            </div>                    
                            <div class="desp-wrap float-none response-hidden">
                                <p>
                                <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_pdesp'][0]['ON_Content_Pdesp']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="traffic-items item-2">
                    <div class="bg-gray float-right d-flex flex-column">
                        <div class="desp-wrap response-show">
                            <p>
                            <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_pdesp'][0]['ON_Content_Pdesp']); ?>
                            </p>
                        </div>
                    </div>
                </div> 
            </div>
        </div>

        <!-- 住 style -->
        <div class="on-left-content on-live-content d-flex flex-column  px-0 mx-0">
            <div class="on-left-wrap px-0">
                <div class="d-flex flex-column px-0 mx-0">
                    <div class="px-0 img-content style-img-content">
                        <img class="w-100 " src="<?php echo (!empty($data['on_pdesp'][1]['ON_Img_Pdesp']))?$data['on_pdesp'][1]['ON_Img_Pdesp']: ''; ?>" alt="<?php echo $alt; ?>">

                    </div>    
                    <div class="d-flex flex-row items">
                        <div class="style-title-content">
                            <div class="title-wrap float-left">
                                <div class="text-left float-left big-word-wrap d-flex">
                                    <div class="float-left">
                                        <span class="big-word"><?php echo $data['on_pdesp'][1]['ON_Title_Pdesp']; ?></span>
                                        <span class="small-word mr-1 response-hidden"><?php echo $data['on_pdesp'][1]['ON_Intro_Pdesp']; ?></span>
                                    </div>                                    
                                    <div class="float-left">
                                        <p class="desp">
                                        <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_pdesp'][1]['ON_Content_Pdesp']); ?>
                                        </p>
                                    </div>
                                </div>                           
                                <!-- <div class="desp-wrap float-left">
                                    <p>
                                        
                                    </p>
                                </div> -->
                            </div>                    
                            
                        </div>
                    </div>                
                </div>
                
            </div>
        </div>

        <!-- 農 NATURE -->
        <div class="on-right-content d-flex flex-column  px-0 mx-0">
            <div class="on-right-wrap px-0">
                <div class="d-flex flex-column p-0 m-0">    
                    <div class="px-0 img-content nature-img-content">
                        <img class="w-100 " src="<?php echo (!empty($data['on_pdesp'][2]['ON_Img_Pdesp']))?$data['on_pdesp'][2]['ON_Img_Pdesp']: ''; ?>" alt="<?php echo $alt; ?>">

                    </div>  
                                    
                </div>
                <div class="d-flex flex-row px-0 mx-0 ">
                        <div class="items">
                            <div class="title-wrap d-flex">
                                <div class="text-left  big-word-wrap d-flex">  
                                    <div class="desp float-left">
                                    <?php echo preg_replace("/\r\n|\n|\r/", "<br/>", $data['on_pdesp'][2]['ON_Content_Pdesp']); ?>
                                    </div>
                                    <div class="float-left text-center">
                                        <span class="big-word"><?php echo $data['on_pdesp'][2]['ON_Title_Pdesp']; ?></span>
                                        <br>
                                        <span class="small-word mr-1 "><?php echo $data['on_pdesp'][2]['ON_Intro_Pdesp']; ?></span>  
                                    </div>                               
                                </div>                           
                                <!-- <div class="desp-wrap float-left">
                                    <p>
                                        
                                    </p>
                                </div> -->
                            </div>   
                        </div>
                    </div>  
            </div>
        </div>

        <!-- Link -->
        <div class="on-link-content d-flex flex-column  p-0 mx-0">
            <div class="on-link-wrap px-0">
                <div class="row px-0  ">
                    <div class="col-12 px-0 mx-0 text-center item-title">
                        <h3 class="mb-0">LINKS</h3>
                    </div>
                </div>
                <div class="px-0 mx-auto items d-flex flex-row">
                    <?php 
                        foreach ($data['on_link'] as $item) {
                            echo '<div class=" px-0 mx-auto item-content">
                                        <div class="img-wrap mx-auto position-relative">
                                            <h3 class="img-title position-absolute">'.$item['ON_Title_Link'].'</h3>
                                            <div class="img">
                                                <a href="'.((!empty($item['ON_Link_Link']))?$item['ON_Link_Link']: '#').'" class="text-reset">
                                                    <img class="" src="'.((!empty($item['ON_Img_Link']))?$item['ON_Img_Link']: '#').'" alt="'.$item['ON_Title_Link'].'">
                                                </a>
                                            </div>
                                        </div>
                                    </div>';
                        }
                    
                    ?>
                    <!-- <div class="col-md-4 px-0 mx-auto item-content">
                        <div class="img-wrap mx-auto position-relative">
                            <h3 class="img-title position-absolute">木屋參訪</h3>
                            <div class="img">
                                <a href="#" class="text-reset">
                                    <img class="" src="assets/02_OnOnNature/03_links/01.jpg" alt="OnonNature Links">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 px-0 mx-auto item-content">
                        <div class="img-wrap mx-auto position-relative">
                            <h3 class="img-title position-absolute">餐點介紹</h3>
                            <div class="img">
                                <a href="#" class="text-reset">
                                    <img class="" src="assets/02_OnOnNature/03_links/02.jpg" alt="OnonNature Links">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 px-0 mx-auto item-content">
                        <div class="img-wrap mx-auto position-relative">
                            <h3 class="img-title position-absolute">園區交通</h3>
                            <div class="img">
                                <a href="#" class="text-reset">
                                    <img class="" src="assets/02_OnOnNature/03_links/03.jpg" alt="OnonNature Links">
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- FAQ -->
        <div class="on-faq-content d-flex flex-column  p-0 mx-0">
            <div class="px-0 mx-0 faq-content ">
                <div class="">
                    <div class="row px-0 mx-0 item-title">
                        <div class="col-12">
                            <h3 class="text-center px-0 mx-0">FAQ</h3>
                        </div>
                    </div> 
                    <div class="d-flex px-0 mx-0 item-content">
                        <div class="accordion" id="accordionExample">
                            <?php 
                            $i = 0;
                            foreach ($data['faq_on'] as $item) {
                                echo '<div class="card">
                                    <div class="card-header p-0" id="heading'.$i.'">
                                        <h2 class="mb-0 p-0 collapsed" data-toggle="collapse" data-target="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'">
                                        '.$item['Faq_Intro_Answer'].'
                                        <span class="collapseBt float-right"></span>
                                        </h2>
                                    </div>

                                    <div id="collapse'.$i.'" class="card-collapse collapse" aria-labelledby="heading'.$i.'" data-parent="#accordionExample">
                                        <div class="card-body">
                                        '.$item['Faq_Content_Answer'].'
                                        </div>
                                    </div>
                                </div>';
                                $i++;
                            }
                            ?>                                    
                        </div>
                    </div>
                    <div class="row p-0 m-0">
                        <a href="./Faq" class="viewmoreBt text-center mx-auto text-decoration-none">view more</a>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Gallery -->
        <div class="on-gallery-content d-flex flex-column  p-0 mx-0">
            <div class="row p-0 mx-0 w-100" id="cases-content">
                <div class="cases-wrap">
                    <div class="row px-0 mx-0 item-title">
                        <div class="col-12 text-center">
                            <h3 class="mb-0">GALLERY</h3>
                        </div>
                    </div>
                    <div class="row p-0 m-0 items mx-auto">
                        <?php 
                            $gallery_count = 1;
                            foreach ($data['on_gallery'] as $item) {
                                echo '<div class=" item gallery-item '.(($gallery_count > 6)?'d-none':'').'">
                                        <a href="'.((isset($item['ON_Img_Gallery']))?$item['ON_Img_Gallery']:'#').'" data-lightbox="sky" data-title="'.$item['ON_Title_Gallery'].'">
                                            <img class="img-1x1" src="'.((isset($item['ON_Img_Gallery']))?$item['ON_Img_Gallery']:'#').'" alt="'.$item['ON_Title_Gallery'].'">
                                        </a>
                                    </div>';
                                $gallery_count++;
                            }
                        ?>
                        
                    </div>
                    <div class="row p-0 m-0">
                        <div class="viewmoreBt text-center mx-auto" onclick="showmore($(this));">view more</div>
                        <div id="mb-150"></div>
                    </div>
                </div>
            </div>

        </div>
        
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-3.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script>
        jQuery(function($){
            $('.header-loop').owlCarousel({
                animateOut: 'fadeOut',
                animateIn: 'fadeIn',
                autoHeight:true,
                items:1,
                loop:true,
                // margin:30,
                autoplay:true,
                dots: false,
            });
        });
    </script>

    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
    
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/ononnature.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>