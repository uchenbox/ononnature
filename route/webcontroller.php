<?php 

ini_set('display_errors','on');
define('__pageroot', dirname(__FILE__).'/');
include('core/webDataManager.php');
session_start();

//$action[0] >> Name of Manager.php
//$action[1] >> Method in Manager.php
//$action = explode(':', $_GET['action']);
class webRoute
{
    var $action;
    //建構子，可設參數但無回傳值(多數用於連線)
    function __construct($action, $params = null){
        $this->action = $action;
        $res['errmsg'] = '資料讀取有誤!';
    }

    function Action_Event(){
        $action = $this->action;
        switch ($action) {
            case 'loadWebOption':
                $result = webDataManager::query_Event('getweboption', array());
            break;
            case "getHomeHeader":
                $result = webDataManager::query_Event('gethomeheader', array());
            break;
            case 'getHomeLink':
                $result = webDataManager::query_Event('gethomelink', array());
            break;
            case 'getHomeDesp':
                $result = webDataManager::query_Event('gethomedesp', array());
            break;
            case "getWalkHeader":
                $result = webDataManager::query_Event('getwalkheader', array());
            break;
            case "getWalkDesp":
                $result = webDataManager::query_Event('getwalkdesp', array());
            break;
            case "getWalkVideo":
                $result = webDataManager::query_Event('getwalkvideo', array());
            break;
            case "getWalkWp001":
                $result = webDataManager::query_Event('getwalkwp001', array());
            break;
            case "getDesignLiveTheme":
                $result = webDataManager::query_Event('getdesignlivetheme', array());
            break;
            case "getDesignLivImg":
                $result = webDataManager::query_Event('getdesignlivimg', array());
            break;
            // case "getWalkWp005":
            //     $result = webDataManager::query_Event('getwalkwp005', array());
            // break;
            // case "getWalkWp006":
            //     $result = webDataManager::query_Event('getwalkwp006', array());
            // break;
            // case "getWalkWp007":
            //     $result = webDataManager::query_Event('getwalkwp007', array());
            // break;
            // case "getWalkWp008":
            //     $result = webDataManager::query_Event('getwalkwp008', array());
            // break;
            // case "getWalkWp009":
            //     $result = webDataManager::query_Event('getwalkwp009', array());
            // break;
            // case "getWalkWp010":
            //     $result = webDataManager::query_Event('getwalkwp010', array());
            // break;
            case "getWalkWp011":
                $result = webDataManager::query_Event('getwalkwp011', array());
            break;
            case "getWalkWp004":
                $result = webDataManager::query_Event('getwalkwp004', array());
            break;
            case "getStructTheme":
                $result = webDataManager::query_Event('getstructtheme', array());
            break;
            case "getStructImg":
                $result = webDataManager::query_Event('getstructimg', array());
            break;
            // case "getWalkWp012":
            //     $result = webDataManager::query_Event('getwalkwp012', array());
            // break;
            // case "getWalkWp013":
            //     $result = webDataManager::query_Event('getwalkwp013', array());
            // break;
            // case "getWalkWp014":
            //     $result = webDataManager::query_Event('getwalkwp014', array());
            // break;
            case "getWalkGallery":
                $result = webDataManager::query_Event('getwalkgallery', array());
            break;
            case "getWalkReserve":
                $result = webDataManager::query_Event('getwalkereserve', array());
            break;
            case "getWalkLink":
                $result = webDataManager::query_Event('getwalklink', array());
            break;
            case "getFaqWalk":
                $result = webDataManager::query_Event('getfaqwalk', array());
            break;
            case "getVisitHeader":
                $result = webDataManager::query_Event('getvisitheader', array());
            break;
            case "getVisitDesp":
                $result = webDataManager::query_Event('getvisitdesp', array());
            break;
            case "getVisitPdesp":
                $result = webDataManager::query_Event('getvisitpdesp', array());
            break;
            case "getVisitTheme":
                $result = webDataManager::query_Event('getvisittheme', array());
            break;
            case "addVisitReserve":
                $result = webDataManager::query_Event('addvisitreserve', $_POST);
            break;
            case "getStyleHeader":
                $result = webDataManager::query_Event('getstyleheader', array());
            break;
            case "getStyleCover":
                $result = webDataManager::query_Event('getstylecover', array());
            break;
            case "getStylePdesp":
                $result = webDataManager::query_Event('getstylepdesp', array());
            break;
            case "getStylePimg":
                $result = webDataManager::query_Event('getstylepimg', array());
            break;
            case "getActImg":
                $result = webDataManager::query_Event('getactimg', array());
            break;
            case "getOnHeader":
                $result = webDataManager::query_Event('getonheader', array());
            break;
            case "getOnHdesp":
                $result = webDataManager::query_Event('getonhdesp', array());
            break;
            case "getOnIntro":
                $result = webDataManager::query_Event('getonintro', array());
            break;
            case "getOnDesp":
                $result = webDataManager::query_Event('getondesp', array());
            break;
            case "getOnPdesp":
                $result = webDataManager::query_Event('getonpdesp', array());
            break;
            case "getOnGallery":
                $result = webDataManager::query_Event('getongallery', array());
            break;
            case "getOnLink":
                $result = webDataManager::query_Event('getonlink', array());
            break;
            case "getFaqOn":
                $result = webDataManager::query_Event('getfaqon', array());
            break;
            case "getMealHeader":
                $result = webDataManager::query_Event('getmealheader', array());
            break;
            case "getMealTheme":
                $result = webDataManager::query_Event('getmealtheme', array());
            break;
            case "getMealDesp":
                $result = webDataManager::query_Event('getmealdesp', array());
            break;
            case "getMealPdesp":
                $result = webDataManager::query_Event('getmealpdesp', array());
            break;
            case "getMealPimg":
                $result = webDataManager::query_Event('getmealpimg', array());
            break;
            case "getTrafficHeader":
                $result = webDataManager::query_Event('gettrafficheader', array());
            break;
            case "getTrafficWay":
                $result = webDataManager::query_Event('gettrafficway', array());
            break;
            case "getTrafficGallery":
                $result = webDataManager::query_Event('gettrafficgallery', array());
            break;
            case "getHistoryHeader":
                $result = webDataManager::query_Event('gethistoryheader', array());
            break;
            case "getHistoryDesp":
                $result = webDataManager::query_Event('gethistorydesp', array());
            break;
            case "getHistoryPdesp":
                $result = webDataManager::query_Event('gethistorypdesp', array());
            break;
            case "getHistoryBrand":
                $result = webDataManager::query_Event('gethistorybrand', array());
            break;
            case "getBlogHeader":
                $result = webDataManager::query_Event('getblogheader', array());
            break;
            case "getBlogDairyAll":
                $result = webDataManager::query_Event('getblogdairyall', $_GET);                
            break;
            case "getBlogThemeLv1":
                $result = webDataManager::query_Event('getblogthemelv1', array());
            break;
            case "getBlogTheme":
                $result = webDataManager::query_Event('getblogtheme', array());
            break;
            case "getBlogDairyRecent":
                $result = webDataManager::query_Event('getblogdairyrecent', array());
            break;
            case "getBlogLabels":
                $result = webDataManager::query_Event('getbloglabels', array());
            break;
            case "getBlogDairyContent":
                $result = webDataManager::query_Event('getblogdairycontent', $_GET);
            break;
            case "getBlogDairyContentPrev":
                $result = webDataManager::query_Event('getblogdairycontentprev', $_GET);
            break;
            case "getBlogDairyContentNext":
                $result = webDataManager::query_Event('getblogdairycontentnext', $_GET);
            break;
            case 'getBlogDairyLimit3':
                $result = webDataManager::query_Event('getblogdairylimit3', array());
            break;
            case 'getBlogDairy':
                $result = webDataManager::query_Event('getblogdairy', array());
            break;
            case 'getFaqHeader':
                $result = webDataManager::query_Event('getfaqheader', array());
            break;
            case 'getFaqTheme':
                $result = webDataManager::query_Event('getfaqtheme', array());
            break;
            case 'getFaq':
                $result = webDataManager::query_Event('getfaq', array());
            break;
            case 'loadFooterOnon':
                $result = webDataManager::query_Event('loadfooteronon', array());
            break;
            case 'loadFooterFamwood':
                $result = webDataManager::query_Event('loadfooterfamwood', array());
            break;
            case 'loadPrompt':
                $result = webDataManager::query_Event('loadprompt', array());
            break;
            case 'loadNav':
                $result = webDataManager::query_Event('loadnav', array());
            break;
            default:
            break;
        }

        // if (is_bool($result)) {
        //     $res['data'] = false;
        // } else {
        //     $res['data'] = $result;
        // }
        
        return json_encode($result);
    }
}


?>
