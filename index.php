<?php 
// if(is_null($_GET['action']))
// {
//     $_GET['action'] = "";
// }
include_once('route/webcontroller.php');
if (!isset($_GET['action'])) {
    header("Location:./Home");
}
else
{
    $control = explode ( "/", $_GET ['action'] );
    $month_en = array('Jan', 'Fed', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    switch ($control[0]) {  
        case "Home": 
            $title = "ononNature";   
            $web = new webRoute('getHomeHeader'); 
            $home_header = json_decode($web->Action_Event(), true);
            $data['home_header'] = $home_header[0];

            $web = new webRoute('getHomeLink');
            $home_link = json_decode($web->Action_Event(), true);
            $data['home_link'] = $home_link;

            $web = new webRoute('getHomeDesp');
            $home_desp = json_decode($web->Action_Event(), true);
            $data['home_desp'] = $home_desp[0];
            
            $web = new webRoute('getBlogDairy');
            $blog_dairy = json_decode($web->Action_Event(), true);
            $data['blog_dairy'] = $blog_dairy;
            include("view/index.php");
        break;
        case "Test": 
            $title = "ononNature";           
            include("view/index-ori.php");
        break;
        case "Walkerhouse": 
            $title = "ononNature行動木屋";  
            $alt = 'famwood 行動木屋';       
            $web = new webRoute('getWalkHeader');
            $walk_header = json_decode($web->Action_Event(), true);
            $data['walk_header'] = $walk_header;
            
            $web = new webRoute('getWalkDesp');
            $walk_desp = json_decode($web->Action_Event(), true);
            $data['walk_desp'] = $walk_desp;
            // 行動紅屋 - 影片
            $web = new webRoute('getWalkVideo');
            $walk_video = json_decode($web->Action_Event(), true);
            $data['walk_video'] = $walk_video;
            // 行動紅屋 - 風景材
            $web = new webRoute('getWalkWp001');
            $walk_wp001 = json_decode($web->Action_Event(), true);
            $data['walk_wp001'] = $walk_wp001;

            // 行動紅屋 - 設計生活類別
            $web = new webRoute('getDesignLiveTheme');
            $walk_dltheme = json_decode($web->Action_Event(), true);
            $data['walk_dltheme'] = $walk_dltheme;
            // 行動紅屋 - 設計生活圖片
            $web = new webRoute('getDesignLivImg');
            $walk_dlimg = json_decode($web->Action_Event(), true);
            $data['walk_dlimg'] = $walk_dlimg;
            // // 行動紅屋 - 共煮共食
            // $web = new webRoute('getWalkWp005');
            // $walk_wp005 = json_decode($web->Action_Event(), true);
            // $data['walk_wp005'] = $walk_wp005;
            // // 行動紅屋 - 與眾樂樂
            // $web = new webRoute('getWalkWp006');
            // $walk_wp006 = json_decode($web->Action_Event(), true);
            // $data['walk_wp006'] = $walk_wp006;
            // // 行動紅屋 - 點墨書香
            // $web = new webRoute('getWalkWp007');
            // $walk_wp007 = json_decode($web->Action_Event(), true);
            // $data['walk_wp007'] = $walk_wp007;
            // // 行動紅屋 - 靈性禪修
            // $web = new webRoute('getWalkWp008');
            // $walk_wp008 = json_decode($web->Action_Event(), true);
            // $data['walk_wp008'] = $walk_wp008;
            // // 行動紅屋 - 沐浴賞景
            // $web = new webRoute('getWalkWp009');
            // $walk_wp009 = json_decode($web->Action_Event(), true);
            // $data['walk_wp009'] = $walk_wp009;
            // 行動紅屋 - 自然共好
            // $web = new webRoute('getWalkWp010');
            // $walk_wp010 = json_decode($web->Action_Event(), true);
            // $data['walk_wp010'] = $walk_wp010;
            // 行動紅屋 - 耐震防颱
            $web = new webRoute('getWalkWp011');
            $walk_wp011 = json_decode($web->Action_Event(), true);
            $data['walk_wp011'] = $walk_wp011;
            // 行動紅屋 - 熱帶建築
            $web = new webRoute('getWalkWp004');
            $walk_wp004 = json_decode($web->Action_Event(), true);
            $data['walk_wp004'] = $walk_wp004;

            // 行動紅屋 - 結構類別
            $web = new webRoute('getStructTheme');
            $walk_strutheme = json_decode($web->Action_Event(), true);
            $data['walk_strutheme'] = $walk_strutheme;
            // 行動紅屋 - 結構圖片
            $web = new webRoute('getStructImg');
            $walk_struimg = json_decode($web->Action_Event(), true);
            $data['walk_struimg'] = $walk_struimg;
            // // 行動紅屋 - 地
            // $web = new webRoute('getWalkWp012');
            // $walk_wp012 = json_decode($web->Action_Event(), true);
            // $data['walk_wp012'] = $walk_wp012;
            // // 行動紅屋 - 壁
            // $web = new webRoute('getWalkWp013');
            // $walk_wp013 = json_decode($web->Action_Event(), true);
            // $data['walk_wp013'] = $walk_wp013;
            // // 行動紅屋 - 天
            // $web = new webRoute('getWalkWp014');
            // $walk_wp014 = json_decode($web->Action_Event(), true);
            // $data['walk_wp014'] = $walk_wp014;
            // 行動紅屋 - Gallery
            $web = new webRoute('getWalkGallery');
            $walk_gallery = json_decode($web->Action_Event(), true);
            $data['walk_gallery'] = $walk_gallery;
            // 行動紅屋 - 預約參訪背景影片
            $web = new webRoute('getWalkReserve');
            $walk_reserve = json_decode($web->Action_Event(), true);
            $data['walk_reserve'] = $walk_reserve;
            // 行動紅屋 - 快速連結
            $web = new webRoute('getWalkLink');
            $walk_link = json_decode($web->Action_Event(), true);
            $data['walk_link'] = $walk_link;
            // 行動紅屋 - faq
            $web = new webRoute('getFaqWalk');
            $walk_faq = json_decode($web->Action_Event(), true);
            $data['walk_faq'] = $walk_faq;

            $web = new webRoute('getBlogDairy');
            $blog_dairy = json_decode($web->Action_Event(), true);
            $data['blog_dairy'] = $blog_dairy;

            include("view/woodhouse.php");
        break;
        case "Housevisit": 
            $title = "行動木屋參訪";       
            
            $web = new webRoute('getVisitHeader');
            $visit_header = json_decode($web->Action_Event(), true);
            $data['visit_header'] = $visit_header;

            $web = new webRoute('getVisitDesp');
            $visit_desp = json_decode($web->Action_Event(), true);
            $data['visit_desp'] = $visit_desp;

            $web = new webRoute('getVisitPdesp');
            $visit_pdesp = json_decode($web->Action_Event(), true);
            $data['visit_pdesp'] = $visit_pdesp;

            $web = new webRoute('getVisitTheme');
            $visit_theme = json_decode($web->Action_Event(), true);
            $data['visit_theme'] = $visit_theme;

            include("view/visit.php");
        break;
        case "Housestyle": 
            $title = "木屋款式";      
            
            $web = new webRoute('getStyleHeader');
            $style_header = json_decode($web->Action_Event(), true);
            $data['style_header'] = $style_header;

            $web = new webRoute('getStyleCover');
            $style_cover = json_decode($web->Action_Event(), true);
            $data['style_cover'] = $style_cover;

            $web = new webRoute('getStylePdesp');
            $style_pdesp = json_decode($web->Action_Event(), true);
            $data['style_pdesp'] = $style_pdesp;

            $web = new webRoute('getStylePimg');
            $style_pimg = json_decode($web->Action_Event(), true);
            $data['style_pimg'] = $style_pimg;
            
            include("view/housestyle.php");
        break;
        case "News": 
            $title = "檔期推廣";        
            
            $web = new webRoute('getActImg');
            $act_img = json_decode($web->Action_Event(), true);
            $data['act_img'] = $act_img;

            include("view/news.php");
        break;
        case "OnonNature": 
            $title = "OnonNature";     
            $alt = 'OnonNature';  
            
            $web = new webRoute('getOnHeader');
            $on_header = json_decode($web->Action_Event(), true);
            $data['on_header'] = $on_header;
            
            $web = new webRoute('getOnHdesp');
            $on_hdesp = json_decode($web->Action_Event(), true);
            $data['on_hdesp'] = $on_hdesp;

            $web = new webRoute('getOnIntro');
            $on_intro = json_decode($web->Action_Event(), true);
            $data['on_intro'] = $on_intro;

            $web = new webRoute('getOnDesp');
            $on_desp = json_decode($web->Action_Event(), true);
            $data['on_desp'] = $on_desp;

            $web = new webRoute('getOnPdesp');
            $on_pdesp = json_decode($web->Action_Event(), true);
            $data['on_pdesp'] = $on_pdesp;

            $web = new webRoute('getOnGallery');
            $on_gallery = json_decode($web->Action_Event(), true);
            $data['on_gallery'] = $on_gallery;

            $web = new webRoute('getOnLink');
            $on_link = json_decode($web->Action_Event(), true);
            $data['on_link'] = $on_link;

            $web = new webRoute('getFaqOn');
            $faq_on = json_decode($web->Action_Event(), true);
            $data['faq_on'] = $faq_on;

            include("view/ononnature.php");
        break;
        case "Meal": 
            $title = "餐點介紹";     
            
            $web = new webRoute('getMealHeader');
            $meal_header = json_decode($web->Action_Event(), true);
            $data['meal_header'] = $meal_header;

            $web = new webRoute('getMealTheme');
            $meal_theme = json_decode($web->Action_Event(), true);
            $data['meal_theme'] = $meal_theme;

            $web = new webRoute('getMealDesp');
            $meal_desp = json_decode($web->Action_Event(), true);
            $data['meal_desp'] = $meal_desp;

            $web = new webRoute('getMealPdesp');
            $meal_pdesp = json_decode($web->Action_Event(), true);
            $data['meal_pdesp'] = $meal_pdesp;

            $web = new webRoute('getMealPimg');
            $meal_pimg = json_decode($web->Action_Event(), true);
            $data['meal_pimg'] = $meal_pimg;

            include("view/meal.php");
        break;
        case "Traffic": 
            $title = "園區交通";   
            
            $web = new webRoute('getTrafficHeader');
            $traffic_header = json_decode($web->Action_Event(), true);
            $data['traffic_header'] = $traffic_header;

            $web = new webRoute('getTrafficWay');
            $traffic_way = json_decode($web->Action_Event(), true);
            $data['traffic_way'] = $traffic_way;

            $web = new webRoute('getTrafficGallery');
            $traffic_gallery = json_decode($web->Action_Event(), true);
            $data['traffic_gallery'] = $traffic_gallery;

            include("view/traffic.php");
        break;
        case "History": 
            $title = "紅屋沿革";        
            
            $web = new webRoute('getHistoryHeader');
            $history_header = json_decode($web->Action_Event(), true);
            $data['history_header'] = $history_header;

            $web = new webRoute('getHistoryDesp');
            $history_desp = json_decode($web->Action_Event(), true);
            $data['history_desp'] = $history_desp;

            $web = new webRoute('getHistoryPdesp');
            $history_pdesp = json_decode($web->Action_Event(), true);
            $data['history_pdesp'] = $history_pdesp;

            $web = new webRoute('getHistoryBrand');
            $history_brand = json_decode($web->Action_Event(), true);
            $data['history_brand'] = $history_brand;

            $web = new webRoute('getBlogDairy');
            $blog_dairy = json_decode($web->Action_Event(), true);
            $data['blog_dairy'] = $blog_dairy;

            include("view/history.php");
        break;
        case "Blog": 
            $title = "紅屋日誌";
            
            $web = new webRoute('getBlogHeader');
            $blog_heaader = json_decode($web->Action_Event(), true);
            $data['blog_heaader'] = $blog_heaader;

            $web = new webRoute('getBlogDairyAll');
            $blog_dairyall = json_decode($web->Action_Event(), true);
            $data['blog_dairyall'] = $blog_dairyall;

            $web = new webRoute('getBlogThemeLv1');
            $blog_themelv1 = json_decode($web->Action_Event(), true);
            $data['blog_themelv1'] = $blog_themelv1;

            $web = new webRoute('getBlogTheme');
            $blog_theme = json_decode($web->Action_Event(), true);
            $data['blog_theme'] = $blog_theme;

            $web = new webRoute('getBlogDairyRecent');
            $blog_dairyrecent = json_decode($web->Action_Event(), true);
            $data['blog_dairyrecent'] = $blog_dairyrecent;

            $web = new webRoute('getBlogLabels');
            $blog_labels = json_decode($web->Action_Event(), true);
            $data['blog_labels'] = $blog_labels;

            include("view/blog.php");
        break;
        case "Blogdetail": 
            $param = $_GET['title']; 

            $web = new webRoute('getBlogDairyContent');
            $blogdairy_content = json_decode($web->Action_Event(), true);
            $data['blogdairy_content'] = $blogdairy_content;

            $web = new webRoute('getBlogDairyContentPrev');
            $blogdairy_prev = json_decode($web->Action_Event(), true);
            $data['blogdairy_prev'] = $blogdairy_prev;

            $web = new webRoute('getBlogDairyContentNext');
            $blogdairy_next = json_decode($web->Action_Event(), true);
            $data['blogdairy_next'] = $blogdairy_next;

            $title = $data['blogdairy_content'][0]['Blog_Title_Dairy']; 
            $blog = $data['blogdairy_content'][0];
            
            $web = new webRoute('getBlogThemeLv1');
            $blog_themelv1 = json_decode($web->Action_Event(), true);
            $data['blog_themelv1'] = $blog_themelv1;

            $web = new webRoute('getBlogTheme');
            $blog_theme = json_decode($web->Action_Event(), true);
            $data['blog_theme'] = $blog_theme;

            $web = new webRoute('getBlogDairyRecent');
            $blog_dairyrecent = json_decode($web->Action_Event(), true);
            $data['blog_dairyrecent'] = $blog_dairyrecent;

            $web = new webRoute('getBlogLabels');
            $blog_labels = json_decode($web->Action_Event(), true);
            $data['blog_labels'] = $blog_labels;

            $web = new webRoute('getBlogDairyLimit3');
            $blog_dairy = json_decode($web->Action_Event(), true);
            $data['blog_dairy'] = $blog_dairy;

            include("view/blog-prd.php");
        break;
        case "Faq": 
            $title = "常見問題";    
            
            $web = new webRoute('getFaqHeader');
            $faq_header = json_decode($web->Action_Event(), true);
            $data['faq_header'] = $faq_header;

            $web = new webRoute('getFaqTheme');
            $faq_theme = json_decode($web->Action_Event(), true);
            $data['faq_theme'] = $faq_theme;

            $web = new webRoute('getFaq');
            $faq = json_decode($web->Action_Event(), true);
            $data['faq'] = $faq;

            include("view/faq.php");
        break;
        case 'api': 
            switch ($control[1]) {                
                case 'loadWebOption':
                    $data = new webRoute($control[1]);
                    echo $data->Action_Event();
                break;
                case 'addVisitReserve':
                    $data = new webRoute($control[1]);
                    echo $data->Action_Event();
                break;
                case 'loadFooterOnon':
                    $data = new webRoute($control[1]);
                    echo $data->Action_Event();
                break;
                case 'loadFooterFamwood':
                    $data = new webRoute($control[1]);
                    echo $data->Action_Event();
                break;
                case 'loadPrompt':
                    $data = new webRoute($control[1]);
                    echo $data->Action_Event();
                break;
                default:
                    # code...
                    break;
            }
        break;
        default:
            header("Location:./Home");
        break;  
    }
}

?>
