<!doctype html>
<html lang="zh-TW">
  <head>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="dist/style/bootstrap4/bootstrap.min.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/fontawesome.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/brands.css">
    <link rel="stylesheet" href="dist/style/fontawesome5/css/solid.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="dist/style/owl.carousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="dist/css/index.min.css<?php echo '?v='.date('Ymdhis'); ?>">
    <link rel="stylesheet" href="dist/vendor/lightbox2/css/lightbox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    
  </head>
  <body class="d-flex flex-column bg-transparent">
      <div id="wrap-header">
        <?php require('layout/Header-3.html') ?>
        <?php require('layout/Sidebar.html') ?>
      </div>
      <div id="wrap-body" class="container-fluid p-0 m-0"> 
            <!-- header -->
            <div id="intrp-style-wrap" class="d-flex flex-column p-0 m-0">
                <div class="intrp-style-content">
                    <h4 class="text-center">– BLOG –</h4>
                    <h2 class="text-center">紅屋日誌</h2>
                    <hr>
                </div>   
                <div class="d-flex flex-row meal-items-content">
                    <div class="traffic-items item-1">
                        <div class="img-wrap">
                            <img class="w-100" style="" src="<?php echo (!empty($data['blog_heaader'][0]['Blog_Img_Header']))?$data['blog_heaader'][0]['Blog_Img_Header']: '#'; ?>" alt="紅屋日誌">
                        </div>
                    </div>
                    <div class="traffic-items item-2 d-md-block">
                        <div class="bg-gray float-right"></div>
                    </div>     
                </div>     
            </div>
            
            <!-- 文章 -->
            <div class="blogs-content d-flex flex-row   px-0 ">
                <div class="px-0  blog-article flex-fill">
                    <?php
                    if($data['blog_dairyall']){
                        foreach ($data['blog_dairyall'] as $blog) {
                            $day = explode('-', substr( $blog['Blog_Sdate_Dairy'] , 0 , 10 ))[2];
                            $momth = explode('-', substr( $blog['Blog_Sdate_Dairy'] , 0 , 10 ))[1];
                            $year = explode('-', substr( $blog['Blog_Sdate_Dairy'] , 0 , 10 ))[0];
                            echo '<div class="item d-flex flex-column px-0 mx-0">
                                    <h5 class="mb-0">'.$day . ' ' . $month_en[($momth * 1) - 1] . ' ' .  $year.' | '.$blog['Blog_Name'].'</h5>
                                    <h3 class="mb-0">'.$blog['Blog_Title_Dairy'].'</h3>
                                    <div class="img-wrap overflow-hidden">
                                        <img class="w-100" src="'.((!empty($blog['Blog_Img_Dairy']))?$blog['Blog_Img_Dairy']: '#').'" alt="'.$blog['Blog_Title_Dairy'].'">
                                    </div>
                                    <p class="overflow-hidden" style="max-height:200px;">'.str_replace("</p>", "<br/>", str_replace("<p>", '', $blog['Blog_Content_Dairy'])).'</p>
                                    <a href="./Blogdetail?title='.$blog['Blog_ID_Dairy'].'" class="text-reset"><h4 class="text-right">Read More →</h4></a>
                                </div>';
                        }
                    }
                    else
                    {
                        echo '<p>請期待新文章上架喔！</p>';
                    }
                    ?>
                    <!-- <div class="item d-flex flex-column p-5 mb-5">
                        <h5>01 Jan 2020 | 理念論壇</h5>
                        <h3>如果有一種自由，從小小木造房子開始 生活的自由，從思考你的居住空間開始</h3>
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="assets/04_紅屋日誌/可用的圖/吊裝.jpg" alt="">
                        </div>
                        <p>「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自</p>
                        <a href="./Blogdetail?title=1" class="text-reset"><h4 class="text-right">Read More →</h4></a>
                    </div>
                    <div class="item d-flex flex-column p-5 mb-5">
                        <h5>01 Jan 2020 | 理念論壇</h5>
                        <h3>如果有一種自由，從小小木造房子開始 生活的自由，從思考你的居住空間開始</h3>
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="assets/04_紅屋日誌/可用的圖/吊裝.jpg" alt="">
                        </div>
                        <p>「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自</p>
                        <h4 class="text-right">Read More →</h4>
                    </div>
                    <div class="item d-flex flex-column p-5 mb-5">
                        <h5>01 Jan 2020 | 理念論壇</h5>
                        <h3>如果有一種自由，從小小木造房子開始 生活的自由，從思考你的居住空間開始</h3>
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="assets/04_紅屋日誌/可用的圖/吊裝.jpg" alt="">
                        </div>
                        <p>「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自</p>
                        <h4 class="text-right">Read More →</h4>
                    </div>
                    <div class="item d-flex flex-column p-5 mb-5">
                        <h5>01 Jan 2020 | 理念論壇</h5>
                        <h3>如果有一種自由，從小小木造房子開始 生活的自由，從思考你的居住空間開始</h3>
                        <div class="img-wrap overflow-hidden">
                            <img class="w-100" src="assets/04_紅屋日誌/可用的圖/吊裝.jpg" alt="">
                        </div>
                        <p>「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自小的教育環境讓我們對於「居住「所有的技術應該都是為了將世界和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自和自己連結起來而開始的。」——隈研吾《小建築》 50、60年代出生的人，莫不遵從社會「有土（房）斯有財」的主流價值，而在臺灣，自</p>
                        <h4 class="text-right">Read More →</h4>
                    </div> -->
                </div>
                <div class="px-0 mx-auto blog-link flex-fill px-0 response-hidden">
                    <div class="link-content">
                        <dl class="row border-bottom px-0 position-relative d-block">
                            <dt class="col-12">近期文章</dt>
                            <?php
                            foreach ($data['blog_dairyrecent'] as $recent) {
                                echo '<dd class="col-12 text-truncate " style="max-width: 250px;"><a class="text-decoration-none text-reset " href="./Blogdetail?title='.$recent['Blog_ID_Dairy'].'">'.$recent['Blog_Title_Dairy'].'</a></dd>';
                            }
                            ?>
                            <!-- <dd class="col-12">自由，從思考居住空間開始</dd>
                            <dd class="col-12">居住空間開始</dd>
                            <dd class="col-12">小木屋，來去鄉下</dd>
                            <dd class="col-12">住宅的居心地</dd> -->
                        </dl>
                        <dl class="row border-bottom px-0  position-relative d-block">
                            <dt class="col-12">文章分類</dt>
                            <?php
                            foreach ($data['blog_themelv1'] as $theme) {
                                echo ' <dd class="col-12">
                                        <a href="./Blog?theme='.$theme['Blog_ID'] .'" class="text-reset text-decoration-none">'.$theme['Blog_Name'] .'</a>';
                                        echo '<dl class="row sub my-0">';
                                        foreach ($data['blog_theme'] as $child) {
                                            if($theme['Blog_ID'] == $child['Blog_UpMID'])
                                            {
                                                echo '<dd class="col-12"><a href="./Blog?theme='.$child['Blog_ID'] .'" class="text-reset text-decoration-none">'.$child['child_name'].'</a></dd>';
                                                break;
                                            }
                                            else
                                            {
                                                echo '';
                                            }
                                        }
                                           
                                        echo '</dl>';
                                echo '</dd>';
                            }
                            echo '<dd class="col-12"><a href="./Blog?theme=null" class="text-reset text-decoration-none">未分類</a></dd>';
                            ?>
                            <!-- <dd class="col-12">
                                理念論壇
                                <dl class="row sub my-0">
                                    <dd class="col-12">小屋哲學</dd>
                                    <dd class="col-12">天然材料</dd>
                                    <dd class="col-12">住宅的居心地</dd>
                                </dl>
                            </dd>
                            <dd class="col-12">媒體報導</dd>
                            <dd class="col-12">公告快訊</dd>
                            <dd class="col-12">未分類</dd> -->
                        </dl>
                        <dl class="row position-relative px-0  d-block">
                            <dt class="col-12">標籤</dt>
                            <?php
                            $filter_lb = array();
                            $stick_str = '';
                            foreach ($data['blog_labels'] as $labels) {
                                $stick_str .= ($labels['Blog_Intro_Dairy'] . ',');
                            }
                            str_replace(' ', '', $stick_str);
                            $filter_lb = array_unique(explode(',', $stick_str));
                            echo '<div class="row px-3">';
                            foreach ($filter_lb as $lb) {
                                if(isset($lb) && !empty($lb))
                                {
                                    echo '<dd class="col-3"><a href="./Blog?label='.trim($lb) .'" class="text-reset text-decoration-none">'.$lb.'</a></dd>';
                                }
                                
                            }
                            echo '</div>';
                            ?>
                            <!-- <dd class="col-3">木構造</dd>
                            <dd class="col-3">組合屋</dd>
                            <dd class="col-3">小木屋</dd>
                            <dd class="col-3">自建</dd> -->
                        </dl>
                    </div>
                </div>
            </div>
            <div class="pagination-content d-flex flex-column  px-0 mx-auto my-5">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center " >
                        <li class="page-item <?php echo ((isset($_GET['start']) && $_GET['start'] != '0')?'':'disabled'); ?>">
                        <a class="page-link text-reset border-0" href="./Blog?<?php echo (isset($_GET['theme']))?'theme='.$_GET['theme'].'&':((isset($_GET['label']))?'label='.$_GET['label'].'&':''); ?>start=<?php echo isset($_GET['start'])?(((int)$_GET['start'] / 10) - 1)*10 : 0; ?>&end=<?php echo isset($_GET['end'])?(((int)$_GET['end'] / 10) - 1)*10:0; ?>" tabindex="-1" aria-disabled="true"><i class="fas fa-arrow-left"></i></a>
                        </li>
                        <?php
                        if(isset($data['blog_dairyall'][0]))
                        {
                         for ($i=0; $i < $data['blog_dairyall'][0]['total_page']; $i++) { 
                             echo '<li class="page-item '.((isset($_GET['start']))?(($_GET['start'] == ( $i * 10))?'active':''):(($i==0)?'active':'')).' "><a class="page-link text-reset border-0" href="./Blog?'.((isset($_GET['theme']))?'theme='.$_GET['theme'].'&':((isset($_GET['label']))?'label='.$_GET['label'].'&':'')).'start='.( $i * 10).'&end='.( ($i+1) * 10).'">'.($i + 1).'</a></li>';
                         }
                        }
                        else
                        {
                            echo '<li class="page-item"><a class="page-link text-reset border-0" href="./Blog?'.((isset($_GET['theme']))?'theme='.$_GET['theme'].'&':((isset($_GET['label']))?'label='.$_GET['label'].'&':'')).'start=0&end=10">1</a></li>';
                        }
                        ?>
                        <!-- <li class="page-item"><a class="page-link text-reset border-0" href="#">1</a></li>
                        <li class="page-item"><a class="page-link text-reset border-0" href="#">2</a></li>
                        <li class="page-item"><a class="page-link text-reset border-0" href="#">3</a></li> -->
                        <li class="page-item <?php echo ((isset($_GET['end']) && $_GET['end'] != ((int)$data['blog_dairyall'][0]['total_page']* 10))?'':((!isset($_GET['end'])?'':'disabled'))); ?> <?php echo (isset($data['blog_dairyall'][0]))?(($data['blog_dairyall'][0]['total_page'] > 1)?'':'disabled'):'disabled'; ?>">
                        <a class="page-link text-reset border-0" href="./Blog?<?php echo (isset($_GET['theme']))?'theme='.$_GET['theme'].'&':((isset($_GET['label']))?'label='.$_GET['label'].'&':''); ?>start=<?php echo (isset($_GET['start']))?(((int)$_GET['start'] / 10) + 1)*10 : 10; ?>&end=<?php echo (isset($_GET['end']))?(((int)$_GET['end'] / 10) + 1)*10 : 20; ?>"><i class="fas fa-arrow-right"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="blogs-link-content d-flex flex-column  px-0 response-show">                
                <div class="px-0 mx-auto blog-link flex-fill px-0">
                    <div class="link-content-2">
                        <dl class="row border-bottom px-0 position-relative d-block">
                            <dt class="col-12">近期文章</dt>
                            <?php
                            foreach ($data['blog_dairyrecent'] as $recent) {
                                echo '<dd class="col-12 text-truncate " style="max-width: 250px;"><a class="text-decoration-none text-reset " href="./Blogdetail?title='.$recent['Blog_ID_Dairy'].'">'.$recent['Blog_Title_Dairy'].'</a></dd>';
                            }
                            ?>
                            <!-- <dd class="col-12">自由，從思考居住空間開始</dd>
                            <dd class="col-12">居住空間開始</dd>
                            <dd class="col-12">小木屋，來去鄉下</dd>
                            <dd class="col-12">住宅的居心地</dd> -->
                        </dl>
                        <dl class="row border-bottom px-0  position-relative d-block">
                            <dt class="col-12">文章分類</dt>
                            <?php
                            foreach ($data['blog_themelv1'] as $theme) {
                                echo ' <dd class="col-12">
                                        <a href="./Blog?theme='.$theme['Blog_ID'] .'" class="text-reset text-decoration-none">'.$theme['Blog_Name'] .'</a>';
                                        echo '<dl class="row sub my-0">';
                                        foreach ($data['blog_theme'] as $child) {
                                            if($theme['Blog_ID'] == $child['Blog_UpMID'])
                                            {
                                                echo '<dd class="col-12"><a href="./Blog?theme='.$child['Blog_ID'] .'" class="text-reset text-decoration-none">'.$child['child_name'].'</a></dd>';
                                                break;
                                            }
                                            else
                                            {
                                                echo '';
                                            }
                                        }
                                           
                                        echo '</dl>';
                                echo '</dd>';
                            }
                            echo '<dd class="col-12"><a href="./Blog?theme=null" class="text-reset text-decoration-none">未分類</a></dd>';
                            ?>
                            <!-- <dd class="col-12">
                                理念論壇
                                <dl class="row sub my-0">
                                    <dd class="col-12">小屋哲學</dd>
                                    <dd class="col-12">天然材料</dd>
                                    <dd class="col-12">住宅的居心地</dd>
                                </dl>
                            </dd>
                            <dd class="col-12">媒體報導</dd>
                            <dd class="col-12">公告快訊</dd>
                            <dd class="col-12">未分類</dd> -->
                        </dl>
                        <dl class="row position-relative px-0  d-block">
                            <dt class="col-12">標籤</dt>
                            <?php
                            $filter_lb = array();
                            $stick_str = '';
                            foreach ($data['blog_labels'] as $labels) {
                                $stick_str .= ($labels['Blog_Intro_Dairy'] . ',');
                            }
                            str_replace(' ', '', $stick_str);
                            $filter_lb = array_unique(explode(',', $stick_str));
                            echo '<div class="item-labels px-3">';
                            foreach ($filter_lb as $lb) {
                                if(isset($lb) && !empty($lb))
                                {
                                    echo '<dd class="col-3 pl-0"><a href="./Blog?label='.trim($lb) .'" class="text-reset text-decoration-none">'.$lb.'</a></dd>';
                                }
                                
                            }
                            echo '</div>';
                            ?>
                            <!-- <dd class="col-3">木構造</dd>
                            <dd class="col-3">組合屋</dd>
                            <dd class="col-3">小木屋</dd>
                            <dd class="col-3">自建</dd> -->
                        </dl>
                    </div>
                </div>
            </div>
      </div>
      <div id="wrap-footer" class="p-0 m-0 w-100">
        <?php require('layout/Footer-2.html') ?>
      </div>

    <!-- Optional JavaScript -->    
    <script src="dist/script/vendor/popper.min.js"></script>
    <script src="dist/script/vendor/jquery-3.5.1.min.js"></script>
    <!-- <script src="dist/script/vendor/jquery-3.3.1.slim.min.js"></script> -->
    <script src="dist/script/vendor/bootstrap.min.js"></script>
    <script src="dist/script/vendor/owl.carousel.min.js"></script>
    <script src="dist/script/main.js"></script>
    <!-- <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> -->
      <!-- <script src="dist/script/init.js"></script> -->
        <script src="dist/script/vendor/jquery-1.11.3.min.js"></script> 
    <script src="dist/vendor/lightbox2/js/lightbox.min.js"></script>
        <script>
            var scripts = [
                'dist/script/init.js',
                'dist/script/blog.js',
                ];
            
                for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.onerror = function() {
                    console.log('Could not load ' + this.src);
                };
            
                script.src = scripts[i] + '?v=' + Date.now();
                document.body.appendChild(script);
                }
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  </body>
</html>