$(function(){
    $(".nav-item").each(function(){
        var url = location.href;
        if(url.indexOf('?')!=-1)
        { 
            var ary = url.split('?')[1].split('&');

            for(i=0;i<=ary.length-1;i++)
            {
                if(ary[i].split('=')[0] == 'act')
                {
                    var loc = ary[i].split('=')[1].split('#')[0];
                    if($(this).attr('data-loc') == loc)
                    {
                        $(this).find('a').addClass('active');
                    }                      
                }         
            }
        }
        
    })
});