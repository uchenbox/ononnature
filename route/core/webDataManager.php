<?php 
class webDataManager {
    
    public static function query_Event($method, $args){   
        include("mod_db.php");
        date_default_timezone_set('Asia/Taipei');
        $ini_result = self::iniread(__pageroot . "core/query/web.ini");  

        switch ($method) {  
            case 'getweboption': 
                $sql_inquery =  $ini_result['data']['getWebOption']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gethomeheader': 
                $sql_inquery = $ini_result['data']['getHomeHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "gethomelink":
                $sql_inquery = $ini_result['data']['getHomeLink']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "gethomedesp":
                $sql_inquery = $ini_result['data']['getHomeDesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalkheader': 
                $sql_inquery = $ini_result['data']['getWalkHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalkdesp': 
                $sql_inquery = $ini_result['data']['getWalkDesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalkvideo': 
                $sql_inquery = $ini_result['data']['getWalkVideo']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalkwp001': 
                $sql_inquery = $ini_result['data']['getWalkWp001']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getdesignlivetheme': 
                $sql_inquery = $ini_result['data']['getDesignLiveTheme']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getdesignlivimg': 
                $sql_inquery = $ini_result['data']['getDesignLivImg']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            // case 'getwalkwp005': 
            //     $sql_inquery = $ini_result['data']['getWalkWp005']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp006': 
            //     $sql_inquery = $ini_result['data']['getWalkWp006']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp007': 
            //     $sql_inquery = $ini_result['data']['getWalkWp007']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp008': 
            //     $sql_inquery = $ini_result['data']['getWalkWp008']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp009': 
            //     $sql_inquery = $ini_result['data']['getWalkWp009']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp010': 
            //     $sql_inquery = $ini_result['data']['getWalkWp010']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            case 'getwalkwp011': 
                $sql_inquery = $ini_result['data']['getWalkWp011']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalkwp004': 
                $sql_inquery = $ini_result['data']['getWalkWp004']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getstructtheme': 
                $sql_inquery = $ini_result['data']['getStructTheme']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getstructimg': 
                $sql_inquery = $ini_result['data']['getStructImg']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            // case 'getwalkwp012': 
            //     $sql_inquery = $ini_result['data']['getWalkWp012']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp013': 
            //     $sql_inquery = $ini_result['data']['getWalkWp013']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            // case 'getwalkwp014': 
            //     $sql_inquery = $ini_result['data']['getWalkWp014']['sql'];
                
            //     $state = $conn->prepare($sql_inquery);
            //     $state->execute();
                
            //     if ($state->rowCount() < 1) {
            //         return false;         
            //     }
            //     else 
            //     {
            //         return $state->fetchAll();
            //     }
            // break;
            case 'getwalkgallery': 
                $sql_inquery = $ini_result['data']['getWalkGallery']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalkereserve': 
                $sql_inquery = $ini_result['data']['getWalkReserve']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getwalklink': 
                $sql_inquery = $ini_result['data']['getWalkLink']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getfaqwalk': 
                $sql_inquery = $ini_result['data']['getFaqWalk']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getvisitheader': 
                $sql_inquery = $ini_result['data']['getVisitHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getvisitdesp': 
                $sql_inquery = $ini_result['data']['getVisitDesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getvisitpdesp': 
                $sql_inquery = $ini_result['data']['getVisitPdesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getvisittheme': 
                $sql_inquery = $ini_result['data']['getVisitTheme']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'addvisitreserve': 
                $sql_inquery = $ini_result['data']['addVisitReserve']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,$args['theme'],PDO::PARAM_STR);
                $state->bindValue(2,$args['name'],PDO::PARAM_STR);
                $state->bindValue(3,$args['tel'],PDO::PARAM_STR);
                $state->bindValue(4,$args['email'],PDO::PARAM_STR);
                $state->bindValue(5,$args['audlt'],PDO::PARAM_INT);
                $state->bindValue(6,$args['child'],PDO::PARAM_INT);
                $state->bindValue(7,$args['comment'],PDO::PARAM_STR);
                $state->bindValue(8,date("Y-m-d H:i:s"),PDO::PARAM_STR);
                $state->execute();

                if ($state->rowCount() < 1) {
                    return false;            
                }
                else 
                {
                    // return $conn->lastInsertId();
                    return true;
                }
            break;
            case 'getstyleheader': 
                $sql_inquery = $ini_result['data']['getStyleHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getstylepdesp': 
                $sql_inquery = $ini_result['data']['getStylePdesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getstylecover': 
                $sql_inquery = $ini_result['data']['getStyleCover']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getstylepimg': 
                $sql_inquery = $ini_result['data']['getStylePimg']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getactimg': 
                $sql_inquery = $ini_result['data']['getActImg']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getonheader': 
                $sql_inquery = $ini_result['data']['getOnHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getonhdesp': 
                $sql_inquery = $ini_result['data']['getOnHdesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getonintro': 
                $sql_inquery = $ini_result['data']['getOnIntro']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getondesp': 
                $sql_inquery = $ini_result['data']['getOnDesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getonpdesp': 
                $sql_inquery = $ini_result['data']['getOnPdesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getongallery': 
                $sql_inquery = $ini_result['data']['getOnGallery']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getonlink': 
                $sql_inquery = $ini_result['data']['getOnLink']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getfaqon': 
                $sql_inquery = $ini_result['data']['getFaqOn']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getmealheader': 
                $sql_inquery = $ini_result['data']['getMealHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getmealtheme': 
                $sql_inquery = $ini_result['data']['getMealTheme']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getmealdesp': 
                $sql_inquery = $ini_result['data']['getMealDesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getmealpdesp': 
                $sql_inquery = $ini_result['data']['getMealPdesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getmealpimg': 
                $sql_inquery = $ini_result['data']['getMealPimg']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gettrafficheader': 
                $sql_inquery = $ini_result['data']['getTrafficHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gettrafficway': 
                $sql_inquery = $ini_result['data']['getTrafficWay']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gettrafficgallery': 
                $sql_inquery = $ini_result['data']['getTrafficGallery']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gethistoryheader': 
                $sql_inquery = $ini_result['data']['getHistoryHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gethistorydesp': 
                $sql_inquery = $ini_result['data']['getHistoryDesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gethistorypdesp': 
                $sql_inquery = $ini_result['data']['getHistoryPdesp']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'gethistorybrand': 
                $sql_inquery = $ini_result['data']['getHistoryBrand']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogheader': 
                $sql_inquery = $ini_result['data']['getBlogHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogdairyall': 
                if(isset($args['theme']) && !empty($args['theme']))
                {
                    $sql_inquery = $ini_result['data']['getBlogDairyFilterTheme']['sql'];

                    $start  = ((isset($args['start']))?(int)$args['start']:0);
                    $end  = ((isset($args['end']))?(int)$args['end']:10);

                    $state = $conn->prepare($sql_inquery);
                    $state->bindValue(1,(($args['theme'] == 'null')?'':$args['theme']),PDO::PARAM_STR);
                    $state->bindValue(2,(($args['theme'] == 'null')?'':$args['theme']),PDO::PARAM_STR);
                    $state->bindValue(3,$start,PDO::PARAM_INT);
                    $state->bindValue(4,$end,PDO::PARAM_INT);
                }
                elseif (isset($args['label']) && !empty($args['label'])) {
                    $sql_inquery = $ini_result['data']['getBlogDairyFilterLabel']['sql'];

                    $start  = ((isset($args['start']))?(int)$args['start']:0);
                    $end  = ((isset($args['end']))?(int)$args['end']:10);

                    $state = $conn->prepare($sql_inquery);
                    $state->bindValue(1,'%'.$args['label'].'%',PDO::PARAM_STR);
                    $state->bindValue(2,'%'.$args['label'].'%',PDO::PARAM_STR);
                    $state->bindValue(3,$start,PDO::PARAM_INT);
                    $state->bindValue(4,$end,PDO::PARAM_INT);
                }
                else
                {
                    $sql_inquery = $ini_result['data']['getBlogDairyAll']['sql'];

                    $start  = ((isset($args['start']))?(int)$args['start']:0);
                    $end  = ((isset($args['end']))?(int)$args['end']:10);

                    $state = $conn->prepare($sql_inquery);
                    $state->bindValue(1,$start,PDO::PARAM_INT);
                    $state->bindValue(2,$end,PDO::PARAM_INT);
                }
                
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogthemelv1': 
                $sql_inquery = $ini_result['data']['getBlogThemeLv1']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogtheme': 
                $sql_inquery = $ini_result['data']['getBlogTheme']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogdairyrecent': 
                $sql_inquery = $ini_result['data']['getBlogDairyRecent']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getbloglabels': 
                $sql_inquery = $ini_result['data']['getBlogLabels']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogdairycontent': 
                $sql_inquery = $ini_result['data']['getBlogDairyContent']['sql'];

                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,(int)$args['title'],PDO::PARAM_INT);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogdairycontentprev': 
                $sql_inquery = $ini_result['data']['getBlogDairyContentPrev']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,(int)$args['title'],PDO::PARAM_INT);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case 'getblogdairycontentnext': 
                $sql_inquery = $ini_result['data']['getBlogDairyContentNext']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->bindValue(1,(int)$args['title'],PDO::PARAM_INT);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;

            case "getblogdairylimit3":
                $sql_inquery = $ini_result['data']['getBlogDairyLimit3']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "getblogdairy":
                $sql_inquery = $ini_result['data']['getBlogDairy']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "getfaqheader":
                $sql_inquery = $ini_result['data']['getFaqHeader']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "getfaqtheme":
                $sql_inquery = $ini_result['data']['getFaqTheme']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "getfaq":
                $sql_inquery = $ini_result['data']['getFaq']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "loadfooterfamwood":
                $sql_inquery = $ini_result['data']['getFooterFamwood']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "loadfooteronon":
                $sql_inquery = $ini_result['data']['getFooterOnon']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "loadprompt":
                $sql_inquery = $ini_result['data']['getPrompt']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            case "loadnav":
                $sql_inquery = $ini_result['data']['getNav']['sql'];
                
                $state = $conn->prepare($sql_inquery);
                $state->execute();
                
                if ($state->rowCount() < 1) {
                    return false;         
                }
                else 
                {
                    return $state->fetchAll();
                }
            break;
            default:
                # code...
            break;
        }
    }
     


    // base
    private static function iniread($filename) 
    {
        if ( file_exists($filename) )
        {
           $result = parse_ini_file($filename, true);
        }
        else 
        {
           $result = $filename . " not found.";  //檔案不存在
        }
        
        $result = array("data" => $result );
        return $result;
    }

}
?>
