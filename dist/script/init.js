var $jq = jQuery.noConflict(true);
$jq(function(){
    // $jq(window).scroll(function () {
    //     if ($jq(this).scrollTop() > 120) {
    //         let styles = {"background-color":"rgba(2,128,144,0.8)"};
    //         $jq('#wrap-header').css(styles);
    //     } else {
    //         let styles = {"background-color":"rgba(2,128,144,1)"};
    //         $jq('#wrap-header').css(styles);
    //     }

    //     if ($jq(this).scrollTop() > 300) {
    //         $jq('#gotop').show("fast");
    //     } else {
    //         $jq('#gotop').stop().hide("fast");
    //     }
    // });
    loadprompt();
    loadCoInfo();
    loadfamwood();
    loadononnature();
    // 初始載入
    var $nav = $(".nav-lists");
    $nav.toggleClass('bg-white', $(this).scrollTop() > ($nav.height() + 50));

    var $nav_scroll_over_header = $(".nav-lists.scroll-over-header");
    var $menu_icon = $("#menu-icon");
    $nav_scroll_over_header.toggleClass('bg-white d-md-flex', $(this).scrollTop() > ($nav_scroll_over_header.height() + 600));
    $menu_icon.toggleClass('d-md-flex', $(this).scrollTop() > ($nav_scroll_over_header.height() + 600));
    $(document).scroll(function () {
        var $nav = $(".nav-lists");
        $nav.toggleClass('bg-white', $(this).scrollTop() > ($nav.height() + 50));
        $nav_scroll_over_header.toggleClass('bg-white d-md-flex', $(this).scrollTop() > ($nav_scroll_over_header.height() + 600));
        $menu_icon.toggleClass('d-md-flex', $(this).scrollTop() > ($nav_scroll_over_header.height() + 600));
    });

    
});
function gotopEvent(){
    $("html,body").animate({
        scrollTop: 0
    }, 1000);
}

function loadCoInfo(){
    $.ajax({
        type: 'POST',
        url: 'api/loadWebOption',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data[0];
            
            $("#Wo-zip").text(postdata['WO_Postalcode']);
            $("#Wo-addr").text(postdata['WO_Addr'] + postdata['WO_Addr1'] + ((postdata['WO_Addr2'].split(';').length > 1)?postdata['WO_Addr2'].split(';')[0]:postdata['WO_Addr2']));
            $("#Wo-tel-1").text(((postdata['WO_Tel'].split(';').length > 1)?postdata['WO_Tel'].split(';')[0]:postdata['WO_Tel']));
            $("#Wo-tel-2").text(((postdata['WO_Tel'].split(';').length > 1)?postdata['WO_Tel'].split(';')[1]:''));
            $("#Wo-work").text(((postdata['WO_Addr2'].split(';').length > 1)?postdata['WO_Addr2'].split(';')[1]:''));
        }

    });
}

function loadfamwood(){
    $.ajax({
        type: 'POST',
        url: 'api/loadFooterFamwood',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data[0];
            if(postdata)
            {
                $famwood = $("#famwood-footer");
                $famwood.find('.bg1').attr('style', `background: url('${postdata.Footer_Img}') no-repeat;background-size: cover`);
                $famwood.find('.tel').text(postdata.Footer_Intro);
                $famwood.find('.line-name').text(postdata.Footer_Title);
                $famwood.find('a').attr('href', 'https://page.line.me/'+postdata.Footer_Content.replace('@', ''));
            }
            else
            {
                $famwood = $("#famwood-footer");
                $famwood.addClass('d-none');
            }
                
        }

    });
}

function loadononnature(){
    $.ajax({
        type: 'POST',
        url: 'api/loadFooterOnon',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data[0];
            if(postdata)
            {
                $famwood = $("#ononnature-footer");
                $famwood.find('.bg2').attr('style', `background: url('${postdata.Footer_Img}') no-repeat;background-size: cover`);
                $famwood.find('.tel').text(postdata.Footer_Intro);
                $famwood.find('.line-name').text(postdata.Footer_Title);
                $famwood.find('a').attr('href', 'https://page.line.me/'+postdata.Footer_Content.replace('@', ''));
            }
            else
            {
                $famwood = $("#famwood-footer");
                $famwood.addClass('d-none');
            }
                
        }

    });
}

function loadprompt(){
    $.ajax({
        type: 'POST',
        url: 'api/loadPrompt',
        dataType: 'json',
        processData : false,
        contentType: false,
        cache: false,
        success:function(data){
            let postdata = data;
            $("#promp-wrap").html('');
            if(postdata)
            {
                let i = 0;
                postdata.forEach(element => {
                    $("#promp-wrap").append(`<div class="alert alert-danger alert-dismissible fade show mb-0 text-center alert-windows" role="alert">
                        <strong><i class="fas fa-info-circle"></i></strong> <a href="" class="text-reset" data-toggle="modal" data-target="#alertModal${i}">${element.Prompt_Intro}</a>            
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`);

                    $("#sidebar-wrap").after(`<div class="modal fade alert-modal" id="alertModal${i}" tabindex="-1" aria-labelledby="alertModal${i}Label" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header border-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body ">
                            <div class="text-center">
                                <img src="${element.Prompt_Img}" >
                            </div>        
                            <h2 class="text-center">${element.Prompt_Title}</h2>
                            <p class="mx-auto">${element.Prompt_Content.replace(/\r\n|\n|\r/, '<br/>')}</p>
                            </div>
                            <div class="modal-footer border-0 text-center">
                                <button type="button" class="btn mx-auto" data-dismiss="modal">知道了!</button>
                            </div>
                        </div>
                        </div>
                    </div>`);
                    i++;
                });
                
            } 
        }

    });
}

